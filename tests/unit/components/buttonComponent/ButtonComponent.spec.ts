import {shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
// TODO: fix bug with import const from vue components
import {
    BUTTON_BLOCK_CLASS,
    BUTTON_CLASS,
    BUTTON_CLASS_COUNTER,
    BUTTON_CLASS_DISABLED,
    BUTTON_CLASS_FAB,
    BUTTON_CLASS_OUTLINE,
    BUTTON_CLASS_PREPEND_ICON,
    BUTTON_CLASS_SMALL,
    BUTTON_CLASS_TRANSPARENT,
    BUTTON_CLASS_WRAPPER,
    BUTTON_LARGE_CLASS,
    BUTTON_LOADING_CLASS,
    BUTTON_MARGIN_BOTTOM_CLASS,
    BUTTON_MIN_WIDTH_CLASS,
    BUTTON_WHITE_CLASS,
    BUTTON_WIDE_CLASS,
} from '../../../../src/components/buttonComponent/constants';
import Component from '@/components/buttonComponent/ButtonComponent.vue';
import * as sinon from 'sinon';
// @ts-ignore
import themeVariables from '../../../../src/assets/scss/colors.module.scss';
import {expectEmitted} from '../../../utils/expects/componentEventEmitted';

describe('snapshots', () => {
    it('custom props', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                background: 'red',
                color: 'blue',
                small: true,
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('large', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                background: 'red',
                color: 'blue',
                small: false,
                large: true,
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('default props', () => {
        let wrapper = getShallowWrapper({
            propsData: {},
        });
        expect(wrapper.element).toMatchSnapshot();
    });

    it('prepend icon', () => {
        let wrapper = getShallowWrapper({
            slots: {
                'prepend-icon': 'slot contents',
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });

    it('counter', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                counter: '99',
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });

    it('isLoading', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                isLoading: true,
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });

    it('disabled filled', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                background: 'red',
                color: 'blue',
                disabled: true,
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('disabled filled small', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                background: 'red',
                color: 'blue',
                disabled: true,
                small: true,
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('disabled outline', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                background: 'red',
                color: 'blue',
                disabled: true,
                outline: true,
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('disabled outline small', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                background: 'red',
                color: 'blue',
                disabled: true,
                outline: true,
                small: true,
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('disabled transparent', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                background: 'red',
                color: 'blue',
                disabled: true,
                transparent: true,
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('disabled transparent small', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                background: 'red',
                color: 'blue',
                disabled: true,
                transparent: true,
                small: true,
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('computed', () => {
    it('buttonStyle', () => {
        let textColor = '#fefefe';
        let backgroundColor = '#000000';

        let textColorStub = sinon.stub().returns(textColor);
        let backgroundColorStub = sinon.stub().returns(backgroundColor);

        let wrapper = getShallowWrapper({
            computed: {
                textColor: textColorStub,
                backgroundColor: backgroundColorStub,
            },
        });

        expect(wrapper.vm.buttonStyle).toEqual(
            {
                '--text-color': textColor,
                '--background-color': backgroundColor,
                backgroundColor: backgroundColor,
                color: textColor,
            },
        );
    });

    describe('buttonClass', () => {
        it('large prop', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    large: true,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: true,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('wide prop', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    wide: true,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: true,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('white prop', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    white: true,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: true,
                },
            );
        });

        it('classic button', () => {
            let wrapper = getShallowWrapper();

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('classic small button', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    small: true,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: true,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('classic button with minWidth', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    minWidth: true,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: true,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('classic button with prepend icon (icon)', () => {
            let withPrependIconStub = sinon.stub().returns(true);

            let wrapper = getShallowWrapper({
                computed: {
                    withPrependIcon: withPrependIconStub,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: true,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
            sinon.assert.calledOnce(withPrependIconStub);
        });

        it('classic button with counter', () => {
            let counter = '12';

            let wrapper = getShallowWrapper({
                propsData: {
                    counter: counter,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: true,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('classic button fab', () => {

            let wrapper = getShallowWrapper({
                propsData: {
                    fab: true,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: true,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('outline button', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    outline: true,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: true,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('outline small button', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    outline: true,
                    small: true,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: true,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: true,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('outline button with minWidth', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    minWidth: true,
                    outline: true,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: true,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: true,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('outline button with prepend icon (icon)', () => {
            let withPrependIconStub = sinon.stub().returns(true);

            let wrapper = getShallowWrapper({
                propsData: {
                    outline: true,
                },
                computed: {
                    withPrependIcon: withPrependIconStub,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: true,
                    [BUTTON_CLASS_PREPEND_ICON]: true,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
            sinon.assert.calledOnce(withPrependIconStub);
        });

        it('outline button with counter', () => {
            let counter = '12';

            let wrapper = getShallowWrapper({
                propsData: {
                    outline: true,
                    counter: counter,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: true,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: true,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('outline button fab', () => {

            let wrapper = getShallowWrapper({
                propsData: {
                    fab: true,
                    outline: true,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: false,
                    [BUTTON_CLASS_OUTLINE]: true,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: true,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('transparent button', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    transparent: true,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: true,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('transparent small button', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    transparent: true,
                    small: true,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: true,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: true,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('transparent button with minWidth', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    minWidth: true,
                    transparent: true,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: true,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: true,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('transparent button with prepend icon (icon)', () => {
            let withPrependIconStub = sinon.stub().returns(true);

            let wrapper = getShallowWrapper({
                propsData: {
                    transparent: true,
                },
                computed: {
                    withPrependIcon: withPrependIconStub,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: true,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: true,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
            sinon.assert.calledOnce(withPrependIconStub);
        });

        it('transparent button with counter', () => {
            let counter = '12';

            let wrapper = getShallowWrapper({
                propsData: {
                    transparent: true,
                    counter: counter,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: true,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: true,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: false,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });

        it('transparent button fab', () => {

            let wrapper = getShallowWrapper({
                propsData: {
                    fab: true,
                    transparent: true,
                },
            });

            expect(wrapper.vm.buttonClass).toEqual(
                {
                    [BUTTON_CLASS]: true,
                    [BUTTON_CLASS_TRANSPARENT]: true,
                    [BUTTON_CLASS_OUTLINE]: false,
                    [BUTTON_CLASS_PREPEND_ICON]: false,
                    [BUTTON_CLASS_COUNTER]: false,
                    [BUTTON_CLASS_SMALL]: false,
                    [BUTTON_CLASS_FAB]: true,
                    [BUTTON_MIN_WIDTH_CLASS]: false,
                    [BUTTON_LARGE_CLASS]: false,
                    [BUTTON_WIDE_CLASS]: false,
                    [BUTTON_WHITE_CLASS]: false,
                },
            );
        });
    });

    describe('wrapperClass', () => {
        it('default class', () => {
            let wrapper = getShallowWrapper();

            expect(wrapper.vm.wrapperClass).toEqual(
                {
                    [BUTTON_CLASS_WRAPPER]: true,
                    [BUTTON_CLASS_DISABLED]: false,
                    [BUTTON_BLOCK_CLASS]: false,
                    [BUTTON_MARGIN_BOTTOM_CLASS]: false,
                    [BUTTON_LOADING_CLASS]: false,
                },
            );
        });

        it('disabled class', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    disabled: true,
                },
            });

            expect(wrapper.vm.wrapperClass).toEqual(
                {
                    [BUTTON_CLASS_WRAPPER]: true,
                    [BUTTON_CLASS_DISABLED]: true,
                    [BUTTON_BLOCK_CLASS]: false,
                    [BUTTON_MARGIN_BOTTOM_CLASS]: false,
                    [BUTTON_LOADING_CLASS]: false,
                },
            );
        });

        it('margin bottom class', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    marginBottom: true,
                },
            });

            expect(wrapper.vm.wrapperClass).toEqual(
                {
                    [BUTTON_CLASS_WRAPPER]: true,
                    [BUTTON_CLASS_DISABLED]: false,
                    [BUTTON_BLOCK_CLASS]: false,
                    [BUTTON_MARGIN_BOTTOM_CLASS]: true,
                    [BUTTON_LOADING_CLASS]: false,
                },
            );
        });

        it('block class', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    block: true,
                },
            });

            expect(wrapper.vm.wrapperClass).toEqual(
                {
                    [BUTTON_CLASS_WRAPPER]: true,
                    [BUTTON_CLASS_DISABLED]: false,
                    [BUTTON_BLOCK_CLASS]: true,
                    [BUTTON_MARGIN_BOTTOM_CLASS]: false,
                    [BUTTON_LOADING_CLASS]: false,
                },
            );
        });

        it('loading class', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    isLoading: true,
                },
            });

            expect(wrapper.vm.wrapperClass).toEqual(
                {
                    [BUTTON_CLASS_WRAPPER]: true,
                    [BUTTON_CLASS_DISABLED]: false,
                    [BUTTON_BLOCK_CLASS]: false,
                    [BUTTON_MARGIN_BOTTOM_CLASS]: false,
                    [BUTTON_LOADING_CLASS]: true,
                },
            );
        });
    });

    describe('backgroundColor', () => {
        it('outline', () => {
            let color = themeVariables.white;

            let wrapper = getShallowWrapper({
                propsData: {
                    outline: true,
                },
            });

            expect(wrapper.vm.backgroundColor).toEqual(color);
        });

        it('transparent', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    transparent: true,
                },
            });

            expect(wrapper.vm.backgroundColor).toEqual('transparent');
        });

        it('set color from prop', () => {
            let propColor = '#5012FE';

            let wrapper = getShallowWrapper({
                propsData: {
                    background: propColor,
                },
            });

            expect(wrapper.vm.backgroundColor).toEqual(propColor);
        });
    });

    describe('textColor', () => {
        it('notFilledButton', () => {
            let color = themeVariables.gray1000;
            let notFilledButtonStub = sinon.stub().returns(true);

            let wrapper = getShallowWrapper({
                propsData: {
                    outline: true,
                },
                computed: {
                    notFilledButton: notFilledButtonStub,
                },
            });

            expect(wrapper.vm.textColor).toEqual(color);
        });

        it('filled button', () => {
            let color = 'red';
            let notFilledButtonStub = sinon.stub().returns(false);

            let wrapper = getShallowWrapper({
                propsData: {
                    color: color,
                },
                computed: {
                    notFilledButton: notFilledButtonStub,
                },
            });

            expect(wrapper.vm.textColor).toEqual(color);
        });
    });

    describe('withPrependIcon', () => {
        it('true', () => {
            let wrapper = getShallowWrapper({
                slots: {
                    'prepend-icon': 'slot contents',
                },
            });

            expect(wrapper.vm.withPrependIcon).toEqual(true);
        });

        it('false', () => {
            let wrapper = getShallowWrapper();

            expect(wrapper.vm.withPrependIcon).toEqual(false);
        });
    });

    describe('notFilledButton', () => {
        it('true (outline)', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    outline: true,
                },
            });

            expect(wrapper.vm.notFilledButton).toEqual(true);
        });

        it('true (transparent)', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    transparent: true,
                },
            });

            expect(wrapper.vm.notFilledButton).toEqual(true);
        });

        it('false', () => {
            let wrapper = getShallowWrapper();

            expect(wrapper.vm.notFilledButton).toEqual(false);
        });
    });

});

describe('watchers', () => {
});

describe('methods', () => {
    it('buttonClicked', () => {
        let wrapper = getShallowWrapper();

        wrapper.vm.buttonClicked();

        expectEmitted(wrapper, 'click');
    });
});

describe('validation', () => {
});

describe('lifecycle hooks', () => {

});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        propsData: {
            background: 'red',
            color: 'blue',
        },
    };
}
