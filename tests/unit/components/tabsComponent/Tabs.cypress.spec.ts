import {mount} from '@cypress/vue';
import * as _ from 'lodash';
import TabsComponentTest from './TabsComponentTest.vue';
import TabItem from '../../../../src/components/tabs/TabItem.vue';
import Component from '../../../../src/components/tabs/Tabs.vue';
import Tabs from '../../../../src/components/tabs/Tabs.vue';
import {TAB_ITEM_ACTIVE_CLASS, TAB_ITEM_DISABLED_CLASS} from '../../../../src/components/tabs/constants';

beforeEach(() => {
});

afterEach(() => {
});

const TAB_NAV_ITEM_CLASS = '.tab__nav__item';
const TAB_CONTENT_CLASS = '.tab__content';

let firstTab = {
    title: 'Title one',
    content: 'content one',
    counter: 4,
};
let secondTab = {
    title: 'Title two',
    content: 'content two',
};

it('one default tab', () => {
    mountComponent({
        slots: {
            default: `<TabItem><template slot="title">${firstTab.title}</template>${firstTab.content}</TabItem>`,
        },
    });

    getFirstTab().should('contain', firstTab.title);
    getFirstTab().should('have.class', TAB_ITEM_ACTIVE_CLASS);

    getTabContent().should('contain', firstTab.content);

    getSecondTab().should('not.exist');
});

it('2 tabs, 1st is active (by default)', () => {
    mountComponent({
        slots: {
            default: `
                    <TabItem><template slot="title">${firstTab.title}</template>${firstTab.content}</TabItem>
                    <TabItem><template slot="title">${secondTab.title}</template>${secondTab.content}</TabItem>
                    `,
        },
    });
    getFirstTab().should('contain', firstTab.title);
    getFirstTab().should('have.class', TAB_ITEM_ACTIVE_CLASS);

    getSecondTab().should('not.have.class', TAB_ITEM_ACTIVE_CLASS);
    getSecondTab().should('contain', secondTab.title);

    getTabContent().should('contain', firstTab.content);
    getTabContent().should('not.contain', secondTab.content);
});

it('one tab with counter', () => {
    let counter = 2;
    mountComponent({
        slots: {
            default: `<TabItem :counter="${counter}"><template slot="title">${firstTab.title}</template>${firstTab.content}</TabItem>`,
        },
    });

    getFirstTab().get('.tab__counter').should('contain', counter);
});

it('one tab with icon slot', () => {
    let iconSlotContent = 'icon';
    mountComponent({
        slots: {
            default: `<TabItem><template slot="icon">${iconSlotContent}</template><template slot="title">${firstTab.title}</template>${firstTab.content}</TabItem>`,
        },
    });

    getFirstTab().get('.tab__icon').should('contain', iconSlotContent);
});

it('one disabled tab', () => {
    mountComponent({
        slots: {
            default: `<TabItem disabled><template slot="title">${firstTab.title}</template>${firstTab.content}</TabItem>`,
        },
    });

    getFirstTab().should('have.class', TAB_ITEM_DISABLED_CLASS);
});

it('two tabs, second tab is active', () => {
    mount(TabsComponentTest, {
        components: {
            TabItem,
            Tabs,
        },
    });

    getFirstTab().should('not.have.class', TAB_ITEM_ACTIVE_CLASS);
    getSecondTab().should('have.class', TAB_ITEM_ACTIVE_CLASS);
});


it('should $emit input when click on tab nav', () => {
    let activeTabIndex = 0;
    let secondTabIndex = 1;
    let inputListenter = cy.stub();

    mountComponent({
        propsData: {
            value: activeTabIndex,
        },
        slots: {
            default: `
                    <TabItem><template slot="title">${firstTab.title}</template>${firstTab.content}</TabItem>
                    <TabItem><template slot="title">${secondTab.title}</template>${secondTab.content}</TabItem>
                    `,
        },
        listeners: {
            input: inputListenter,
        },
    });

    cy.get(TAB_NAV_ITEM_CLASS).eq(secondTabIndex).click();
    inputListenter.calledOnceWith(secondTabIndex);
});

it('actions slot should be visible', () => {
    let activeTabIndex = 0;
    let actionsSlot = 'Actions slot content';

    mountComponent({
        propsData: {
            value: activeTabIndex,
            rounded: true,
        },
        slots: {
            default: `
                    <TabItem><template slot="title">${firstTab.title}</template>${firstTab.content}</TabItem>
                    <TabItem><template slot="title">${secondTab.title}</template>${secondTab.content}</TabItem>
                    `,
            actions: actionsSlot,
        },
    });

    cy.contains(actionsSlot).should('be.visible');
});

it('actions slot should be not visible', () => {
    let activeTabIndex = 0;
    let actionsSlot = 'Actions slot content';

    mountComponent({
        propsData: {
            value: activeTabIndex,
        },
        slots: {
            default: `
                    <TabItem><template slot="title">${firstTab.title}</template>${firstTab.content}</TabItem>
                    <TabItem><template slot="title">${secondTab.title}</template>${secondTab.content}</TabItem>
                    `,
            actions: actionsSlot,
        },
    });

    cy.contains(actionsSlot).should('not.exist');
});

function mountComponent(additionalOptions: any = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    mount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        components: {
            TabItem,
        },
    };
}

function getFirstTab(): any {
    return cy.get(TAB_NAV_ITEM_CLASS).eq(0);
}

function getSecondTab(): any {
    return cy.get(TAB_NAV_ITEM_CLASS).eq(1);
}

function getTabContent(): any {
    return cy.get(TAB_CONTENT_CLASS);
}
