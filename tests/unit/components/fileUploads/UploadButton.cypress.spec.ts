import {mount} from '@cypress/vue';
import * as _ from 'lodash';
import Component from '@/components/fileUploads/UploadButton.vue';

beforeEach(() => {
});

afterEach(() => {
});

describe('should render correct template', () => {
    it('button with text', () => {
        let text = 'upload';

        mountComponent({
            slots: {
                default: text,
            },
        });

        cy.contains(text).should('be.visible');
    });

    it('fab button', () => {
        let text = 'upload';

        mountComponent({
            propsData: {
                fab: true,
            },
            slots: {
                default: text,
            },
        });

        cy.contains(text).should('not.contain');
    });

    it('outline button', () => {
        mountComponent({
            propsData: {
                outline: true,
            },
        });

        cy.get('.buttonComponent').should('have.class', 'outlineStyle');
    });
});

function mountComponent(additionalOptions: any = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    mount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}
