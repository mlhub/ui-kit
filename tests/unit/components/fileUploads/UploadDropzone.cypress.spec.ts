import {mount} from '@cypress/vue';
import * as _ from 'lodash';
import Component from '@/components/fileUploads/UploadDropzone.vue';

describe('should render correct template', () => {
    it('mimetypes must be hidden', () => {
        mountComponent();

        cy.get('.uploader__mimetypes').should('not.exist');
    });

    it('file list must be hidden', () => {
        mountComponent();

        getList().should('not.exist');
    });

    it('mimetypes must be visible', () => {
        let accept = ['image/jpeg', 'image/jpg', 'image/pdf', 'image/png'];
        let result = 'jpeg / jpg / pdf / png';

        mountComponent({
            propsData: {
                accept: accept,
            },
        });

        cy.get('.uploader__mimetypes').should('be.visible');
        cy.contains(result).should('be.visible');
    });

    it('show texts from props', () => {
        let texts = {title: 'drop', divider: 'orr', button: 'click'};
        mountComponent({
            propsData: {
                texts: texts,
            },
        });

        cy.contains(texts.title).should('be.visible');
        cy.contains(texts.divider).should('be.visible');
        cy.contains(texts.button).should('be.visible');
    });

});

function mountComponent(additionalOptions: any = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    mount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}

function getList(): any {
    return cy.get('.uploader__list');
}

