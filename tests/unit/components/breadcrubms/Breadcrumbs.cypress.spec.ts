import * as _ from 'lodash';
import {mount} from '@cypress/vue';
import Component from '@/components/breadcrubms/Breadcrumbs.vue';

it('should render correct template', () => {
    const routes = [
        {
            url: 'url-string',
            title: 'title-string',
        },
        {
            url: 'url-string-2',
            title: 'title-string-2',
        },
    ];
    mountComponent({
        propsData: {
            routes: routes,
        },
    });

    cy.contains(routes[0].title).should('be.visible');
    cy.contains(routes[1].title).should('be.visible');
    cy.get(`.breadcrumbs__link[href=${routes[0].url}]`).should('exist');
    cy.get(`.breadcrumbs__link[href=${routes[1].url}]`).should('exist');
});

function mountComponent(additionalOptions: any = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    mount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}
