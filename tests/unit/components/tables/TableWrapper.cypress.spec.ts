import {mount} from '@cypress/vue';
import * as _ from 'lodash';
import Component from '@/components/tables/TableWrapper.vue';
import TableColumn from '@/components/tables/TableColumn.vue';
import PaginationResponseFactory from '../../../utils/pagination/PaginationResponseFactory';

const paginationResponseFactory = new PaginationResponseFactory();

const idLabel = 'ID';
const titleLabel = 'Title';
const items = [
    {
        id: 1,
        title: 'one',
    },
    {
        id: 2,
        title: 'two',
    },
    {
        id: 3,
        title: 'three',
    },
];

it('should render correct template when items prop passed', () => {
    mountComponent({
        components: {
            TableColumn,
        },
        propsData: {
            items: items,
        },
        slots: {
            default: `
                <TableColumn label="${idLabel}">
                    <template slot-scope="row">
                            {{ row.id }}
                    </template>
                </TableColumn>
            <TableColumn label="${titleLabel}">
                <template slot-scope="row">
                        {{ row.title }}
                </template>
            </TableColumn>
            `,
        },
    });

    cy.get('.pagination').should('not.exist');
    cy.get('.perPageSelector').should('not.exist');

    expectTableHeaderRenderedCorrectly();
    expectTableRowsRenderedCorrectly();
});

it('should render correct template when loadItemsFunction prop passed', () => {
    let loadItemsFunctionStub = cy.stub().resolves({
        data: paginationResponseFactory.generate({
            data: items,
        }),
    });
    mountComponent({
        components: {
            TableColumn,
        },
        propsData: {
            loadItemsFunction: loadItemsFunctionStub,
        },
        slots: {
            default: `
                <TableColumn label="${idLabel}">
                    <template slot-scope="row">
                            {{ row.id }}
                    </template>
                </TableColumn>
            <TableColumn label="${titleLabel}">
                <template slot-scope="row">
                        {{ row.title }}
                </template>
            </TableColumn>
            `,
        },
    });

    cy.get('.pagination').should('be.visible');
    cy.get('.perPageSelector').should('be.visible');

    expectTableHeaderRenderedCorrectly();
    expectTableRowsRenderedCorrectly();
});

function expectTableHeaderRenderedCorrectly(): void {
    cy.get('thead').within(() => {
        cy.get('th').should('have.length', 2);
        cy.contains(idLabel).should('be.visible');
        cy.contains(titleLabel).should('be.visible');
    });
}

function expectTableRowsRenderedCorrectly(): void {
    cy.get('tbody tr').should('have.length', items.length);
    for (const itemsKey in items) {
        cy.get('tbody tr').eq(+itemsKey).within(() => {
            cy.get('td').eq(0).should('contain', items[itemsKey].id);
            cy.get('td').eq(1).should('contain', items[itemsKey].title);
        });
    }
}

function mountComponent(additionalOptions: object = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    mount(Component, options);
}

function getDefaultWrapperOptions(): object {
    return {};
}
