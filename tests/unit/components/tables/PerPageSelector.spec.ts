import {shallowMount, Wrapper} from '@vue/test-utils';
import * as _ from 'lodash';
import PerPageSelector from '@/components/tables/PerPageSelector.vue';
import {expectEmitted} from '../../../utils/expects/componentEventEmitted';

it('perPage change should emit input event ', () => {
    let newValue = 50;
    let wrapper = getShallowWrapper({
        propsData: {
            value: 100,
        },
    });

    wrapper.vm.perPage = {value: newValue};

    expectEmitted(wrapper, 'input', newValue);
});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(PerPageSelector, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}
