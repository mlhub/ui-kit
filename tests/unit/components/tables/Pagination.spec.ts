import {shallowMount, Wrapper} from '@vue/test-utils';
import * as _ from 'lodash';
import Component from '@/components/tables/Pagination.vue';
import * as sinon from 'sinon';
import {expectEmitted, expectNotEmitted} from '../../../utils/expects/componentEventEmitted';

beforeEach(() => {
});

afterEach(() => {
    sinon.restore();
});

describe('changeCurrentPage', () => {
    it('given page == current page', () => {
        let pageId = 1234;
        let wrapper = getShallowWrapper({
            propsData: {
                value: pageId,
            },
        });

        wrapper.vm.changeCurrentPage(pageId);

        expectNotEmitted(wrapper, 'input');
    });

    it('given page !== current page', () => {
        let pageId = 1234;
        let wrapper = getShallowWrapper({
            propsData: {
                value: 56656566,
            },
        });

        wrapper.vm.changeCurrentPage(pageId);

        expectEmitted(wrapper, 'input', pageId);
    });
});

describe('prev/next page buttons', () => {
    it('1 page exists', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: 1,
                totalPages: 1,
            },
        });

        expect(wrapper.findComponent({ref: 'previousPageButton'}).classes()).toContain('disabled__icon');
        expect(wrapper.findComponent({ref: 'nextPageButton'}).classes()).toContain('disabled__icon');
    });

    it('selected 1-st page of 2', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: 1,
                totalPages: 2,
            },
        });

        expect(wrapper.findComponent({ref: 'previousPageButton'}).classes()).toContain('disabled__icon');
        expect(wrapper.findComponent({ref: 'nextPageButton'}).classes()).not.toContain('disabled__icon');
    });

    it('selected 2-nd page of 2', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: 2,
                totalPages: 2,
            },
        });

        expect(wrapper.findComponent({ref: 'previousPageButton'}).classes()).not.toContain('disabled__icon');
        expect(wrapper.findComponent({ref: 'nextPageButton'}).classes()).toContain('disabled__icon');
    });
});

describe('snapshots', () => {
    it('total 9 pages, selected 5-th', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: 5,
                totalPages: 9,
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('total 30 pages, selected 1-st', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: 1,
                totalPages: 30,
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('total 30 pages, selected 30-th', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: 30,
                totalPages: 30,
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('total 30 pages, selected 15-th', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: 15,
                totalPages: 30,
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });
});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}
