import _ from 'lodash';
import sinon from 'sinon';
import * as faker from 'faker';
import Lottie from 'vue-lottie';
import {shallowMount, Wrapper} from '@vue/test-utils';
import Component from '@/components/utils/lottie/JsonAnimation.vue';
import {expectEmitted} from '../../../../utils/expects/componentEventEmitted';

let stopStub = sinon.stub();
let pauseStub = sinon.stub();
let playStub = sinon.stub();

let animationStub = {
    stop: stopStub,
    pause: pauseStub,
    play: playStub,
    addEventListener: sinon.stub().callsArg(1),
};

it('should pass correct props to child components', async () => {
    let expectedLottieOptions = {
        animationData: {},
        animationSpeed: 1,
        loop: faker.random.boolean(),
    };
    let wrapper = getShallowWrapper({
        propsData: {
            animationData: expectedLottieOptions.animationData,
            loop: expectedLottieOptions.loop,
        },
    });

    expect(wrapper.findComponent(Lottie).props().options).toEqual(expectedLottieOptions);
});

it('should emit @completed when animation event listener handle complete', () => {
    let wrapper = getShallowWrapper();
    emitAnimationCreated(wrapper);

    sinon.assert.calledOnce(animationStub.addEventListener);
    expectEmitted(wrapper, 'completed');
});

it('should emit @created when animation was created', () => {
    let wrapper = getShallowWrapper();
    emitAnimationCreated(wrapper);

    expectEmitted(wrapper, 'created');
});

it('stop method was called', () => {
    let wrapper = getShallowWrapper();

    emitAnimationCreated(wrapper);

    wrapper.vm.stop();
    sinon.assert.calledOnce(stopStub);
});

it('pause method was called', () => {
    let wrapper = getShallowWrapper();

    emitAnimationCreated(wrapper);

    wrapper.vm.pause();
    sinon.assert.calledOnce(pauseStub);
});

it('play method was called', () => {
    let wrapper = getShallowWrapper();

    emitAnimationCreated(wrapper);

    wrapper.vm.play();
    sinon.assert.calledOnce(playStub);
});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        propsData: {
            animationData: {},
            loop: faker.random.boolean(),
        },
    };
}

function emitAnimationCreated(wrapper: Wrapper<any>) {
    wrapper.findComponent(Lottie).vm.$emit('animCreated', animationStub);
}
