import _ from 'lodash';
import moxios from 'moxios';
import * as faker from 'faker';
import {shallowMount, Wrapper} from '@vue/test-utils';
import Component from '@/components/utils/lottie/JsonUrlAnimation.vue';
import JsonAnimation from '@/components/utils/lottie/JsonAnimation.vue';
import {expectEmitted} from '../../../../utils/expects/componentEventEmitted';
import sinon from 'sinon';

beforeEach(() => {
    moxios.install();
});

afterEach(() => {
    moxios.uninstall();
});

let animation = {
    animationContent: 'test',
};
let url = faker.internet.url();
let loop = faker.random.boolean();

it('should pass correct props to child components', (done) => {
    let wrapper = getShallowWrapper();

    expect(wrapper.findComponent(JsonAnimation).exists()).toBeFalsy();
    expect(wrapper.find('.animation__loader').exists()).toBeTruthy();

    moxios.wait(() => {
        let animationResponse = {[faker.random.objectElement()]: faker.random.objectElement()};
        let request = moxios.requests.mostRecent();
        expect(request.url).toEqual(url);

        request.respondWith({
            status: 200,
            response: animationResponse,
        }).then(() => {
            let animationWrapper = wrapper.findComponent(JsonAnimation);
            expect(animationWrapper.exists()).toBeTruthy();
            expect(animationWrapper.props().animationData).toEqual(animationResponse);
            expect(animationWrapper.props().loop).toEqual(loop);
            expect(wrapper.find('.animation__loader').exists()).toBeFalsy();
            done();
        });
    });
});

it('should load animation when url changed', async (done) => {
    let wrapper = await getWrapper();

    let newUrl = faker.internet.url();
    wrapper.setProps({
        url: newUrl,
    });

    moxios.wait(() => {
        let animationResponse = {[faker.random.objectElement()]: faker.random.objectElement()};
        let requests = moxios.requests.mostRecent();
        expect(requests.url).toEqual(newUrl);

        requests.respondWith({
            status: 200,
            response: animationResponse,
        }).then(() => {
            expect(wrapper.findComponent(JsonAnimation).props().animationData).toEqual(animationResponse);
            done();
        });
    });
});

it('should emit @completed when JsonAnimation emits @completed', async () => {
    let wrapper = await getWrapper();

    wrapper.findComponent(JsonAnimation).vm.$emit('completed');
    expectEmitted(wrapper, 'completed');
});

it('should emit @created when JsonAnimation emits @created', async () => {
    let wrapper = await getWrapper();

    wrapper.findComponent(JsonAnimation).vm.$emit('created');
    expectEmitted(wrapper, 'created');
});

it('stop method was called', () => {
    let stopStub = sinon.stub();
    let wrapper = getShallowWrapper({
        computed: {
            animationWrapper() {
                return {
                    stop: stopStub,
                };
            },
        },
    });

    wrapper.vm.stop();

    sinon.assert.calledOnce(stopStub);
});

it('pause method was called', () => {
    let pauseStub = sinon.stub();
    let wrapper = getShallowWrapper({
        computed: {
            animationWrapper() {
                return {
                    pause: pauseStub,
                };
            },
        },
    });

    wrapper.vm.pause();

    sinon.assert.calledOnce(pauseStub);
});

it('play method was called', () => {
    let playStub = sinon.stub();
    let wrapper = getShallowWrapper({
        computed: {
            animationWrapper() {
                return {
                    play: playStub,
                };
            },
        },
    });

    wrapper.vm.play();

    sinon.assert.calledOnce(playStub);
});

function getWrapper(options = {}): Promise<Wrapper<any>> {
    return new Promise((resolve) => {
        let wrapper = getShallowWrapper(options);

        moxios.wait(() => {
            let request = moxios.requests.mostRecent();
            expect(request.url).toEqual(url);

            request.respondWith({
                status: 200,
                response: animation,
            }).then(() => {
                resolve(wrapper);
            });
        });
    });
}

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        propsData: {
            url: url,
            loop: loop,
        },
    };
}
