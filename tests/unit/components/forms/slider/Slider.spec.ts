import {shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import Component from '@/components/forms/slider/Slider.vue';
import {expectEmitted} from '../../../../utils/expects/componentEventEmitted';


it('default value 50', async () => {
    let wrapper = getShallowWrapper({
        propsData: {
            value: 50,
        },
    });

    expect(wrapper.find('.sliderComponent_wrap').attributes().style).toBe('--progress-width: 50%;');
});


it('float value', async () => {
    let wrapper = getShallowWrapper({
        propsData: {
            value: 0.5,
            step: 0.1,
            max: 1,
        },
    });

    expect(wrapper.find('.sliderComponent_wrap').attributes().style).toBe('--progress-width: 50%;');
});


it('update value', async () => {
    let wrapper = getShallowWrapper({
        propsData: {
            value: 50,
        },
    });

    expect(wrapper.find('.sliderComponent_wrap').attributes().style).toBe('--progress-width: 50%;');
    const range = wrapper.find('input[type="range"]');
    await range.setValue(70);
    expectEmitted(wrapper, 'input', 70);
});


function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),

        additionalOptions,
    );

    return shallowMount(Component, options);
}


function getDefaultWrapperOptions(): any {
    return {};
}
