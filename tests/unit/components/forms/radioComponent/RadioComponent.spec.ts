import {shallowMount, Wrapper} from '@vue/test-utils';
import Component from '@/components/forms/radioComponent/RadioComponent.vue';
import _ from 'lodash';
import {expectEmitted} from '../../../../utils/expects/componentEventEmitted';
import {
    RADIO__CHECKED_CLASS,
    RADIO__DISABLED_CLASS,
    RADIO__SMALL_CLASS,
    RADIO__WRAPPER_CLASS,
} from '@/components/forms/radioComponent/constants';
import {Radio} from 'vue-checkbox-radio';

let value = 1;

describe('should render correct layout', () => {
    it('default ( unchecked)', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                modelValue: 2,
            },
        });

        let radioWrapper = wrapper.find(`.${RADIO__WRAPPER_CLASS}`);
        let radioComponent = wrapper.findComponent(Radio);

        expect(radioWrapper.classes(RADIO__CHECKED_CLASS)).toBeFalsy();
        expect(radioWrapper.classes(RADIO__SMALL_CLASS)).toBeFalsy();
        expect(radioWrapper.classes(RADIO__DISABLED_CLASS)).toBeFalsy();
        expect(radioComponent.props().disabled).toBeFalsy();
        expect(radioComponent.props().value).toEqual(value);
        expect(wrapper.find('.radio__label').exists()).toBeFalsy();
    });

    it('checked', async () => {
        let wrapper = getShallowWrapper();

        expect(wrapper.find(`.${RADIO__WRAPPER_CLASS}`).classes(RADIO__CHECKED_CLASS)).toBeTruthy();
    });

    it('with label', async () => {
        let label = 'label';
        let wrapper = getShallowWrapper({
            propsData: {
                label: label,
            },
        });
        let labelElement = wrapper.find('.radio__label');

        expect(labelElement.exists()).toBeTruthy();
        expect(labelElement.text()).toEqual(label);
    });

    it('disabled', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                disabled: true,
            },
        });

        expect(wrapper.find(`.${RADIO__WRAPPER_CLASS}`).classes(RADIO__DISABLED_CLASS)).toBeTruthy();
    });

    it('small', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                small: true,
            },
        });

        expect(wrapper.find(`.${RADIO__WRAPPER_CLASS}`).classes(RADIO__SMALL_CLASS)).toBeTruthy();
    });
});

it('component $emits', async () => {
    let newValue = 2;
    let wrapper = getShallowWrapper();

    await wrapper.findComponent(Radio).vm.$emit('input', newValue);

    expectEmitted(wrapper, 'input', newValue);
});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        propsData: {
            value: value,
            modelValue: value,
        },
    };
}
