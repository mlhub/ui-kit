import {mount} from '@cypress/vue';
import * as _ from 'lodash';
import Component from '@/components/forms/selectComponent/MultipleSelect.vue';
import PaginationResponseFactory from '../../../../utils/pagination/PaginationResponseFactory';

const paginationResponseFactory = new PaginationResponseFactory();

const options = [
    {
        id: 1,
        title: 'one',
        created_at: '2021-09-23 14:34:00',
    },
    {
        id: 2,
        title: 'two',
        created_at: '2021-09-23 14:34:00',
    },
    {
        id: 3,
        title: 'three',
        created_at: '2021-09-23 14:34:00',
    },
];
const label = 'Label';

describe('should render correct template in closed state', () => {
    it('with no items selected', () => {
        mountComponent({
            propsData: {
                label: label,
                options: options,
                itemText: 'title',
                itemValue: 'id',
                value: [],
            },
        });

        cy.contains(label).should('be.visible');
        for (const option of options) {
            cy.contains(option.title).should('not.exist');
        }
    });

    it('with some items selected', () => {
        let selectedOptions = options.slice(0, 2);
        mountComponent({
            propsData: {
                label: label,
                options: options,
                itemText: 'title',
                itemValue: 'id',
                value: selectedOptions,
            },
        });

        cy.contains(label).should('be.visible');
        for (const option of selectedOptions) {
            cy.contains(option.title).should('be.visible');
        }
        cy.contains(options[2].title).should('not.exist');
    });
});

it('should open on click', () => {
    mountComponent({
        propsData: {
            label: label,
            options: options,
            itemText: 'title',
            itemValue: 'id',
            value: [],
        },
    });

    cy.get('.v-select').click();

    for (const option of options) {
        cy.contains(option.title).should('be.visible');
    }
});

it('should select new item on click', (done) => {
    let inputListener = cy.stub();
    let changeListener = cy.stub();
    let newItem = options[1];
    let expectedEmitPayload = [options[0], options[1]];
    mountComponent({
        propsData: {
            label: label,
            options: options,
            itemText: 'title',
            itemValue: 'id',
            value: [options[0]],
        },
        listeners: {
            input: inputListener,
            change: changeListener,
        },
    });

    cy.get('.v-select').click();
    cy.contains(newItem.title).click()
        .then(() => {
            expect(inputListener).to.be.calledWith(expectedEmitPayload);
            expect(changeListener).to.be.calledWith(expectedEmitPayload);
            done();
        });
});

it('should unselect new item on click in closed state', (done) => {
    let inputListener = cy.stub();
    let changeListener = cy.stub();
    let optionToUnselect = options[1];
    let expectedEmitPayload = [options[0]];
    mountComponent({
        propsData: {
            label: label,
            options: options,
            itemText: 'title',
            itemValue: 'id',
            value: [options[0], options[1]],
        },
        listeners: {
            input: inputListener,
            change: changeListener,
        },
    });

    cy.contains(optionToUnselect.title).siblings('button').click()
        .then(() => {
            expect(inputListener).to.be.calledWith(expectedEmitPayload);
            expect(changeListener).to.be.calledWith(expectedEmitPayload);
            done();
        });
});

it('should unselect new item on click in opened state', (done) => {
    let inputListener = cy.stub();
    let changeListener = cy.stub();
    let optionToUnselect = options[1];
    let expectedEmitPayload = [options[0]];
    mountComponent({
        propsData: {
            label: label,
            options: options,
            itemText: 'title',
            itemValue: 'id',
            value: [options[0], options[1]],
        },
        listeners: {
            input: inputListener,
            change: changeListener,
        },
    });

    cy.get('.v-select').click();
    cy.get('ul .input__option').within(() => {
        cy.contains(optionToUnselect.title).click()
            .then(() => {
                expect(inputListener).to.be.calledWith(expectedEmitPayload);
                expect(changeListener).to.be.calledWith(expectedEmitPayload);
                done();
            });
    });
});

it('should load options when loadOptionsFunction prop passed', () => {
    mountComponent({
        propsData: {
            label: label,
            loadOptionsFunction: loadOptionsFunction,
            itemText: 'title',
            itemValue: 'id',
            value: [],
        },
    });

    cy.get('.v-select').click();

    for (const option of options) {
        cy.contains(option.title).should('be.visible');
    }
});

function loadOptionsFunction() {
    return Promise.resolve({
        data: paginationResponseFactory.generate({
            data: options,
            links: {next: null},
        }),
    });
}

function mountComponent(additionalOptions: object = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    mount(Component, options);
}

function getDefaultWrapperOptions(): object {
    return {};
}
