import {mount} from '@cypress/vue';
import * as _ from 'lodash';
import Component from '@/components/forms/selectComponent/SingleSelect.vue';
import PaginationResponseFactory from '../../../../utils/pagination/PaginationResponseFactory';

const paginationResponseFactory = new PaginationResponseFactory();

const options = [
    {
        id: 1,
        title: 'one',
        created_at: '2021-09-23 14:34:00',
    },
    {
        id: 2,
        title: 'two',
        created_at: '2021-09-23 14:34:00',
    },
];
const label = 'Label';

it('should render correct template in closed state with min amount of props', () => {
    mountComponent({
        propsData: {
            label: label,
            options: options,
            itemText: 'title',
            itemValue: 'id',
            value: options[0],
        },
    });

    cy.contains(label).should('be.visible');
    cy.contains(options[1].title).should('not.exist');
});

it('should open on click', () => {
    const value = options[0];
    mountComponent({
        propsData: {
            label: label,
            options: options,
            itemText: 'title',
            itemValue: 'id',
            value: value,
        },
    });

    cy.get('.v-select').click();

    for (const option of options) {
        cy.contains(option.title).should('be.visible');
        if (option.id === value.id) {
            cy.get('.input__option').within(() => {
                cy.contains(option.title).siblings('.input__option__icon').should('be.visible');
            });
        } else {
            cy.get('.input__option').within(() => {
                cy.contains(option.title).siblings('.input__option__icon').should('not.exist');
            });
        }
    }
});

it('should emit @input and @change with correct payloads when item is selected', (done) => {
    let inputListener = cy.stub();
    let changeListener = cy.stub();
    let value = options[0];
    let newValue = options[1];
    mountComponent({
        propsData: {
            label: label,
            options: options,
            itemText: 'title',
            itemValue: 'id',
            value: value,
        },
        listeners: {
            input: inputListener,
            change: changeListener,
        },
    });

    cy.get('.v-select').click().then(() => {
        cy.contains(newValue.title).click().then(() => {
            expect(inputListener).to.be.calledWith(newValue);
            expect(changeListener).to.be.calledWith(newValue);
            done();
        });
    });
});

it('should load options when loadOptionsFunction prop passed', () => {
    let value = options[0];
    mountComponent({
        propsData: {
            label: label,
            loadOptionsFunction: loadOptionsFunction,
            itemText: 'title',
            itemValue: 'id',
            value: value,
        },
    });

    cy.get('.v-select').click();

    for (const option of options) {
        cy.contains(option.title).should('be.visible');
    }
});

function loadOptionsFunction() {
    return Promise.resolve({
        data: paginationResponseFactory.generate({
            data: options,
            links: {next: null},
        }),
    });
}

function mountComponent(additionalOptions: object = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    mount(Component, options);
}

function getDefaultWrapperOptions(): object {
    return {};
}
