import {mount} from '@cypress/vue';
import * as _ from 'lodash';
import * as sinon from 'sinon';
import Component, {DEBOUNCED_DELAY} from '@/components/forms/searchComponent/SearchComponent.vue';
import 'cypress-fill-command';

beforeEach(() => {
});

afterEach(() => {
});

let defaultValue = '';

describe('should render correct layout', () => {
    it('default', () => {
        mountComponent();

        getSearchIcon().should('be.visible');
        getInput().should('be.visible');
        getInput().invoke('val').should('be.empty');
        getCross().should('be.not.visible');
    });

    it('should have value from prop', () => {
        let value = 'value';

        mountComponent({
            propsData: {
                value: value,
            },
        });

        getInput().invoke('val').should('contain', value);
    });

    it('large prop', () => {
        mountComponent({
            propsData: {
                large: true,
            },
        });

        cy.get('.input__wrapper').should('have.class', 'large');
    });
});

it('should $emit @input with empty string payload after cross click', () => {
    let inputListener = cy.spy();

    mountComponent({
        propsData: {
            value: 'value',
        },
        listeners: {
            input: inputListener,
        },
    });

    getCross().trigger('click');

    cy.wait(0).then(() => {
        expect(inputListener).to.be.calledOnceWith('');
    });
});

it('should props load request after some time called function', (done) => {
    let request = cy.stub();
    let value = 'newValue';
    mountComponent({
        propsData: {
            loadRequest: request,
        },
    });

    getInput().fill(value);

    cy.wait(DEBOUNCED_DELAY).then(() => {
        expect(request).to.be.calledOnceWith(value);
        done();
    });
});

it('should set search to url', (done) => {
    let pushStateSpy = sinon.spy(window.history, 'pushState');
    let newValue = 'search';

    mountComponent({
        propsData: {
            value: 'value',
            modifiesUrl: true,
        },
    });

    getInput().fill(newValue);

    cy.wait(DEBOUNCED_DELAY).then(() => {
        expect(pushStateSpy).to.be.calledOnceWith({}, sinon.match.string, sinon.match(newValue));
        done();
    });
});

it('should get url to search', (done) => {
    const href = '/?search=newVal';
    history.replaceState(history.state, '', href);
    let inputListener = cy.spy();

    mountComponent({
        propsData: {
            value: '',
            modifiesUrl: true,
        },
        listeners: {
            input: inputListener,
        },
    });

    cy.wait(DEBOUNCED_DELAY).then(() => {
        expect(inputListener).to.be.calledOnceWith('newVal');
        done();
    });
});

function mountComponent(additionalOptions: any = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    mount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        propsData: {
            value: defaultValue,
        },
    };
}

function getCross(): any {
    return cy.get('.cross');
}

function getInput(): any {
    return cy.get('.input__content input');
}

function getSearchIcon(): any {
    return cy.get('.input__prepend .svgIcon');
}


