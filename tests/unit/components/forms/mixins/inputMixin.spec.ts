import {shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import * as sinon from 'sinon';
import inputMixin from '@/components/forms/mixins/inputMixin';
import {expectEmitted} from '../../../../utils/expects/componentEventEmitted';

const component: any = {
    template: `
        <div>
        <InputTemplate ref="inputTemplate"/>
        <input ref="input"/></div>
    `,
    mixins: [
        inputMixin,
    ],
};

describe('snapshots', () => {

});

describe('computed', () => {
    describe('showPrependContent', () => {
        it('true', () => {
            let wrapper = getShallowWrapper({
                slots: {
                    'prepend': 'slot contents',
                },
            });
            expect(wrapper.vm.showPrependContent).toEqual(true);
        });

        it('false', () => {
            let wrapper = getShallowWrapper();
            expect(wrapper.vm.showPrependContent).toEqual(false);
        });
    });

    it('input', () => {
        let wrapper = getShallowWrapper();

        expect(wrapper.vm.input).not.toBeUndefined();
    });

    it('inputTemplate', () => {
        let wrapper = getShallowWrapper();

        expect(wrapper.vm.inputTemplate).not.toBeUndefined();
    });

});

describe('watchers', () => {
});

describe('methods', () => {

    it('focusElement', () => {
        let focusStub = sinon.stub();

        let inputStub = {
            focus: focusStub,
        };

        let wrapper = getShallowWrapper({
            computed: {
                input: () => inputStub,
            },
        });

        wrapper.vm.focusElement();

        sinon.assert.calledOnce(focusStub);
    });

    it('onMouseEnter', () => {
        let inputTemplateStub = {
            isHovered: false,
        };

        let wrapper = getShallowWrapper({
            computed: {
                inputTemplate: () => inputTemplateStub,
            },
        });

        wrapper.vm.onMouseEnter();

        expect(wrapper.vm.inputTemplate.isHovered).toEqual(true);
    });

    it('onMouseLeave', () => {
        let inputTemplateStub = {
            isHovered: true,
        };

        let wrapper = getShallowWrapper({
            computed: {
                inputTemplate: () => inputTemplateStub,
            },
        });

        wrapper.vm.onMouseLeave();

        expect(wrapper.vm.inputTemplate.isHovered).toEqual(false);
    });

    describe('onBlur', () => {
        it('should emit @change', () => {
            let inputValue = 'value';
            let inputTemplateStub = {
                inputValue: inputValue,
            };

            let wrapper = getShallowWrapper({
                computed: {
                    inputTemplate: () => inputTemplateStub,
                },
            });

            wrapper.vm.onBlur();

            expectEmitted(wrapper, 'change', inputValue);
        });

        it('if hovered', () => {
            let inputTemplateStub = {
                isHovered: true,
                isFocused: true,
            };

            let wrapper = getShallowWrapper({
                computed: {
                    inputTemplate: () => inputTemplateStub,
                },
            });

            wrapper.vm.onBlur();

            expect(wrapper.vm.inputTemplate.isFocused).toEqual(true);
        });

        it('if click outside (not hovered)', () => {
            let inputTemplateStub = {
                isHovered: false,
                isFocused: true,
            };

            let wrapper = getShallowWrapper({
                computed: {
                    inputTemplate: () => inputTemplateStub,
                },
            });

            wrapper.vm.onBlur();

            expect(wrapper.vm.inputTemplate.isFocused).toEqual(false);
        });
    });

    it('onFocus', () => {
        let inputTemplateStub = {
            isFocused: false,
        };

        let wrapper = getShallowWrapper({
            computed: {
                inputTemplate: () => inputTemplateStub,
            },
        });

        wrapper.vm.onFocus();

        expect(wrapper.vm.inputTemplate.isFocused).toEqual(true);
    });
});

describe('validation', () => {
});

describe('lifecycle hooks', () => {

});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        stubs: {
            'InputTemplate': {
                render: () => {
                },
            },
        },
    };
}
