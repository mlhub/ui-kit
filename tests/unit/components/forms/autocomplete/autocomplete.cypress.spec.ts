import {mount} from '@cypress/vue';
import * as _ from 'lodash';
import Component, {DEBOUNCED_DELAY} from '@/components/forms/autocomplete/Autocomplete.vue';
import 'cypress-fill-command';

beforeEach(() => {
});

afterEach(() => {
});

const options = [
    {
        id: 1,
        title: 'one',
        created_at: '2021-09-23 14:34:00',
    },
    {
        id: 2,
        title: 'two',
        created_at: '2021-09-23 14:34:00',
    },
];

const optionsCustomKeys = [
    {
        value: 1,
        title: 'one',
    },
    {
        value: 2,
        title: 'two',
    },
];

describe('should render correct layout', () => {
    it('default', () => {
        mountComponent();

        getSearchIcon().should('be.visible');
        getInput().should('be.visible');
        getInput().invoke('val').should('be.empty');
    });

    it('with placeholder', () => {
        let placeholder = 'Search by event name';
        mountComponent({
            propsData: {
                placeholder: placeholder,
            },
        });

        getInput().invoke('attr', 'placeholder').should('contain', placeholder);
    });

    it('large prop', () => {
        mountComponent({
            propsData: {
                large: true,
            },
        });

        cy.get('.input__wrapper').should('have.class', 'large');
    });
});

it('should load options when loadOptionsFunction prop passed', () => {
    mountComponent({
        propsData: {
            loadOptionsFunction: loadOptionsFunction,
        },
    });

    getInput().trigger('focus');
    getInput().fill('Search text');

    cy.wait(DEBOUNCED_DELAY).then(() => {
        for (const option of options) {
            cy.contains(option.title).should('be.visible');
        }
    });
});

it('should load options when loadOptionsFunction custom key value prop passed', () => {
    mountComponent({
        propsData: {
            itemText: 'title',
            loadOptionsFunction: loadOptionsFunctionCustomOptions,
        },
    });

    getInput().trigger('focus');
    getInput().fill('Search text');

    cy.wait(DEBOUNCED_DELAY).then(() => {
        for (const option of optionsCustomKeys) {
            cy.contains(option.title).should('be.visible');
        }
    });
});

it('should highlight text when search value equals option title', () => {
    let value = options[0].title;
    mountComponent({
        propsData: {
            loadOptionsFunction: loadOptionsFunction,
        },
    });

    getInput().trigger('focus');
    getInput().fill(value);

    cy.wait(DEBOUNCED_DELAY).then(() => {
        cy.get('.text__highlight').contains(value).should('be.visible');
    });
});

it('should emit @input with correct payloads when item is selected', (done) => {
    let inputListener = cy.stub();
    let value = options[0];

    mountComponent({
        propsData: {
            loadOptionsFunction: loadOptionsFunction,
        },
        listeners: {
            input: inputListener,
        },
    });

    getInput().trigger('focus');
    getInput().fill('Search text');

    cy.wait(DEBOUNCED_DELAY).then(() => {
        cy.get('.v-select').click().then(() => {
            cy.contains(value.title).click().then(() => {
                expect(inputListener).to.be.calledWith({
                    text: value.title,
                    value: value.id,
                    realOption: value,
                });
                done();
            });
        });
    });
});

it('should emit @input with null when item is cleared', (done) => {
    let inputListener = cy.stub();
    let value = options[0];

    mountComponent({
        propsData: {
            loadOptionsFunction: loadOptionsFunction,
        },
        listeners: {
            input: inputListener,
        },
    });

    getInput().trigger('focus');
    getInput().fill('Search text');

    cy.wait(DEBOUNCED_DELAY).then(() => {
        cy.get('.v-select').click().then(() => {
            cy.contains(value.title).click().then(() => {
                expect(inputListener).to.be.calledWith({
                    text: value.title,
                    value: value.id,
                    realOption: value,
                });
                cy.get('.vs__clear').should('be.visible');
                cy.get('.vs__clear').click().then(() => {
                    expect(inputListener).to.be.calledWith(null);
                    done();
                });
            });
        });
    });
});

function loadOptionsFunction(): Promise<typeof options> {
    return Promise.resolve(options);
}

function loadOptionsFunctionCustomOptions(): Promise<typeof optionsCustomKeys> {
    return Promise.resolve(optionsCustomKeys);
}

function mountComponent(additionalOptions: any = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    mount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        propsData: {
            itemText: 'title',
            itemValue: 'id',
        },
    };
}

function getInput(): any {
    return cy.get('.vs__search');
}

function getSearchIcon(): any {
    return cy.get('.autocomplete__icon .svgIcon');
}


