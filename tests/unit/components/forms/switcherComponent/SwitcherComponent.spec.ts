import {shallowMount, Wrapper} from '@vue/test-utils';
import Component from '@/components/forms/switcherComponent/SwitcherComponent.vue';
import _ from 'lodash';
import {
    SWITCHER_CHECKED,
    SWITCHER_DISABLED,
    SWITCHER_SMALL,
    SWITCHER_WRAPPER_CLASS,
} from '../../../../../src/components/forms/switcherComponent/constants';
import {expectEmitted} from '../../../../utils/expects/componentEventEmitted';

describe('should render correct template', () => {
    it('default template', async () => {
        let wrapper = getShallowWrapper();

        let switcherWrapper = wrapper.find(`.${SWITCHER_WRAPPER_CLASS}`);
        expect(switcherWrapper.classes(SWITCHER_SMALL)).toBeFalsy();
        expect(switcherWrapper.classes(SWITCHER_DISABLED)).toBeFalsy();
        expect(switcherWrapper.classes(SWITCHER_CHECKED)).toBeFalsy();
        // @ts-ignore
        expect(wrapper.find('.switcher__input').element.checked).toEqual(false);
        expect(wrapper.find('.switcher__label').exists()).toBeFalsy();
    });

    it('with label', async () => {
        let label = 'label';
        let wrapper = getShallowWrapper({
            propsData: {
                label: label,
            },
        });

        let labelElement = wrapper.find('.switcher__label');
        expect(labelElement.exists()).toBeTruthy();
        expect(labelElement.text()).toEqual(label);
    });

});

describe('should give correct classes', () => {
    it('small', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                small: true,
            },
        });

        expect(wrapper.find(`.${SWITCHER_WRAPPER_CLASS}`).classes(SWITCHER_SMALL)).toBeTruthy();
    });

    it('disabled', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                disabled: true,
            },
        });

        expect(wrapper.find(`.${SWITCHER_WRAPPER_CLASS}`).classes(SWITCHER_DISABLED)).toBeTruthy();
    });

    it('checked', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: true,
            },
        });

        expect(wrapper.find(`.${SWITCHER_WRAPPER_CLASS}`).classes(SWITCHER_CHECKED)).toBeTruthy();
    });
});

it('input $emit', async () => {
    let newValue = true;
    let wrapper = getShallowWrapper();

    let input = wrapper.find('.switcher__input');
    await input.setChecked();
    // @ts-ignore
    expect(input.element.checked).toEqual(newValue);
    expectEmitted(wrapper, 'input', newValue);
});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}
