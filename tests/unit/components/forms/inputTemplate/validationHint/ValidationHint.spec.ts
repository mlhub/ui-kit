import {shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import Component from '@/components/forms/inputTemplate/validationHint/ValidationHint.vue';

describe('snapshots', () => {
    it('default props', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                validationHint: 'word',
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('computed', () => {
});

describe('watchers', () => {
});

describe('methods', () => {

});

describe('validation', () => {
});

describe('lifecycle hooks', () => {

});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}
