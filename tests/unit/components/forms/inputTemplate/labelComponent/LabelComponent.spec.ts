import {shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import faker from 'faker';
// @ts-ignore
import Component, {
    LABEL_CLASS,
    LABEL_DISABLED_CLASS,
} from '@/components/forms/inputTemplate/labelComponent/LabelComponent.vue';

describe('snapshots', () => {
    it('with label', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                label: 'label',
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });

    it('without label', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                label: 'label',
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });

    it('small', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                label: 'label',
                small: true,
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });

    it('disabled', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                label: 'label',
                small: false,
                disabled: true,
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });

    it('without colons', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                label: 'label',
                small: false,
                labelWithoutColon: true,
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('computed', () => {
    describe('labelText', () => {
        it('with colon', () => {
            let label = 'label';
            let wrapper = getShallowWrapper({
                propsData: {
                    label: label,
                    labelWithoutColon: false,
                },
            });

            expect(wrapper.vm.labelText).toEqual(`${label}:`);
        });

        it('without colon', () => {
            let label = 'label';
            let wrapper = getShallowWrapper({
                propsData: {
                    label: label,
                    labelWithoutColon: true,
                },
            });

            expect(wrapper.vm.labelText).toEqual(label);
        });
    });
    describe('labelClass', () => {
        it('default', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    label: faker.random.word(),
                },
            });

            expect(wrapper.vm.labelClass).toEqual(
                {
                    [LABEL_DISABLED_CLASS]: false,
                    [LABEL_CLASS]: true,
                },
            );
        });

        it('disabled', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    label: faker.random.word(),
                    disabled: true,
                },
            });

            expect(wrapper.vm.labelClass).toEqual(
                {
                    [LABEL_DISABLED_CLASS]: true,
                    [LABEL_CLASS]: true,
                },
            );
        });
    });
});

describe('watchers', () => {
});

describe('methods', () => {

});

describe('validation', () => {
});

describe('lifecycle hooks', () => {

});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}
