import {shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import Component from '@/components/forms/inputTemplate/InputTemplate.vue';
import faker from 'faker';
import {expectEmitted} from '../../../../utils/expects/componentEventEmitted';
import {
    INPUT_DISABLED_CLASS,
    INPUT_FOCUSED_CLASS,
    INPUT_HOVERED_CLASS,
    INPUT_LARGE_CLASS,
    INPUT_READONLY_CLASS,
    INPUT_SMALL_CLASS,
    INPUT_WITH_BOTTOM_LINE_CLASS,
    INPUT_WITH_ERROR_CLASS,
    INPUT_WITH_HINT_CLASS,
    INPUT_WITH_PREPEND_CLASS,
    INPUT_WRAPPER_CLASS,
} from '@/components/forms/inputTemplate/constants';
import * as sinon from 'sinon';

describe('snapshots', () => {
    it('default props', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: 'inputComponent value',
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('computed', () => {
    it('inputValue', () => {
        let value = faker.random.word();
        let newValue = `${faker.random.word()}2`;
        let wrapper = getShallowWrapper({
            propsData: {
                value: value,
            },
        });

        expect(wrapper.vm.inputValue).toEqual(value);

        wrapper.vm.inputValue = newValue;
        expectEmitted(wrapper, 'input', newValue);
    });

    describe('inputClass', () => {

        it('with classes from attrs', () => {

            let wrapper = getShallowWrapper({
                propsData: {
                    name: faker.random.word(),
                    label: faker.random.word(),
                    placeholder: faker.random.word(),
                    hint: faker.random.word(),
                    value: faker.random.word(),
                    disabled: true,
                    small: true,
                    readonly: false,
                    withPrependContent: true,
                },
            });

            expect(wrapper.vm.inputClass).toEqual(
                {
                    [INPUT_WRAPPER_CLASS]: true,
                    [INPUT_SMALL_CLASS]: true,
                    [INPUT_WITH_BOTTOM_LINE_CLASS]: true,
                    [INPUT_WITH_HINT_CLASS]: true,
                    [INPUT_DISABLED_CLASS]: true,
                    [INPUT_WITH_ERROR_CLASS]: false,
                    [INPUT_WITH_PREPEND_CLASS]: true,
                    [INPUT_HOVERED_CLASS]: false,
                    [INPUT_FOCUSED_CLASS]: false,
                    [INPUT_LARGE_CLASS]: false,
                    [INPUT_READONLY_CLASS]: false,
                },
            );

        });

        it('default classes', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    name: faker.random.word(),
                    placeholder: faker.random.word(),
                    value: faker.random.word(),
                },
            });

            expect(wrapper.vm.inputClass).toEqual(
                {
                    [INPUT_WRAPPER_CLASS]: true,
                    [INPUT_SMALL_CLASS]: false,
                    [INPUT_WITH_BOTTOM_LINE_CLASS]: false,
                    [INPUT_WITH_HINT_CLASS]: false,
                    [INPUT_DISABLED_CLASS]: false,
                    [INPUT_WITH_ERROR_CLASS]: false,
                    [INPUT_WITH_PREPEND_CLASS]: false,
                    [INPUT_HOVERED_CLASS]: false,
                    [INPUT_FOCUSED_CLASS]: false,
                    [INPUT_LARGE_CLASS]: false,
                    [INPUT_READONLY_CLASS]: false,
                },
            );
        });

        it('large', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    name: faker.random.word(),
                    placeholder: faker.random.word(),
                    value: faker.random.word(),
                    large: true,
                },
            });

            expect(wrapper.vm.inputClass).toEqual(
                {
                    [INPUT_WRAPPER_CLASS]: true,
                    [INPUT_SMALL_CLASS]: false,
                    [INPUT_WITH_BOTTOM_LINE_CLASS]: false,
                    [INPUT_WITH_HINT_CLASS]: false,
                    [INPUT_DISABLED_CLASS]: false,
                    [INPUT_WITH_ERROR_CLASS]: false,
                    [INPUT_WITH_PREPEND_CLASS]: false,
                    [INPUT_HOVERED_CLASS]: false,
                    [INPUT_FOCUSED_CLASS]: false,
                    [INPUT_LARGE_CLASS]: true,
                    [INPUT_READONLY_CLASS]: false,
                },
            );
        });

        it('errorMessage', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    value: 'inputComponent value',
                    errorMessage: faker.random.word(),
                },
            });

            expect(wrapper.vm.inputClass).toEqual(
                {
                    [INPUT_WRAPPER_CLASS]: true,
                    [INPUT_SMALL_CLASS]: false,
                    [INPUT_WITH_BOTTOM_LINE_CLASS]: false,
                    [INPUT_WITH_HINT_CLASS]: false,
                    [INPUT_DISABLED_CLASS]: false,
                    [INPUT_WITH_ERROR_CLASS]: true,
                    [INPUT_WITH_PREPEND_CLASS]: false,
                    [INPUT_HOVERED_CLASS]: false,
                    [INPUT_FOCUSED_CLASS]: false,
                    [INPUT_LARGE_CLASS]: false,
                    [INPUT_READONLY_CLASS]: false,
                },
            );
        });

        it('readonly', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    value: 'inputComponent value',
                    readonly: true,
                },
            });

            expect(wrapper.vm.inputClass).toEqual(
                {
                    [INPUT_WRAPPER_CLASS]: true,
                    [INPUT_SMALL_CLASS]: false,
                    [INPUT_WITH_BOTTOM_LINE_CLASS]: false,
                    [INPUT_WITH_HINT_CLASS]: false,
                    [INPUT_DISABLED_CLASS]: false,
                    [INPUT_WITH_ERROR_CLASS]: false,
                    [INPUT_WITH_PREPEND_CLASS]: false,
                    [INPUT_HOVERED_CLASS]: false,
                    [INPUT_FOCUSED_CLASS]: false,
                    [INPUT_LARGE_CLASS]: false,
                    [INPUT_READONLY_CLASS]: true,
                },
            );
        });
    });

    describe('maxWidthOfUpperElements', () => {
        it('50%', () => {
            let result = '50%';
            let wrapper = getShallowWrapper({
                propsData: {
                    name: faker.random.word(),
                    placeholder: faker.random.word(),
                    value: faker.random.word(),
                    label: faker.random.word(),
                    validationHint: faker.random.word(),
                },
            });

            expect(wrapper.vm.maxWidthOfUpperElements).toEqual(result);
        });

        it('100%', () => {
            let result = '100%';
            let wrapper = getShallowWrapper({
                propsData: {
                    name: faker.random.word(),
                    placeholder: faker.random.word(),
                    value: faker.random.word(),
                    label: faker.random.word(),
                },
            });

            expect(wrapper.vm.maxWidthOfUpperElements).toEqual(result);
        });
    });

    it('cssVars', () => {
        let inputColor = 'red';

        let result = {
            '--input-color': inputColor,
        };
        let wrapper = getShallowWrapper({
            propsData: {
                name: faker.random.word(),
                placeholder: faker.random.word(),
                value: faker.random.word(),
                inputColor: inputColor,
            },
        });

        expect(wrapper.vm.cssVars).toEqual(result);
    });

    it('bottomLine', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                name: faker.random.word(),
                placeholder: faker.random.word(),
                value: faker.random.word(),
            },
            stubs: {
                'InputBottomLine': {
                    render: () => {
                    },
                },
            },
        });

        expect(wrapper.vm.bottomLine).not.toBeUndefined();
    });

    it('onChange', () => {
        let value = faker.random.word();

        let wrapper = getShallowWrapper({
            propsData: {
                name: faker.random.word(),
                placeholder: faker.random.word(),
                value: faker.random.word(),
            },
        });

        wrapper.vm.onChange(value);

        expectEmitted(wrapper, 'input', value);
    });

    it('setValidationStatus', () => {
        let isValid = faker.random.boolean();

        let wrapper = getShallowWrapper({
            propsData: {
                name: faker.random.word(),
                placeholder: faker.random.word(),
                value: faker.random.word(),
            },
        });

        wrapper.vm.setValidationStatus(isValid);

        expect(wrapper.vm.showError).toEqual(!isValid);
    });

    it('validate', () => {
        let value = faker.random.boolean();
        let validateStub = sinon.stub().returns(value);
        let bottomLine = {
            validate: validateStub,
        };

        let wrapper = getShallowWrapper({
            propsData: {
                name: faker.random.word(),
                placeholder: faker.random.word(),
                value: faker.random.word(),
                hint: faker.random.word(),
            },
            computed: {
                bottomLine: () => bottomLine,
            },
        });

        expect(wrapper.vm.validate()).toEqual(value);
        sinon.assert.calledOnce(validateStub);
    });
});

describe('watchers', () => {
});

describe('methods', () => {
    it('reset', () => {
        let resetStub = sinon.stub();
        let bottomLine = {
            reset: resetStub,
        };
        let wrapper = getShallowWrapper({
            propsData: {
                value: faker.random.word(),
            },
            computed: {
                bottomLine: () => bottomLine,
            },
        });

        wrapper.vm.reset();

        sinon.assert.calledOnce(resetStub);
        expectEmitted(wrapper, 'input', '');
    });
});

describe('validation', () => {
});

describe('lifecycle hooks', () => {

});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}
