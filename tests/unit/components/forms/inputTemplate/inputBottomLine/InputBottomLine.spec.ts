import {shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import faker from 'faker';
import Component from '@/components/forms/inputTemplate/inputBottomLine/InputBottomLine.vue';
import * as sinon from 'sinon';
import * as validations from '@/utils/validation';
import {requiredRule} from '@/utils/validation';
import {expectEmitted, expectNotEmitted} from '../../../../../utils/expects/componentEventEmitted';

describe('snapshots', () => {
    it('default props', () => {
        let wrapper = getShallowWrapper();
        expect(wrapper.element).toMatchSnapshot();
    });

    it('with hint', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                hint: 'hint',
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });

    it('with hint && showError = true', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                hint: 'hint',
            },
            computed: {
                showError: () => true,
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });

    it('errorMessage', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                errorMessage: 'server error message',
            },
            computed: {
                showError: () => false,
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('computed', () => {
    it('isValid', () => {
        let validationResult = faker.random.boolean();
        let isValidStub = sinon.stub(validations, 'isValid').returns(validationResult);

        let wrapper = getShallowWrapper();

        expect(wrapper.vm.isValid).toEqual(validationResult);
        sinon.assert.calledOnce(isValidStub);
    });

    it('errorText', () => {
        let validationResult = faker.random.word();
        let validateStub = sinon.stub(validations, 'validate').returns(validationResult);

        let wrapper = getShallowWrapper();

        expect(wrapper.vm.errorText).toEqual(validationResult);
        sinon.assert.calledOnce(validateStub);
    });

    describe('showError', () => {
        it('true', () => {
            let validationResult = false;
            let isValidStub = sinon.stub().returns(validationResult);

            let wrapper = getShallowWrapper({
                data() {
                    return {
                        valueWasChanged: true,
                    };
                },
                computed: {
                    isValid: isValidStub,
                },
            });

            expect(wrapper.vm.showError).toEqual(true);
            sinon.assert.calledOnce(isValidStub);
        });

        it('false', () => {
            let validationResult = true;
            let isValidStub = sinon.stub().returns(validationResult);

            let wrapper = getShallowWrapper({
                data() {
                    return {
                        valueWasChanged: true,
                    };
                },
                computed: {
                    isValid: isValidStub,
                },
            });

            expect(wrapper.vm.showError).toEqual(false);
            sinon.assert.calledOnce(isValidStub);
        });
    });
});

describe('watchers', () => {
});

describe('methods', () => {
    describe('validate', () => {
        it('resetting case', () => {
            let wrapper = getShallowWrapper({
                data() {
                    return {
                        resetting: true,
                    };
                },
                computed: {
                    isValid: () => true,
                },
            });

            expect(wrapper.vm.validate()).toBeTruthy();
            expectNotEmitted(wrapper, 'validate');
        });

        it('true case', () => {
            let wrapper = getShallowWrapper({
                computed: {
                    isValid: () => true,
                },
            });

            expect(wrapper.vm.validate()).toBeTruthy();
            expectEmitted(wrapper, 'validate', true);
        });

        it('false case', () => {
            let wrapper = getShallowWrapper({
                computed: {
                    isValid: () => false,
                },
            });

            expect(wrapper.vm.validate()).toBeFalsy();
            expectEmitted(wrapper, 'validate', false);
        });
    });
});

describe('validation', () => {
});

describe('lifecycle hooks', () => {

});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        propsData: {
            value: 'inputComponent value',
            rules: [
                requiredRule('mail'),
            ],
        },
    };
}
