import {shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import upperElementsMixin from '@/components/forms/inputTemplate/mixins/upperElementsMixin';

const component: any = {
    template: '<div></div>',
    mixins: [
        upperElementsMixin,
    ],
};

describe('snapshots', () => {

});

describe('computed', () => {

    describe('maxWidthStyle', () => {
        it('50%', () => {
            let maxWidthOfUpperElements = '50%';

            let result = {
                maxWidth: maxWidthOfUpperElements,
            };

            let wrapper = getShallowWrapper({
                propsData: {
                    maxWidthOfUpperElements: maxWidthOfUpperElements,
                },
            });

            expect(wrapper.vm.maxWidthStyle).toEqual(result);
        });

        it('100%', () => {
            let maxWidthOfUpperElements = '100%';

            let result = {
                maxWidth: maxWidthOfUpperElements,
            };

            let wrapper = getShallowWrapper({
                propsData: {
                    maxWidthOfUpperElements: maxWidthOfUpperElements,
                },
            });

            expect(wrapper.vm.maxWidthStyle).toEqual(result);
        });
    });
});

describe('watchers', () => {
});

describe('methods', () => {

});

describe('validation', () => {
});

describe('lifecycle hooks', () => {

});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}
