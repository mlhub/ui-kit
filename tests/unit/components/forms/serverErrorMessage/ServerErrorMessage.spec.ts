import {shallowMount, Wrapper} from '@vue/test-utils';
import Component from '@/components/forms/serverErrorMessage/ServerErrorMessage.vue';
import ErrorIcon from '@/components/icons/formIcons/ErrorIcon.vue';
import _ from 'lodash';

describe('snapshots', () => {
    it('default', () => {
        let wrapper = getShallowWrapper();

        expect(wrapper.element).toMatchSnapshot();
    });
    it('custom message', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                message: 'Error',
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });
});

describe(':props', () => {
    it('default', () => {
        let wrapper = getShallowWrapper();

        let errorIcon = wrapper.findComponent(ErrorIcon);
        expect(errorIcon.attributes().nohover).toEqual('');
    });
});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}
