import {mount} from '@cypress/vue';
import * as _ from 'lodash';
import Component from '@/components/forms/textareaComponent/TextareaComponent.vue';
import 'cypress-fill-command';
import {waitForStubCallCypress} from '../../../../utils/cypress/coreUtils';

beforeEach(() => {
});

afterEach(() => {
});


describe('should render correct template', () => {
    it('value from prop', () => {
        let text = 'text';

        mountComponent({
            propsData: {
                value: text,
            },
        });

        getTextarea().should('have.value', text);
    });

    it('should show placeholder', () => {
        let placeholder = 'placeholder';

        mountComponent({
            propsData: {
                value: '',
                placeholder: placeholder,
            },
        });

        cy.get('textarea').should('have.attr', placeholder, placeholder);
    });

    it('should have css style resize:none', () => {
        mountComponent({
            propsData: {
                resizable: false,
            },
        });

        cy.get('textarea').should('have.css', 'resize', 'none');
    });

    it('should have css style resize:none', () => {
        mountComponent({
            propsData: {
                resizable: false,
            },
        });

        cy.get('textarea').should('have.css', 'resize', 'none');
    });

    it('should have label', () => {
        let label = 'label';

        mountComponent({
            propsData: {
                label: label,
            },
        });

        cy.contains(label).should('be.visible');
    });

    it('should have error message', () => {
        let errorMessage = 'errorMessage';

        mountComponent({
            propsData: {
                errorMessage: errorMessage,
            },
        });

        cy.contains(errorMessage).should('be.visible');
    });

    it('should have info message', () => {
        let hint = 'infoMessage';

        mountComponent({
            propsData: {
                hint: hint,
            },
        });

        cy.contains(hint).should('be.visible');
    });
});

it('should change value', (done) => {
    let newValue = 'new Value';

    let inputListener = cy.stub();

    mountComponent({
        listeners: {
            input: inputListener,
        },
    });

    getTextarea().fill(newValue);
    waitForStubCallCypress(inputListener).then(() => {
        expect(inputListener).to.be.calledOnceWith(newValue);
        done();
    });
});

function mountComponent(additionalOptions: any = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    mount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        propsData: {
            value: '',
        },
    };
}

function getTextarea(): any {
    return cy.get('textarea');
}


