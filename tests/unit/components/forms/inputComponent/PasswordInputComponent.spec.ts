import {shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import Component from '@/components/forms/inputComponent/PasswordInputComponent.vue';

describe('snapshots', () => {
    it('hidden text', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: 'inputComponent value',
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });

    it('show text', () => {
        let wrapper = getShallowWrapper({
            data() {
                return {
                    showText: true,
                };
            },
            propsData: {
                value: 'inputComponent value',
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('computed', () => {
});

describe('watchers', () => {
});

describe('methods', () => {
});

describe('validation', () => {
});

describe('lifecycle hooks', () => {

});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}
