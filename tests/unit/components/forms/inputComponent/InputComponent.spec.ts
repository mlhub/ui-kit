import {shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import Component from '@/components/forms/inputComponent/InputComponent.vue';

describe('snapshots', () => {
    it('default props', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: 'inputComponent value',
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });

    it('full props', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                label: 'label',
                placeholder: 'placeholder',
                hint: 'hints',
                value: 'inputComponent value',
                disabled: true,
                small: true,
                readonly: true,
                labelWithoutColon: true,
                validationHint: 'Validation hint',
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('prepend icon slot', () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: 'inputComponent value',
            },
            slots: {
                'prepend': 'slot contents',
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('computed', () => {
});

describe('watchers', () => {
});

describe('methods', () => {
});

describe('validation', () => {
});

describe('lifecycle hooks', () => {

});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}
