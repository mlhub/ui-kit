import {config, shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import Component from '@/components/forms/formComponent/FormComponent.vue';
import * as sinon from 'sinon';
import {expectEmitted, expectNotEmitted} from '../../../../utils/expects/componentEventEmitted';

//@TODO overwriting methods via the `methods` property is deprecated
config.showDeprecationWarnings = false;

describe('snapshots', () => {
    it('main', () => {
        let wrapper = getShallowWrapper({
            slots: {
                default: getInputComponentStub(),
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });
});

describe('computed', () => {
});

describe('watchers', () => {
});

describe('methods', () => {
    describe('submit', () => {
        it('true', () => {
            let validateResult = true;
            let validateStub = sinon.stub().returns(validateResult);
            let wrapper = getShallowWrapper({
                methods: {
                    validate: validateStub,
                },
            });

            wrapper.vm.submit();

            expectEmitted(wrapper, 'submit');
            sinon.assert.calledOnce(validateStub);
        });

        it('false', () => {
            let validateResult = false;
            let validateStub = sinon.stub().returns(validateResult);
            let wrapper = getShallowWrapper({
                methods: {
                    validate: validateStub,
                },
            });

            wrapper.vm.submit();

            expectNotEmitted(wrapper, 'submit');
            sinon.assert.calledOnce(validateStub);
        });
    });

    describe('validate', () => {
        it('no inputs - should return true', () => {
            let wrapper = getShallowWrapper();

            expect(wrapper.vm.validate()).toEqual(true);
        });

        it('all inputs validation return true - should return true', () => {
            let inputs = [getInputComponentStub(true), getInputComponentStub(true)];
            let wrapper = getShallowWrapper({
                slots: {
                    default: inputs,
                },
            });

            expect(wrapper.vm.validate()).toEqual(true);
            sinon.assert.calledOnce(inputs[0].methods.validate);
            sinon.assert.calledOnce(inputs[1].methods.validate);
        });

        it('all inputs validation return true, but input[0] has errorMessage:  should return false', () => {
            let inputs = [getInputComponentStub(true, 'error'), getInputComponentStub(true)];

            let wrapper = getShallowWrapper({
                slots: {
                    default: inputs,
                },
            });

            expect(wrapper.vm.validate()).toEqual(false);
            sinon.assert.calledOnce(inputs[0].methods.validate);
            sinon.assert.calledOnce(inputs[1].methods.validate);
        });

        it('one input validation returns false - should return false', () => {
            let inputs = [getInputComponentStub(true), getInputComponentStub(false)];
            let wrapper = getShallowWrapper({
                slots: {
                    default: inputs,
                },
            });

            expect(wrapper.vm.validate()).toEqual(false);
            sinon.assert.calledOnce(inputs[0].methods.validate);
            sinon.assert.calledOnce(inputs[1].methods.validate);
        });
    });

    describe('reset', () => {
        it('should reset all inputs', () => {
            let inputs = [getInputComponentStub(), getInputComponentStub()];
            let wrapper = getShallowWrapper({
                slots: {
                    default: inputs,
                },
            });

            wrapper.vm.reset();

            sinon.assert.calledOnce(inputs[0].methods.reset);
            sinon.assert.calledOnce(inputs[1].methods.reset);
        });
    });
});

describe('validation', () => {
});

function getInputComponentStub(validateMethodReturnValue = true, errorMessage = ''): any {
    return {
        name: 'InputComponent',
        template: '<input type="text">',
        computed:{
            errorMessage: () =>  errorMessage,
        },
        methods: {
            reset: sinon.stub(),
            validate: sinon.stub().returns(validateMethodReturnValue),
            focusElement: sinon.stub(),
        },
    };
}

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}
