import {shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import Component from '@/components/forms/checkboxComponent/CheckboxComponent.vue';
import {
    CHECKBOX__CHECKED_CLASS,
    CHECKBOX__DISABLED_CLASS,
    CHECKBOX__SMALL_CLASS,
    CHECKBOX__WITH_ERROR_CLASS,
    CHECKBOX__WRAPPER_CLASS,
} from '@/components/forms/checkboxComponent/constants';
import {Checkbox} from 'vue-checkbox-radio';
import CheckedIcon from '@/components/icons/checkbox/CheckedIcon.vue';
import MinusIcon from '@/components/icons/checkbox/MinusIcon.vue';

let value = false;

describe('should render correct layout', () => {

    it('default (unchecked)', async () => {
        let wrapper = getShallowWrapper();

        let checkboxWrapper = wrapper.find(`.${CHECKBOX__WRAPPER_CLASS}`);
        expect(checkboxWrapper.classes(CHECKBOX__CHECKED_CLASS)).toBeFalsy();
        expect(checkboxWrapper.classes(CHECKBOX__SMALL_CLASS)).toBeFalsy();
        expect(checkboxWrapper.classes(CHECKBOX__DISABLED_CLASS)).toBeFalsy();
        expect(checkboxWrapper.classes(CHECKBOX__WITH_ERROR_CLASS)).toBeFalsy();

        expect(wrapper.findComponent(Checkbox).props().disabled).toBeFalsy();
        expect(wrapper.find('.checkbox__label').exists()).toBeFalsy();
        expect(wrapper.findComponent(CheckedIcon).exists()).toBeFalsy();
    });

    it('checked', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: true,
            },
        });

        expect(wrapper.find(`.${CHECKBOX__WRAPPER_CLASS}`).classes(CHECKBOX__CHECKED_CLASS)).toBeTruthy();
        expect(wrapper.findComponent(CheckedIcon).exists()).toBeTruthy();
    });

    it('checked + indeterminate', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: true,
                indeterminate:true,
            },
        });

        expect(wrapper.find(`.${CHECKBOX__WRAPPER_CLASS}`).classes(CHECKBOX__CHECKED_CLASS)).toBeTruthy();
        expect(wrapper.findComponent(CheckedIcon).exists()).toBeFalsy();
        expect(wrapper.findComponent(MinusIcon).exists()).toBeTruthy();
    });

    it('with label', async () => {
        let label = 'label';
        let wrapper = getShallowWrapper({
            propsData: {
                label: label,
            },
        });

        let labelElement = wrapper.find('.checkbox__label');
        expect(labelElement.exists()).toBeTruthy();
        expect(labelElement.text()).toEqual(label);
    });

    it('disabled', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                disabled: true,
            },
        });

        expect(wrapper.find(`.${CHECKBOX__WRAPPER_CLASS}`)).toBeTruthy();
    });

    it('has error', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                hasError: true,
            },
        });

        expect(wrapper.find(`.${CHECKBOX__WITH_ERROR_CLASS}`)).toBeTruthy();
    });

    it('small', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                small: true,
            },
        });

        expect(wrapper.find(`.${CHECKBOX__SMALL_CLASS}`)).toBeTruthy();
    });
});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        propsData: {
            value: value,
        },
    };
}
