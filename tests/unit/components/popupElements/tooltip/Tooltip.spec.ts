import {shallowMount, Wrapper} from '@vue/test-utils';
import Component from '../../../../../src/components/popupElements/tooltip/Tooltip.vue';
import _ from 'lodash';
import {TOOLTIP_CLASS} from '../../../../../src/components/popupElements/tooltip/constants';
import {POSITION, POSITION_ABSOLUTE} from '../../../../../src/components/popupElements/constants';

let styles = {
    top: '150px',
    left: '250px',
};

describe('should render correct template', () => {
    it('fixed position', async () => {
        let classes = [TOOLTIP_CLASS, POSITION.TOP];
        let activatorSlotContent = 'activator';
        let defaultSlotContent = 'default';

        let wrapper = getShallowWrapper({
            propsData: {
                position: POSITION.TOP,
            },
            slots: {
                activator: activatorSlotContent,
                default: defaultSlotContent,
            },
        });

        expect(wrapper.find(`.${TOOLTIP_CLASS}`).classes()).toEqual(classes);
        expect(wrapper.find('.fixedPopup').element.style.top).toEqual(styles.top);
        expect(wrapper.find('.fixedPopup').element.style.left).toEqual(styles.left);
        expect(wrapper.vm.$slots.activator[0].text).toEqual(activatorSlotContent);
        expect(wrapper.vm.$slots.default[0].text).toEqual(defaultSlotContent);
    });

    it('absolute position', async () => {
        let classes = [TOOLTIP_CLASS, POSITION_ABSOLUTE, POSITION.TOP];
        let wrapper = getShallowWrapper({
            propsData: {
                position: POSITION.TOP,
                absolute: true,
            },
        });

        expect(wrapper.find(`.${TOOLTIP_CLASS}`).classes()).toEqual(classes);
    });
});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        computed: {
            // extended from mixin
            popupStyle: () => {
                return styles;
            },
        },
    };
}
