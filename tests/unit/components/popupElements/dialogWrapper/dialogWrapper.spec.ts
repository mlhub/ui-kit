import {shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import Component from '../../../../../src/components/popupElements/dialogWrapper/DialogWrapper.vue';
import {expectEmitted} from '../../../../utils/expects/componentEventEmitted';
import {
    DIALOG_ACTIONS_POSITION,
    DIALOG_CLASS,
    DIALOG_CLASS_ANIMATION,
    DIALOG_CLASS_CUSTOM_PADDINGS,
    DIALOG_CLASS_SIMPLE,
    DIALOG_CLASS_WITH_UNDERLINE,
    DIALOG_SIZE_EXTRA_LARGE,
    DIALOG_WIDTH_EXTRA_LARGE,
    DIALOG_WIDTH_MEDIUM,
} from '../../../../../src/components/popupElements/dialogWrapper/constants';
import ButtonComponent from '@/components/buttonComponent/ButtonComponent.vue';

describe('should render correct template', () => {
    it('default', async () => {
        let wrapper = getShallowWrapper();
        let dialog = wrapper.findComponent({ref: 'dialog'});
        expect(dialog.props().value).toEqual(true);
        expect(dialog.props().persistent).toEqual(true);
        expect(dialog.props().width).toEqual(DIALOG_WIDTH_MEDIUM);
        expect(wrapper.find('.dialog__cross').exists()).toBeFalsy();

        expectDefaultButtonsAreCorrect(wrapper);
    });

    it('custom props', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                persistent: false,
                simple: true,
                size: DIALOG_SIZE_EXTRA_LARGE,
                withCross: true,
            },
            slots: {
                header: 'Header',
            },
        });
        let dialog = wrapper.findComponent({ref: 'dialog'});
        expect(dialog.props().value).toEqual(true);
        expect(dialog.props().persistent).toEqual(false);
        expect(dialog.props().width).toEqual(DIALOG_WIDTH_EXTRA_LARGE);
        expect(wrapper.find('.dialog__cross').exists()).toBeTruthy();

        expectDefaultButtonsAreCorrect(wrapper);
    });


    it('disable submit button prop', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                disableSubmitButton: true,
            },
        });

        expect(getSubmitButton(wrapper).props().disabled).toEqual(true);
    });

    it('withoutActions prop', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                withoutActions: true,
            },
        });

        expect(wrapper.find('.dialog__actions').exists()).toEqual(false);
    });

    it('default button text props', async () => {
        let submitButtonText = 'Text submit';
        let submitCancelText = 'Text cancel';

        let wrapper = getShallowWrapper({
            propsData: {
                defaultSubmitButtonText: submitButtonText,
                defaultCancelButtonText: submitCancelText,
            },
        });

        expect(getCancelButton(wrapper).text()).toEqual(submitCancelText);
        expect(getSubmitButton(wrapper).text()).toEqual(submitButtonText);
    });

    describe('should choose correct class', () => {
        it('default', async () => {
            let wrapper = getShallowWrapper();

            let dialogContainer = getDialogContainer(wrapper);
            expect(dialogContainer.classes(DIALOG_CLASS)).toEqual(true);
            expect(dialogContainer.classes(DIALOG_CLASS_SIMPLE)).toEqual(false);
            expect(dialogContainer.classes(DIALOG_CLASS_ANIMATION)).toEqual(false);
            expect(dialogContainer.classes(DIALOG_ACTIONS_POSITION.CENTER)).toEqual(false);
            expect(dialogContainer.classes(DIALOG_ACTIONS_POSITION.RIGHT)).toEqual(true);
            expect(dialogContainer.classes(DIALOG_ACTIONS_POSITION.LEFT)).toEqual(false);
            expect(dialogContainer.classes(DIALOG_CLASS_WITH_UNDERLINE)).toEqual(false);
            expect(dialogContainer.classes(DIALOG_CLASS_CUSTOM_PADDINGS)).toEqual(false);
        });

        it('actions position start', async () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    actionsPosition: DIALOG_ACTIONS_POSITION.LEFT,
                },
            });

            let dialogContainer = getDialogContainer(wrapper);

            expect(dialogContainer.classes(DIALOG_ACTIONS_POSITION.LEFT)).toEqual(true);
        });

        it('actions position center', async () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    actionsPosition: DIALOG_ACTIONS_POSITION.CENTER,
                },
            });

            let dialogContainer = getDialogContainer(wrapper);

            expect(dialogContainer.classes(DIALOG_ACTIONS_POSITION.CENTER)).toEqual(true);
        });

        it('simple', async () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    simple: true,
                },
            });

            expect(wrapper.find('.dialog__container').classes(DIALOG_CLASS_SIMPLE)).toEqual(true);
        });

        it('withUnderline', async () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    withUnderline: true,
                },
            });

            expect(wrapper.find('.dialog__container').classes(DIALOG_CLASS_WITH_UNDERLINE)).toEqual(true);
        });

        it('custom paddings', async () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    customPaddings: true,
                },
            });

            expect(wrapper.find('.dialog__container').classes(DIALOG_CLASS_CUSTOM_PADDINGS)).toEqual(true);
        });

        it('animation', async () => {
            let wrapper = getShallowWrapper({
                slots: {
                    animation: 'animation slot',
                },
            });

            expect(wrapper.find('.dialog__container').classes(DIALOG_CLASS_ANIMATION)).toEqual(true);
        });
    });

    describe('slots content must be correct', () => {
        it('all slots but animation', async () => {
            let headerSlotContent = 'Header';
            let bodySlotContent = 'Body';
            let actionsSlotContent = 'Button';

            let wrapper = getShallowWrapper({
                slots: {
                    'header': headerSlotContent,
                    'body': bodySlotContent,
                    'actions': actionsSlotContent,
                },
            });

            expect(wrapper.find('.dialog__header').text()).toEqual(headerSlotContent);
            expect(wrapper.find('.dialog__body').text()).toEqual(bodySlotContent);
            expect(wrapper.find('.dialog__actions').text()).toEqual(actionsSlotContent);
        });

        it('animation slot', async () => {
            let animationSlotContent = 'Animation';
            let headerSlotContent = 'Header';
            let actionsSlotContent = 'Button';

            let wrapper = getShallowWrapper({
                slots: {
                    'animation': animationSlotContent,
                    'header': headerSlotContent,
                    'actions': actionsSlotContent,
                },
            });

            expect(wrapper.find('.dialog__animation').text()).toEqual(animationSlotContent);
            expect(wrapper.find('.dialog__header').text()).toEqual(headerSlotContent);
            expect(wrapper.find('.dialog__actions').text()).toEqual(actionsSlotContent);
        });
    });
});

describe('component $emits', () => {
    it('dialog input $emit', async () => {
        let newValue = false;
        let wrapper = getShallowWrapper();

        await wrapper.findComponent({ref: 'dialog'}).vm.$emit('input', newValue);

        expectEmitted(wrapper, 'input', newValue);
    });

    it('cross icon @click', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                simple: false,
                withCross: true,
            },
            slots: {
                header: 'header',
            },
        });

        await wrapper.find('.dialog__cross').trigger('click');

        expectEmitted(wrapper, 'input', false);
    });

    it('default cancel button @click', async () => {
        let wrapper = getShallowWrapper();

        await getCancelButton(wrapper).vm.$emit('click');

        expectEmitted(wrapper, 'cancel');
        expectEmitted(wrapper, 'input', false);
    });

    it('default submit button @click', async () => {
        let wrapper = getShallowWrapper();
        await getSubmitButton(wrapper).vm.$emit('click');

        expectEmitted(wrapper, 'submit');
    });
});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        propsData: {
            value: true,
        },
    };
}

function getCancelButton(wrapper: Wrapper<any>): any {
    return wrapper.findAllComponents(ButtonComponent).wrappers[0];
}

function getSubmitButton(wrapper: Wrapper<any>): any {
    return wrapper.findAllComponents(ButtonComponent).wrappers[1];
}

function getDialogContainer(wrapper: Wrapper<any>): any {
    return wrapper.find('.dialog__container');

}

function expectDefaultButtonsAreCorrect(wrapper: Wrapper<any>): void {
    expect(getCancelButton(wrapper).props().transparent).toEqual(true);
    expect(getCancelButton(wrapper).text()).toEqual('Cancel');

    expect(getSubmitButton(wrapper).props().minWidth).toEqual(true);
    expect(getSubmitButton(wrapper).props().disabled).toEqual(false);
    expect(getSubmitButton(wrapper).text()).toEqual('Confirm');
}
