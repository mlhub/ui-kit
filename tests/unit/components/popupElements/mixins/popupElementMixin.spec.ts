import {shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import popupElementMixin from '@/components/popupElements/mixins/popupElementMixin';
import {POSITION} from '../../../../../src/components/popupElements/constants';

const component: any = {
    template: `
        <div ref="activator">
        </div>
    `,
    mixins: [
        popupElementMixin,
    ],
};

let coordinates = {
    top: 150,
    left: 250,
    height: 250,
    width: 350,
};
let activatorStub = {
    getBoundingClientRect: () => {
        return coordinates;
    },
};

it('should set correct block position', () => {
    let wrapper = getShallowWrapper();

    expect(wrapper.vm.blockPosition).toEqual(coordinates);
});

describe('computed', () => {
    it('activator', () => {
        let wrapper = getShallowWrapper();

        expect(wrapper.vm.$refs.activator).not.toBeUndefined();
    });

    describe('popupStyle', () => {
        it('fixed position', () => {
            let top = 10;
            let left = 20;

            let wrapper = getShallowWrapper({
                computed: {
                    positionTop: () => top,
                    positionLeft: () => left,
                },
            });

            expect(wrapper.vm.popupStyle).toEqual({
                top: top,
                left: left,
            });
        });

        it('absolute position', () => {
            let top = 10;
            let left = 20;

            let wrapper = getShallowWrapper({
                propsData: {
                    absolute: true,
                },
                computed: {
                    positionTop: () => top,
                    positionLeft: () => left,
                },
            });

            expect(wrapper.vm.popupStyle).toEqual({});
        });
    });

    describe('positionTop', () => {
        it('position === top,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.TOP,
                },
            });

            expect(wrapper.vm.positionTop).toEqual(`${coordinates.top}px`);
        });

        it('position === topLeft,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.TOP_LEFT,
                },
            });

            expect(wrapper.vm.positionTop).toEqual(`${coordinates.top}px`);
        });

        it('position === topRight,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.TOP_RIGHT,
                },
            });

            expect(wrapper.vm.positionTop).toEqual(`${coordinates.top}px`);
        });

        it('position === leftTop,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.LEFT_TOP,
                },
            });

            expect(wrapper.vm.positionTop).toEqual(`${coordinates.top}px`);
        });

        it('position === rightTop,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.RIGHT_TOP,
                },
            });

            expect(wrapper.vm.positionTop).toEqual(`${coordinates.top}px`);
        });

        it('position === bottom,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.BOTTOM,
                },
            });

            expect(wrapper.vm.positionTop).toEqual(`${coordinates.top + coordinates.height}px`);
        });

        it('position === bottomLeft,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.BOTTOM_LEFT,
                },
            });

            expect(wrapper.vm.positionTop).toEqual(`${coordinates.top + coordinates.height}px`);
        });

        it('position === bottomRight,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.BOTTOM_RIGHT,
                },
            });

            expect(wrapper.vm.positionTop).toEqual(`${coordinates.top + coordinates.height}px`);
        });

        it('position === leftBottom,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.LEFT_BOTTOM,
                },
            });

            expect(wrapper.vm.positionTop).toEqual(`${coordinates.top + coordinates.height}px`);
        });

        it('position === rightBottom,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.RIGHT_BOTTOM,
                },
            });

            expect(wrapper.vm.positionTop).toEqual(`${coordinates.top + coordinates.height}px`);
        });

        it('position === left,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.LEFT,
                },
            });

            expect(wrapper.vm.positionTop).toEqual(`${coordinates.top + coordinates.height / 2}px`);
        });

        it('position === right,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.RIGHT,
                },
            });

            expect(wrapper.vm.positionTop).toEqual(`${coordinates.top + coordinates.height / 2}px`);
        });

        it('position === default,', () => {
            let wrapper = getShallowWrapper();

            expect(wrapper.vm.positionTop).toEqual(`${coordinates.top + coordinates.height / 2}px`);
        });
    });

    describe('positionLeft', () => {
        it('position === top,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.TOP,
                },
            });

            expect(wrapper.vm.positionLeft).toEqual(`${coordinates.left + coordinates.width / 2}px`);
        });

        it('position === bottom,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.BOTTOM,
                },
            });

            expect(wrapper.vm.positionLeft).toEqual(`${coordinates.left + coordinates.width / 2}px`);
        });

        it('position === topLeft,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.TOP_LEFT,
                },
            });

            expect(wrapper.vm.positionLeft).toEqual(`${coordinates.left}px`);
        });

        it('position === bottomLeft,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.BOTTOM_LEFT,
                },
            });

            expect(wrapper.vm.positionLeft).toEqual(`${coordinates.left}px`);
        });

        it('position === left,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.LEFT,
                },
            });

            expect(wrapper.vm.positionLeft).toEqual(`${coordinates.left}px`);
        });

        it('position === leftTop,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.LEFT_TOP,
                },
            });

            expect(wrapper.vm.positionLeft).toEqual(`${coordinates.left}px`);
        });

        it('position === topRight,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.TOP_RIGHT,
                },
            });

            expect(wrapper.vm.positionLeft).toEqual(`${coordinates.left + coordinates.width}px`);
        });

        it('position === bottomRight,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.BOTTOM_RIGHT,
                },
            });

            expect(wrapper.vm.positionLeft).toEqual(`${coordinates.left + coordinates.width}px`);
        });

        it('position === right,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.RIGHT,
                },
            });

            expect(wrapper.vm.positionLeft).toEqual(`${coordinates.left + coordinates.width}px`);
        });

        it('position === rightTop,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.RIGHT_TOP,
                },
            });

            expect(wrapper.vm.positionLeft).toEqual(`${coordinates.left + coordinates.width}px`);
        });

        it('position === rightBottom,', () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    position: POSITION.RIGHT_BOTTOM,
                },
            });

            expect(wrapper.vm.positionLeft).toEqual(`${coordinates.left + coordinates.width}px`);
        });

        it('position === default,', () => {
            let wrapper = getShallowWrapper();

            expect(wrapper.vm.positionLeft).toEqual(`${coordinates.left + coordinates.width}px`);
        });
    });
});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    let mount = shallowMount(component, options);

    window.dispatchEvent(new Event('load'));
    // @ts-ignore
    mount.vm.setPopupElementPosition();
    return mount;
}

function getDefaultWrapperOptions(): any {
    return {
        computed: {
            activator: () => activatorStub,
        },
    };
}
