import {shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import Component from '../../../../../src/components/popupElements/popover/Popover.vue';
import {
    POPOVER_ACTIVATOR_ACTIVE_CLASS,
    POPOVER_CLASS,
    POPOVER_SIZE,
} from '../../../../../src/components/popupElements/popover/constants';
import {POSITION, POSITION_ABSOLUTE} from '../../../../../src/components/popupElements/constants';
import {expectEmitted} from '../../../../utils/expects/componentEventEmitted';

let styles = {
    top: '150px',
    left: '250px',
};

it('should render correct template in closed state', async () => {
    let activatorSlotContent = 'activator';

    let wrapper = getShallowWrapper({
        propsData: {
            position: POSITION.TOP,
        },
        slots: {
            activator: activatorSlotContent,
        },
    });

    expectPopoverVisibility(wrapper, false);
    expect(wrapper.find('.fixedPopup__activator').exists()).toEqual(true);
    expect(wrapper.vm.$slots.activator[0].text).toEqual(activatorSlotContent);
});

describe('should render correct template in opened state', () => {
    it('fixed position', async () => {
        let classes = [POPOVER_CLASS, POSITION.TOP];
        let titleSlotContent = 'title';
        let activatorSlotContent = 'activator';
        let defaultSlotContent = 'default';
        let wrapper = getShallowWrapper({
            propsData: {
                position: POSITION.TOP,
            },
            slots: {
                activator: activatorSlotContent,
                title: titleSlotContent,
                default: defaultSlotContent,
            },
        });

        await wrapper.find('.fixedPopup__activator').trigger('click');

        expectPopoverVisibility(wrapper, true);
        expect(wrapper.find(`.${POPOVER_CLASS}`).classes()).toEqual(classes);
        expect(wrapper.find('.fixedPopup').element.style.top).toEqual(styles.top);
        expect(wrapper.find('.fixedPopup').element.style.left).toEqual(styles.left);
        expect(wrapper.vm.$slots.title[0].text).toEqual(titleSlotContent);
        expect(wrapper.vm.$slots.activator[0].text).toEqual(activatorSlotContent);
        expect(wrapper.vm.$slots.default[0].text).toEqual(defaultSlotContent);
    });

    it('absolute position', async () => {
        let classes = [POPOVER_CLASS, POSITION_ABSOLUTE, POSITION.TOP];
        let wrapper = getShallowWrapper({
            propsData: {
                position: POSITION.TOP,
                absolute: true,
            },
        });

        await wrapper.find('.fixedPopup__activator').trigger('click');

        expectPopoverVisibility(wrapper, true);
        expect(wrapper.find(`.${POPOVER_CLASS}`).classes()).toEqual(classes);
    });

    it('large type', async () => {
        let classes = [POPOVER_CLASS, POSITION.TOP, POPOVER_SIZE.LARGE];
        let wrapper = getShallowWrapper({
            propsData: {
                position: POSITION.TOP,
                size: POPOVER_SIZE.LARGE,
            },
        });

        await wrapper.find('.fixedPopup__activator').trigger('click');

        expectPopoverVisibility(wrapper, true);
        expect(wrapper.find(`.${POPOVER_CLASS}`).classes()).toEqual(classes);
    });

});


it('should close popover after cross @click', async () => {
    let wrapper = getShallowWrapper({
        propsData: {
            position: POSITION.TOP,
            withCross: true,
        },
    });

    await wrapper.find('.fixedPopup__activator').trigger('click');
    await wrapper.find('.fixedPopup__cross').trigger('click');

    expectPopoverVisibility(wrapper, false);
    expectEmitted(wrapper, 'hide');
});


it('should close popover after click yourself', async () => {
    let wrapper = getShallowWrapper({
        propsData: {
            position: POSITION.TOP,
        },
    });

    await wrapper.find('.fixedPopup__activator').trigger('click');

    expectPopoverVisibility(wrapper, true);

    await wrapper.find('.fixedPopup__activator').trigger('click');

    expectPopoverVisibility(wrapper, false);
    expectEmitted(wrapper, 'hide');
});


it('should add class to activator when opened', async () => {
    let wrapper = getShallowWrapper();
    const activatorClass = 'fixedPopup__activator';
    const classes = [activatorClass, POPOVER_ACTIVATOR_ACTIVE_CLASS];

    await wrapper.find(`.${activatorClass}`).trigger('click');

    expectPopoverVisibility(wrapper, true);
    expect(wrapper.find(`.${activatorClass}`).classes()).toEqual(classes);
});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        slots: {
            default: 'slot contents',
            title: 'title slot contents',
            activator: 'activator slot contents',
            header: 'header slot contents',
            footer: 'header slot contents',
        },
        computed: {
            // extended from mixin
            popupStyle: () => {
                return styles;
            },
        },
    };
}

function expectPopoverVisibility(wrapper: any, visible: boolean): void {
    expect(wrapper.find(`.${POPOVER_CLASS}`).exists()).toEqual(visible);
}
