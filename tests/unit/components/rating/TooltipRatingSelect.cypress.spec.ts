import {mount} from '@cypress/vue';
import * as _ from 'lodash';
// @ts-ignore
import Component from '../../../../src/components/rating/TooltipRatingSelect.vue';
import mlhKit from '../../../../src/entry.esm';
import Vue from 'vue';
import {$themeVariables} from '../../../utils/themeVariablesStub';

let notFilledStarColor = `--icon-color:${$themeVariables.gray30}; --hover-color:${$themeVariables.gray800}; --active-color:${$themeVariables.gray1000};`;
let filledStarColor = `--icon-color:${$themeVariables.yellow100}; --hover-color:${$themeVariables.gray800}; --active-color:${$themeVariables.gray1000};`;

it('hovered star (3)', () => {
    let starsCount = 4;
    let ratingTooltips = [
        'Bad',
        'Normal',
        'Good',
        'Excellent',
    ];
    let hoveredStartOrder = 2;

    mountComponent({
        propsData: {
            starsCount: starsCount,
            ratingTooltips: ratingTooltips,
        },
    });

    getStar(hoveredStartOrder).realHover();
    getStar(hoveredStartOrder).trigger('mouseenter');

    getStar(0).find('.svgIcon').should('have.attr', 'style', filledStarColor);
    getStar(1).find('.svgIcon').should('have.attr', 'style', filledStarColor);
    getStar(2).find('.svgIcon').should('have.attr', 'style', filledStarColor);
    getStar(3).find('.svgIcon').should('have.attr', 'style', notFilledStarColor);

    getTooltip(2);
    cy.contains(ratingTooltips[hoveredStartOrder]).should('be.visible');
});

function mountComponent(additionalOptions: any = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    Vue.use(mlhKit);
    mount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}

function getTooltip(order: number): any {
    return cy.get('.fixedPopup__wrapper').eq(order).find('.fixedPopup');
}

function getStar(order: number): any {
    return cy.get('.fixedPopup__wrapper').eq(order).find('.rating__icon');
}
