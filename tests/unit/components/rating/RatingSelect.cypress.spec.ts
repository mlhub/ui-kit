import {mount} from '@cypress/vue';
import * as _ from 'lodash';
// @ts-ignore
import Component from '../../../../src/components/rating/RatingSelect.vue';
import {DEFAULT_STARS_COUNT} from '../../../../src/components/rating/constants';
import mlhKit from '../../../../src/entry.esm';
import Vue from 'vue';
import {$themeVariables} from '../../../utils/themeVariablesStub';

let notFilledStarColor = `--icon-color:${$themeVariables.gray30}; --hover-color:${$themeVariables.gray800}; --active-color:${$themeVariables.gray1000};`;
let filledStarColor = `--icon-color:${$themeVariables.yellow100}; --hover-color:${$themeVariables.gray800}; --active-color:${$themeVariables.gray1000};`;

describe('should render correct layout', () => {
    it('default stars count', () => {
        mountComponent();

        cy.get('.rating__icon').should('have.length', DEFAULT_STARS_COUNT);
        expectAllStarsAreNotFilled();
    });

    it('stars count is 3', () => {
        let starsCount = 3;
        mountComponent({
            propsData: {
                starsCount: starsCount,
            },
        });

        cy.get('.rating__icon').should('have.length', starsCount);
        expectAllStarsAreNotFilled();
    });

    it('default value is 2', () => {
        let value = 2;
        mountComponent({
            propsData: {
                value: value,
            },
        });

        getStar(0).find('.svgIcon').should('have.attr', 'style', filledStarColor);
        getStar(1).find('.svgIcon').should('have.attr', 'style', filledStarColor);
        getStar(2).find('.svgIcon').should('have.attr', 'style', notFilledStarColor);
        getStar(3).find('.svgIcon').should('have.attr', 'style', notFilledStarColor);
        getStar(4).find('.svgIcon').should('have.attr', 'style', notFilledStarColor);
    });
});

it('hovered star', () => {
    let hoveredStartOrder = 2;
    mountComponent();

    getStar(hoveredStartOrder).realHover();
    getStar(hoveredStartOrder).trigger('mouseenter');

    getStar(0).find('.svgIcon').should('have.attr', 'style', filledStarColor);
    getStar(1).find('.svgIcon').should('have.attr', 'style', filledStarColor);
    getStar(2).find('.svgIcon').should('have.attr', 'style', filledStarColor);
    getStar(3).find('.svgIcon').should('have.attr', 'style', notFilledStarColor);
    getStar(4).find('.svgIcon').should('have.attr', 'style', notFilledStarColor);
});

it('click on star', () => {
    let hoveredStartOrder = 4;
    let changeListener = cy.stub();
    mountComponent({
        listeners: {
            change: changeListener,
        },
    });

    getStar(hoveredStartOrder).trigger('click');

    expectAllStarsAreFilled();
    changeListener.calledOnceWith(hoveredStartOrder);
});

function mountComponent(additionalOptions: any = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    Vue.use(mlhKit);
    mount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}

function getStar(order: number): any {
    return cy.get('.rating__icon').eq(order);
}

function expectAllStarsAreNotFilled(): any {
    cy.get('.rating__icon .svgIcon').should('have.attr', 'style', notFilledStarColor);
}

function expectAllStarsAreFilled(): any {
    cy.get('.rating__icon .svgIcon').should('have.attr', 'style', filledStarColor);
}
