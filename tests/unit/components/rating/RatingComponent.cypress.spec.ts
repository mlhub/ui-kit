import {mount} from '@cypress/vue';
import * as _ from 'lodash';
import Component from '../../../../src/components/rating/RatingComponent.vue';
import mlhKit from '../../../../src/entry.esm';
import Vue from 'vue';
import {$themeVariables} from '../../../utils/themeVariablesStub';
import {DEFAULT_STARS_COUNT} from '../../../../src/components/rating/constants';

beforeEach(() => {
});

afterEach(() => {
});

let notFilledStarColor = `--icon-color:${$themeVariables.gray30}; --hover-color:${$themeVariables.gray800}; --active-color:${$themeVariables.gray1000};`;
let filledStarColor = `--icon-color:${$themeVariables.yellow100}; --hover-color:${$themeVariables.gray800}; --active-color:${$themeVariables.gray1000};`;

it('should render correct layout with rating 0', () => {
    mountComponent({
        propsData: {
            rating: 0,
        },
    });

    cy.get('.rating__icon').should('have.length', DEFAULT_STARS_COUNT);
    expectAllStarsAreNotFilled();
});

it('rating is 2.44', () => {
    mountComponent({
        propsData: {
            rating: 2.44,
        },
    });

    getStar(0).find('.svgIcon').should('have.attr', 'style', filledStarColor);
    getStar(1).find('.svgIcon').should('have.attr', 'style', filledStarColor);
    getStar(2).find('.svgIcon').should('have.attr', 'style', notFilledStarColor);
    getStar(3).find('.svgIcon').should('have.attr', 'style', notFilledStarColor);
    getStar(4).find('.svgIcon').should('have.attr', 'style', notFilledStarColor);

    getText().should('contain', 2.4);
});

it('rating is 2.48', () => {
    mountComponent({
        propsData: {
            rating: 2.48,
        },
    });

    getStar(0).find('.svgIcon').should('have.attr', 'style', filledStarColor);
    getStar(1).find('.svgIcon').should('have.attr', 'style', filledStarColor);
    getStar(2).find('.svgIcon').should('have.attr', 'style', filledStarColor);
    getStar(3).find('.svgIcon').should('have.attr', 'style', notFilledStarColor);
    getStar(4).find('.svgIcon').should('have.attr', 'style', notFilledStarColor);

    getText().should('contain', 2.5);
});

function mountComponent(additionalOptions: any = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    Vue.use(mlhKit);
    mount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {};
}

function getStar(order: number): any {
    return cy.get('.rating__icon').eq(order);
}

function getText(): any {
    return cy.get('.rating__text');
}

function expectAllStarsAreNotFilled(): any {
    cy.get('.rating__icon .svgIcon').should('have.attr', 'style', notFilledStarColor);
}
