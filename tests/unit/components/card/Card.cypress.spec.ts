//todo: uncomment and fix after release
// import {mount} from "@cypress/vue";
// import * as _ from "lodash";
// import Component from "@/components/card/Card.vue";
// import {CARD_LABEL_COLORS, CARD_LABEL_POSITIONS} from "../../../../src/components/card/cardLabel/constants";
// import faker from "faker";
// import lineClamp from 'vue-line-clamp';
// import Vue from 'vue';
// import {$themeVariables} from "../../../utils/themeVariablesStub";
// import mlhKit from "../../../../src/build";
//
// beforeEach(() => {
// });
//
// afterEach(() => {
// });
//
// let title = 'Card title';
// let img = 'image.png';
//
// it('card with only required props', () => {
//     mountComponent();
//
//     cy.contains(title).should('be.visible');
//     getWrapper().should('not.have.class', 'dark');
//     getCardOptionsButton().should('not.exist');
//     cy.get(`.card__options__link`).should('not.exist');
//     cy.get(`.card__image`).should('have.attr', 'style', `background: url("${img}") center center / cover no-repeat;`);
//     getProgressWrapper().should('not.exist');
//     getCardLabelWrapper().should('not.exist');
//     getPopoverContent().should('not.exist');
// });
//
// it('card with labels', () => {
//     let labels = [
//         {
//             text: 'Completed',
//             color: 'green',
//         },
//         {
//             text: 'Assigned',
//             color: 'blue',
//         },
//         {
//             text: 'Denied',
//             color: 'red',
//         },
//     ];
//     mountComponent({
//         propsData: {
//             labels: labels,
//         },
//     });
//
//     getCardLabelWrapper().should('have.length', 3);
//     cy.contains(labels[0].text).parent().should('have.attr', 'style', `order: ${CARD_LABEL_POSITIONS[CARD_LABEL_COLORS.GREEN]};`);
//     cy.contains(labels[1].text).parent().should('have.attr', 'style', `order: ${CARD_LABEL_POSITIONS[CARD_LABEL_COLORS.BLUE]};`);
//     cy.contains(labels[2].text).parent().should('have.attr', 'style', `order: ${CARD_LABEL_POSITIONS[CARD_LABEL_COLORS.RED]};`);
//
//     cy.contains(labels[0].text).should('have.class', CARD_LABEL_COLORS.GREEN);
//     cy.contains(labels[1].text).should('have.class', CARD_LABEL_COLORS.BLUE);
//     cy.contains(labels[2].text).should('have.class', CARD_LABEL_COLORS.RED);
// });
//
// it('card long title', () => {
//     let title = faker.lorem.lines(5);
//
//     mountComponent({
//         propsData: {
//             title: title,
//         },
//     });
//
//     cy.get('.card__title').should('not.equal', 'title');
// });
//
// it('card with progress', () => {
//     let progress = 80;
//
//     mountComponent({
//         propsData: {
//             progress: progress,
//         },
//     });
//
//     getProgressWrapper().should('be.visible');
//     cy.get('.progress__slider').should('have.attr', 'style', `width: ${progress}%;`);
//     cy.contains(`${progress}%`).should('be.visible');
// });
//
// it('card with popover, @mouseover on card', () => {
//     let popover = 'Popover';
//     mountComponent({
//         slots: {
//             popover: popover,
//         },
//     });
//
//     getPopoverActivator().should('be.visible');
//     getWrapper().realHover();
//     getPopoverActivator().trigger('mouseenter');
//     cy.contains(popover).should('exist');
//     cy.get('.fixedPopup__content').should('exist');
// });
//
// describe('card with options', () => {
//     let link = 'link';
//     let options = 'options';
//
//     it('@mouseover on card', () => {
//         mountComponent({
//             slots: {
//                 link: link,
//                 options: options,
//             },
//         });
//         expectWrapperIsDark();
//         getWrapper().realHover();
//         getPopoverActivator().trigger('mouseenter');
//
//         getCardOptionsButton().should('be.visible');
//         cy.contains(link).should('be.visible');
//         cy.contains(options).should('not.exist');
//     });
//
//     it('@mouseover on card then click on button', () => {
//         mountComponent({
//             slots: {
//                 link: link,
//                 options: options,
//             },
//         });
//         getWrapper().realHover();
//         getPopoverActivator().trigger('mouseenter');
//         getCardOptionsButton().trigger('click');
//         cy.contains(options).should('exist');
//     });
//
//     it('@mouseover on card then click on button (only link)', () => {
//         mountComponent({
//             slots: {
//                 link: link,
//             },
//         });
//         getWrapper().realHover();
//         getPopoverActivator().trigger('mouseenter');
//         cy.contains(link).should('be.visible');
//         getCardOptionsButton().should('not.exist');
//         cy.contains(options).should('not.exist');
//     });
//
//     it('@mouseover on card then click on button (only options)', () => {
//         mountComponent({
//             slots: {
//                 options: options,
//             },
//         });
//         getWrapper().realHover();
//         getPopoverActivator().trigger('mouseenter');
//         getCardOptionsButton().trigger('click');
//         cy.contains(link).should('not.exist');
//         getCardOptionsButton().should('be.visible');
//         cy.contains(options).should('exist');
//     });
// });
//
// describe('card with single option', () => {
//     let link = 'link';
//     let option = 'option';
//     it('@mouseover on card', () => {
//         mountComponent({
//             slots: {
//                 link: link,
//                 option: option,
//             },
//         });
//         expectWrapperIsDark();
//
//         getWrapper().realHover();
//         getPopoverActivator().trigger('mouseenter');
//
//         cy.contains(link).should('be.visible');
//         cy.contains(option).should('be.visible');
//     });
// });
//
// function mountComponent(additionalOptions: any = {}): void {
//     let options = _.merge(
//         getDefaultWrapperOptions(),
//         additionalOptions,
//     );
//     Vue.use(lineClamp);
//     mount(Component, options);
// }
//
// function getDefaultWrapperOptions(): any {
//     return {
//         propsData: {
//             title: title,
//             img: img,
//         },
//     };
// }
//
// function getCardLabelWrapper(): any {
//     return cy.get(`.cardLabel__wrapper`);
// }
//
// function getProgressWrapper(): any {
//     return cy.get(`.progress__wrapper`);
// }
//
// function getPopoverContent(): any {
//     return cy.get(`.fixedPopup__content`);
// }
//
// function getPopoverActivator(): any {
//     return cy.get(`.fixedPopup__activator`).eq(0);
// }
//
// function getCardOptionsButton(): any {
//     return cy.get(`.card__options__button`);
// }
//
// function getWrapper(): any {
//     return cy.get('.card__wrapper');
// }
//
// function expectWrapperIsDark(): any {
//     getWrapper().should('have.class', 'dark');
// }
