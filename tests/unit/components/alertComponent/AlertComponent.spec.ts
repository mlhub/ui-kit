import {shallowMount, Wrapper} from '@vue/test-utils';
import Component from '@/components/alertComponent/AlertComponent.vue';
import _ from 'lodash';
import {ALERT_FIXED_CLASS, ALERT_TYPES, ALERT_WRAPPER_CLASS} from '../../../../src/components/alertComponent/contants';

import AlertWarningIcon from '@/components/icons/alerts/AlertWarningIcon.vue';
import AlertErrorIcon from '@/components/icons/alerts/AlertErrorIcon.vue';
import AlertInfoIcon from '@/components/icons/alerts/AlertInfoIcon.vue';
import AlertSuccessIcon from '@/components/icons/alerts/AlertSuccessIcon.vue';
import AlertTipIcon from '@/components/icons/alerts/AlertTipIcon.vue';
import {expectEmitted} from '../../../utils/expects/componentEventEmitted';

let defaultSlotContent = 'Default slot';

describe('should render correct template', () => {
    it('default template ( value - false)', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                value: false,
            },
        });

        expect(wrapper.find(`.${ALERT_WRAPPER_CLASS}`).exists()).toBeFalsy();
    });

    it('default template (value - true)', async () => {
        let wrapper = getShallowWrapper();

        expectDefaultClassesAreCorrect(wrapper);
        expectDefaultIconsAreCorrect(wrapper);
        expectDefaultSlotsAreCorrect(wrapper);
    });

    it('with cross', async () => {
        let wrapper = getShallowWrapper({
            propsData: {
                closable: true,
            },
        });

        expect(wrapper.find('.alert__cross').exists()).toBeTruthy();
    });

    describe('show correct class and icon by type', () => {
        it('type - Tip', async () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    type: ALERT_TYPES.TIP,
                },
            });

            let icon = wrapper.findComponent(AlertTipIcon);
            expect(wrapper.find(`.${ALERT_WRAPPER_CLASS}`).classes(ALERT_TYPES.TIP)).toBeTruthy();
            expect(icon.exists()).toBeTruthy();
            expect(icon.props().iconColor).toEqual(wrapper.vm.themeVariables.gray300);
        });

        it('type - error', async () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    type: ALERT_TYPES.ERROR,
                },
            });

            let icon = wrapper.findComponent(AlertErrorIcon);
            expect(wrapper.find(`.${ALERT_WRAPPER_CLASS}`).classes(ALERT_TYPES.ERROR)).toBeTruthy();
            expect(icon.exists()).toBeTruthy();
            expect(icon.props().iconColor).toEqual(wrapper.vm.themeVariables.red300);
        });

        it('type - success', async () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    type: ALERT_TYPES.SUCCESS,
                },
            });

            let icon = wrapper.findComponent(AlertSuccessIcon);
            expect(wrapper.find(`.${ALERT_WRAPPER_CLASS}`).classes(ALERT_TYPES.SUCCESS)).toBeTruthy();
            expect(icon.exists()).toBeTruthy();
            expect(icon.props().iconColor).toEqual(wrapper.vm.themeVariables.green300);
        });

        it('type - info', async () => {
            let wrapper = getShallowWrapper({
                propsData: {
                    type: ALERT_TYPES.INFO,
                },
            });

            let icon = wrapper.findComponent(AlertInfoIcon);
            expect(wrapper.find(`.${ALERT_WRAPPER_CLASS}`).classes(ALERT_TYPES.INFO)).toBeTruthy();
            expect(wrapper.findComponent(AlertInfoIcon).exists()).toBeTruthy();
            expect(icon.props().iconColor).toEqual(wrapper.vm.themeVariables.blue300);

        });
    });

    describe('show correct slot and slots content', () => {
        it('title', async () => {
            let slotContent = 'Title';
            let wrapper = getShallowWrapper({
                slots: {
                    'title': slotContent,
                },
            });
            let slotWrapper = wrapper.find('.alert__title');

            expect(slotWrapper.exists()).toBeTruthy();
            expect(slotWrapper.text()).toEqual(slotContent);
        });

        it('actions', async () => {
            let slotContent = 'Actions';
            let wrapper = getShallowWrapper({
                slots: {
                    'actions': slotContent,
                },
            });
            let slotWrapper = wrapper.find('.alert__actions');

            expect(slotWrapper.exists()).toBeTruthy();
            expect(slotWrapper.text()).toEqual(slotContent);
        });
    });
});

it('cross click', async () => {
    let wrapper = getShallowWrapper({
        propsData: {
            closable: true,
        },
    });

    await wrapper.find('.alert__cross').trigger('click');

    expectEmitted(wrapper, 'input', false);
});


function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        propsData: {
            value: true,
            type: ALERT_TYPES.WARNING,
        },
        slots: {
            default: defaultSlotContent,
        },
    };
}

function expectDefaultClassesAreCorrect(wrapper: Wrapper<any>) {
    let alertWrapper = wrapper.find(`.${ALERT_WRAPPER_CLASS}`);

    expect(alertWrapper.exists()).toBeTruthy();
    expect(alertWrapper.classes(ALERT_TYPES.WARNING)).toBeTruthy();
    expect(alertWrapper.classes(ALERT_FIXED_CLASS)).toBeFalsy();
}

function expectDefaultIconsAreCorrect(wrapper: Wrapper<any>) {
    expect(wrapper.findComponent(AlertWarningIcon).exists()).toBeTruthy();
    expect(wrapper.findComponent(AlertErrorIcon).exists()).toBeFalsy();
    expect(wrapper.findComponent(AlertInfoIcon).exists()).toBeFalsy();
    expect(wrapper.findComponent(AlertSuccessIcon).exists()).toBeFalsy();
    expect(wrapper.findComponent(AlertTipIcon).exists()).toBeFalsy();
}

function expectDefaultSlotsAreCorrect(wrapper: Wrapper<any>) {
    expect(wrapper.find('.alert__title').exists()).toBeFalsy();
    expect(wrapper.find('.alert__actions').exists()).toBeFalsy();
    expect(wrapper.find('.alert__cross').exists()).toBeFalsy();
    expect(wrapper.find('.alert__content').text()).toEqual(defaultSlotContent);
}
