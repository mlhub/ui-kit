import {mount} from '@cypress/vue';
import * as _ from 'lodash';
import Component from '@/components/pickers/color/colorPicker/ColorPicker.vue';
import {DEFAULT_RGBA_COLOR} from '../../../../../../src/components/pickers/color/colorPicker/constants';
import {RGBAColor} from '../../../../../../src/components/utils/colors/types/RGBAColor';
import RGBAColorParser from '../../../../../../src/utils/colors/RGBAColorParser';

let rgbaColorParser = new RGBAColorParser();

beforeEach(() => {
});

afterEach(() => {
});

it('should render correct layout', () => {
    mountComponent();

    getActivator().should('be.visible');
    getActivator().should('have.attr', 'style', `background: ${rgbaColorParser.parse(DEFAULT_RGBA_COLOR)};`);
    cy.get('.colorPicker').should('not.be.visible');
});

it('should show picker after click', () => {
    let inputListener = cy.stub();

    mountComponent({
        listeners: {
            input: inputListener,
        },
    });

    getActivator().trigger('click');
    cy.get('.colorPicker').should('be.visible');
});

it('should emit @input with new color after color pick', () => {
    let inputListener = cy.spy();
    let newColor: RGBAColor = {
        r: 173,
        g: 136,
        b: 136,
        a: 1,
    };

    mountComponent({
        listeners: {
            input: inputListener,
        },
    });
    getActivator().trigger('click');
    cy.get('.picking-area-overlay2').click(50, 50);
    cy.wait(1).then(() => {
        expect(inputListener).to.be.calledWith(newColor);
    });
});

it('should normal hex color after set value string', () => {
    let newColor = {
        r: '136',
        g: '136',
        b: '136',
        a: '0.50',
    };

    mountComponent({
        propsData: {
            value: newColor,
        },
    });
    getActivator().trigger('click');
    cy.wait(1).then( () => {
        cy.get('.colorPicker .input-field.hex input').should('have.value', '888888');
        cy.get('.colorPicker .input-group div:nth-child(5) input').should('have.value', '50');
    });
});

it('should close if click outside', () => {
    mountComponent();

    getActivator().trigger('click');
    cy.get('body').click(200, 0);
    cy.get('.colorPicker').should('not.be.visible');
});

function mountComponent(additionalOptions: any = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    mount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        propsData: {
            value: DEFAULT_RGBA_COLOR,
        },
    };
}

function getActivator(): any {
    return cy.get('.colorPicker__activator');
}
