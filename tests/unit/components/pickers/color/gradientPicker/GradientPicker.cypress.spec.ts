import {mount} from '@cypress/vue';
import * as _ from 'lodash';
import Component from '@/components/pickers/color/gradientPicker/GradientPicker.vue';
import {DEFAULT_GRADIENT, GRADIENT_TYPE} from '../../../../../../src/components/pickers/color/gradientPicker/constants';
import {Gradient} from '../../../../../../src/components/utils/colors/types/Gradient';
import GradientParser from '../../../../../../src/utils/colors/GradientParser';

let gradientParser = new GradientParser();

beforeEach(() => {
});

afterEach(() => {
});

it('should render correct layout', () => {
    mountComponent();

    getActivator().should('be.visible');
    getActivator().should('have.attr', 'style', `background: ${gradientParser.parse(DEFAULT_GRADIENT)};`);
    cy.get('.colorPicker').should('not.be.visible');
});

it('should emit @input with new value after gradient pick', () => {
    let inputListener = cy.spy();
    let newGradient: Gradient = {
        degree: 0,
        type: GRADIENT_TYPE.LINEAR,
        points: [
            {
                red: 173,
                green: 136,
                blue: 136,
                alpha: 0.99,
                left: 0,
            },
            {
                red: 255,
                green: 0,
                blue: 0,
                alpha: 0.99,
                left: 100,
            },
        ],
    };

    mountComponent({
        listeners: {
            input: inputListener,
        },
    });
    getActivator().trigger('click');
    cy.get('.picking-area-overlay2').click(50, 50);
    cy.wait(1).then(() => {
        expect(inputListener).to.be.calledWith(newGradient);
    });
});

it('should close if click outside', () => {
    mountComponent();

    getActivator().trigger('click');
    cy.get('body').click(200, 0);
    cy.get('.colorPicker').should('not.be.visible');
});


function mountComponent(additionalOptions: any = {}): void {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    mount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        propsData: {
            value: DEFAULT_GRADIENT,
        },
    };
}

function getActivator(): any {
    return cy.get('.colorPicker__activator');
}
