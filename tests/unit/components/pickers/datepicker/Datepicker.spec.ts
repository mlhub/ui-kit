import {config, shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import Component from '../../../../../src/components/pickers/datepicker/Datepicker.vue';
import * as sinon from 'sinon';
import {expectEmitted} from '../../../../utils/expects/componentEventEmitted';

//@TODO overwriting methods via the `methods` property is deprecated
config.showDeprecationWarnings = false;

describe('computed', () => {
    describe('date', () => {
        it('mode - date', () => {
            let value = '2021-11-11';
            let newValue = '2021-12-12';

            let wrapper = getShallowWrapper({
                propsData: {
                    value: value,
                },
            });

            wrapper.vm.date = newValue;
            expectEmitted(wrapper, 'input', newValue);
        });
    });

});

describe('watchers', () => {
});

describe('methods', () => {
    it('cancel', () => {
        let updateValueStub = sinon.stub();
        let hidePopoverStub = sinon.stub();
        let value = '2021-11-11';

        let wrapper = getShallowWrapper({
            propsData: {
                value: value,
            },
            computed: {
                picker: () => {
                    return {
                        updateValue: updateValueStub,
                    };
                },
            },
            methods: {
                hidePopover: hidePopoverStub,
            },
        });

        wrapper.vm.cancel();

        sinon.assert.calledOnce(hidePopoverStub);
        sinon.assert.calledWith(updateValueStub, value, {
            hidePopover: true,
        });
    });

    it('hidePopover', () => {
        let inputOnBlurStub = sinon.stub();
        let inputStub = () => {
            return {
                onBlur: inputOnBlurStub,
            };
        };

        let wrapper = getShallowWrapper({
            data() {
                return {
                    popoverIsActive: true,
                };
            },
            computed: {
                input: inputStub,
            },
        });

        wrapper.vm.hidePopover();

        expect(wrapper.vm.popoverIsActive).toEqual(false);
        sinon.assert.calledOnce(inputOnBlurStub);
    });
});

describe('validation', () => {
});

describe('lifecycle hooks', () => {

});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    let setLocaleStub = sinon.stub();

    return {
        propsData: {
            value: '2021-11-11',
        },
        methods: {
            setLocale: setLocaleStub,
        },
    };
}
