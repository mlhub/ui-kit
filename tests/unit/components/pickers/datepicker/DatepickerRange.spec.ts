import {config, shallowMount, Wrapper} from '@vue/test-utils';
import _ from 'lodash';
import Component from '../../../../../src/components/pickers/datepicker/DatepickerRange.vue';
import * as sinon from 'sinon';
import {expectEmitted} from '../../../../utils/expects/componentEventEmitted';
import moment from 'moment';
import {DATE_FORMAT, MOBILE_HEADER_DATE_FORMAT} from '../../../../../src/components/pickers/datepicker/constants';

//@TODO overwriting methods via the `methods` property is deprecated
config.showDeprecationWarnings = false;

describe('computed', () => {
    it('range', () => {
        let value = {
            start: '2021-10-10',
            end: '2021-11-20',
        };
        let newValue = {
            start: '2022-10-10',
            end: '2022-11-20',
        };

        let wrapper = getShallowWrapper({
            propsData: {
                value: value,
            },
        });

        wrapper.vm.range = newValue;

        expectEmitted(wrapper, 'input', newValue);
    });

    describe('mobileHeaderText', () => {
        it('datesWasSet true', () => {
            let temporaryDates = {
                start: '2021-11-03',
                end: '2021-11-05',
            };

            let result = moment(temporaryDates.start, DATE_FORMAT).format(MOBILE_HEADER_DATE_FORMAT) + ' - ' + moment(temporaryDates.end, DATE_FORMAT).format(MOBILE_HEADER_DATE_FORMAT);

            let datesWasSetStub = sinon.stub().returns(true);

            let wrapper = getShallowWrapper({
                data() {
                    return {
                        temporaryDates: temporaryDates,
                    };
                },
                computed: {
                    datesWasSet: datesWasSetStub,
                },
            });

            expect(wrapper.vm.mobileHeaderText).toEqual(result);
        });

        it('temporaryDate false', () => {
            let placeholder = 'dd/mm/yyyy - dd/mm/yyyy';

            let datesWasSetStub = sinon.stub().returns(false);

            let wrapper = getShallowWrapper({
                computed: {
                    datesWasSet: datesWasSetStub,
                },
            });

            expect(wrapper.vm.mobileHeaderText).toEqual(placeholder);
        });
    });

    describe('datesWasSet', () => {
        it('true', () => {
            let temporaryDates = {
                start: '2021-11-03',
                end: '2021-11-05',
            };

            let wrapper = getShallowWrapper({
                data() {
                    return {
                        temporaryDates: temporaryDates,
                    };
                },
            });

            expect(wrapper.vm.datesWasSet).toEqual(true);
        });

        it('false', () => {
            let temporaryDates = {
                start: '',
                end: '',
            };

            let wrapper = getShallowWrapper({
                data() {
                    return {
                        temporaryDates: temporaryDates,
                    };
                },
            });

            expect(wrapper.vm.datesWasSet).toEqual(false);
        });
    });
});

describe('watchers', () => {
});

describe('methods', () => {
    it('setTemporaryDates', () => {
        let temporaryDates = {
            start: '2021-11-11',
            end: '2021-12-12',
        };

        let wrapper = getShallowWrapper();

        wrapper.vm.setTemporaryDates(temporaryDates);

        expect(wrapper.vm.temporaryDates).toEqual(temporaryDates);
    });

    it('cancel', () => {
        let range = {
            start: '2021-11-11',
            end: '2021-12-12',
        };
        let value = {
            start: '2021-11-11',
            end: '2021-12-12',
        };
        let updateValueStub = sinon.stub();
        let hidePopoverStub = sinon.stub();
        let rangeStub = sinon.stub().returns(range);

        let wrapper = getShallowWrapper({
            propsData: {
                value: value,
            },
            computed: {
                range: rangeStub,
                picker: () => {
                    return {
                        updateValue: updateValueStub,
                    };
                },
            },
            methods: {
                hidePopover: hidePopoverStub,
            },
        });

        wrapper.vm.cancel();

        sinon.assert.calledOnce(hidePopoverStub);
        sinon.assert.calledWith(updateValueStub, range, {
            hidePopover: true,
        });
    });

    it('hidePopover', () => {
        let inputStartOnBlurStub = sinon.stub();
        let inputEndOnBlurStub = sinon.stub();

        let inputStartStub = () => {
            return {
                onBlur: inputStartOnBlurStub,
            };
        };
        let inputEndStub = () => {
            return {
                onBlur: inputEndOnBlurStub,
            };
        };

        let wrapper = getShallowWrapper({
            data() {
                return {
                    popoverIsActive: true,
                };
            },
            computed: {
                inputStart: inputStartStub,
                inputEnd: inputEndStub,
            },
        });

        wrapper.vm.hidePopover();

        expect(wrapper.vm.popoverIsActive).toEqual(false);
        sinon.assert.calledOnce(inputStartOnBlurStub);
        sinon.assert.calledOnce(inputEndOnBlurStub);
    });
});

describe('validation', () => {
});

describe('lifecycle hooks', () => {

});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    let setLocaleStub = sinon.stub();

    return {
        propsData: {
            value: {
                start: '2021-11-11',
                end: '2021-11-22',
            },
        },
        methods: {
            setLocale: setLocaleStub,
        },
    };
}
