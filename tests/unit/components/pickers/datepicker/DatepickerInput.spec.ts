import {shallowMount, Wrapper} from '@vue/test-utils';
import Component from '../../../../../src/components/pickers/datepicker/DatepickerInput.vue';
import _ from 'lodash';
import {expectEmitted} from '../../../../utils/expects/componentEventEmitted';

describe('computed', () => {

});

describe('watchers', () => {
});

describe('methods', () => {
    it('onInput', () => {
        let event = {};
        let wrapper = getShallowWrapper();

        wrapper.vm.onInput(event);

        expectEmitted(wrapper, 'input', event);
    });
});

describe('validation', () => {
});

describe('lifecycle hooks', () => {

});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(Component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        propsData:{
            value:'11-11-2021',
        },
    };
}
