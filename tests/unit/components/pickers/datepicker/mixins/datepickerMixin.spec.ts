import {shallowMount, Wrapper, config} from '@vue/test-utils';
import _ from 'lodash';
import datepickerMixin from '../../../../../../src/components/pickers/datepicker/mixins/datepickerMixin';
import moment from 'moment';

import {
    DEFAULT_DATEPICKER_CLASS,
    DESKTOP_TRANSITION,
    MOBILE_TRANSITION,
    POPOVER_IS_ACTIVE_CLASS,
} from '../../../../../../src/components/pickers/datepicker/constants';
import * as sinon from 'sinon';

//@TODO overwriting methods via the `methods` property is deprecated
config.showDeprecationWarnings = false;

const component: any = {
    template: `
        <div>
        <DatePicker :value="'11/11/2021'" ref="picker"/>
        </div>
    `,
    mixins: [
        datepickerMixin,
    ],
};

describe('computed', () => {
    describe('minDate', () => {
        it('disablePreviousDates === true', () => {
            let result = moment(new Date()).format('DD MM YYYY hh:mm');

            let wrapper = getShallowWrapper({
                propsData: {
                    disablePreviousDates: true,
                },
            });

            expect(moment(wrapper.vm.minDate).format('DD MM YYYY hh:mm')).toEqual(result);
        });

        it('disablePreviousDates === false', () => {
            let result = null;

            let wrapper = getShallowWrapper({
                propsData: {
                    disablePreviousDates: false,
                },
            });

            expect(wrapper.vm.minDate).toEqual(result);
        });
    });

    describe('pickerClass', () => {
        it('popoverIsActive', () => {
            let wrapper = getShallowWrapper({
                data() {
                    return {
                        popoverIsActive: true,
                    };
                },
            });

            expect(wrapper.vm.pickerClass).toEqual(
                {
                    [DEFAULT_DATEPICKER_CLASS]: true,
                    [POPOVER_IS_ACTIVE_CLASS]: true,
                });
        });

        it('!popoverIsActive', () => {
            let wrapper = getShallowWrapper();

            expect(wrapper.vm.pickerClass).toEqual(
                {
                    [DEFAULT_DATEPICKER_CLASS]: true,
                    [POPOVER_IS_ACTIVE_CLASS]: false,
                });
        });
    });

    describe('columns', () => {
        it('isVertical', () => {
            let result = 1;
            let wrapper = getShallowWrapper({
                propsData: {
                    isVertical: true,
                },
            });

            expect(wrapper.vm.columns).toEqual(result);
        });

        it('!isVertical', () => {
            let result = 2;
            let wrapper = getShallowWrapper({
                propsData: {
                    isVertical: false,
                },
            });

            expect(wrapper.vm.columns).toEqual(result);
        });

        it('isMobile', () => {
            let result = 1;
            let isMobileStub = sinon.stub().returns(true);

            let wrapper = getShallowWrapper({
                propsData: {
                    isVertical: false,
                },
                computed: {
                    isMobile: isMobileStub,
                },
            });

            expect(wrapper.vm.columns).toEqual(result);
        });
    });

    describe('rows', () => {
        it('isVertical', () => {
            let result = 2;
            let wrapper = getShallowWrapper({
                propsData: {
                    isVertical: true,
                },
            });

            expect(wrapper.vm.rows).toEqual(result);
        });

        it('!isVertical', () => {
            let result = 1;
            let wrapper = getShallowWrapper({
                propsData: {
                    isVertical: false,
                },
            });

            expect(wrapper.vm.rows).toEqual(result);
        });

        it('isMobile', () => {
            let result = 2;
            let isMobileStub = sinon.stub().returns(true);

            let wrapper = getShallowWrapper({
                propsData: {
                    isVertical: false,
                },
                computed: {
                    isMobile: isMobileStub,
                },
            });

            expect(wrapper.vm.rows).toEqual(result);
        });
    });

    it('picker', () => {
        let wrapper = getShallowWrapper();

        expect(wrapper.vm.picker).not.toBeUndefined();
    });
});

describe('watchers', () => {
});

describe('methods', () => {
    describe('openPopover', () => {
        it('!disabled (mobile)', () => {
            let isMobileStub = sinon.stub().returns(true);
            let showPopoverStub = sinon.stub();
            let wrapper = getShallowWrapper({
                computed: {
                    isMobile: isMobileStub,
                },
            });

            wrapper.vm.openPopover(showPopoverStub);

            sinon.assert.calledOnce(showPopoverStub);
            sinon.assert.calledWith(showPopoverStub, {
                transition: MOBILE_TRANSITION,
            });
        });

        it('!disabled (desktop)', () => {
            let isMobileStub = sinon.stub().returns(false);
            let showPopoverStub = sinon.stub();
            let wrapper = getShallowWrapper({
                computed: {
                    isMobile: isMobileStub,
                },
            });

            wrapper.vm.openPopover(showPopoverStub);

            sinon.assert.calledOnce(showPopoverStub);
            sinon.assert.calledWith(showPopoverStub, {
                transition: DESKTOP_TRANSITION,
            });
        });

        it('disabled', () => {
            let isMobileStub = sinon.stub().returns(false);
            let showPopoverStub = sinon.stub();
            let wrapper = getShallowWrapper({
                propsData: {
                    disabled: true,
                },
                computed: {
                    isMobile: isMobileStub,
                },
            });

            wrapper.vm.openPopover(showPopoverStub);

            sinon.assert.notCalled(showPopoverStub);
        });
    });

    // todo: finish moment.locale test
    // it('setLocale', () => {
    //     // let lang = 'ru';
    //     let picker = {
    //         $locale: {
    //             id: 'en-US',
    //         },
    //     };
    //     let momentStub = sinon.stub(moment);
    //     let localeStub = momentStub.locale;
    //
    //     let wrapper = getShallowWrapper({
    //         computed: {
    //             picker: () => {
    //                 return picker;
    //             },
    //         },
    //     });
    //
    //     wrapper.vm.setLocale(wrapper.vm.picker.$locale.id);
    //     sinon.assert.calledTwice(localeStub);
    // });

    describe('displayedValue', () => {
        it('with value', () => {
            let value = '22/12/2021';

            let wrapper = getShallowWrapper();

            expect(wrapper.vm.displayedValue(value)).toEqual(value);
        });

        it('without value', () => {
            let value = '';
            let result = '';

            let wrapper = getShallowWrapper();

            expect(wrapper.vm.displayedValue(value)).toEqual(result);
        });
    });
});

describe('validation', () => {
});

describe('lifecycle hooks', () => {
    it('mounted', () => {
        let setLocaleStub = sinon.stub();
        getShallowWrapper({
            methods: {
                setLocale: setLocaleStub,
            },
        });
        sinon.assert.calledOnce(setLocaleStub);
    });
});

function getShallowWrapper(additionalOptions: any = {}): Wrapper<any> {
    let options = _.merge(
        getDefaultWrapperOptions(),
        additionalOptions,
    );
    return shallowMount(component, options);
}

function getDefaultWrapperOptions(): any {
    return {
        stubs: {
            'DatePicker': {
                render: () => {
                },
            },
        },
    };
}
