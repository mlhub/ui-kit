import {AnimationFactory} from './AnimationFactory';
import {AnimationCustomizationPatternFactory} from './AnimationCustomizationPatternFactory';
import {AnimationCustomizationApplier} from '@/utils/jsonAnimations/AnimationCustomizationApplier';
import defaultCustomizationColors from '@/utils/jsonAnimations/defaultCustomizationColors';

let animationFactory = new AnimationFactory();
let animationCustomizationPatternFactory = new AnimationCustomizationPatternFactory();
let applier = new AnimationCustomizationApplier();

it('should not change layers that should not be changed', () => {
    let layerName = 'not changed layer';
    let animation = animationFactory.generateWithLayer(layerName);
    let pattern = animationCustomizationPatternFactory.generate();

    let result = applier.apply(animation, pattern);

    expect(result).toEqual(animation);
});

it('should change layers that has pattern fields names', () => {
    let pattern = animationCustomizationPatternFactory.generate();
    let animation = animationFactory.generateEmpty();

    for (const key in pattern) {
        animation.layers.push(animationFactory.generateLayer(key));
    }

    let result = applier.apply(animation, pattern);

    expectAnimationFieldsExceptLayersRemainTheSame(animation, result);
    expect(result.layers.length).toEqual(animation.layers.length);
    expect(result.layers).not.toEqual(animation.layers);
    expectAnimationLayersHaveTheSameNames(animation, result);
});

it('should change layers that has names from default customization color set', () => {
    let pattern = animationCustomizationPatternFactory.generate();
    let animation = animationFactory.generateEmpty();

    for (const name in defaultCustomizationColors) {
        animation.layers.push(animationFactory.generateLayer(name));
    }

    let result = applier.apply(animation, pattern);

    expectAnimationFieldsExceptLayersRemainTheSame(animation, result);
    expect(result.layers.length).toEqual(animation.layers.length);
    expect(result.layers).not.toEqual(animation.layers);
    expectAnimationLayersHaveTheSameNames(animation, result);
});

function expectAnimationFieldsExceptLayersRemainTheSame(originalAnimation: any, newAnimation: any): void {
    for (const key in newAnimation) {
        if (key === 'layers') {
            continue;
        }

        expect(newAnimation[key]).toEqual(originalAnimation[key]);
    }
}

function expectAnimationLayersHaveTheSameNames(originalAnimation: any, newAnimation: any): void {
    for (const key in newAnimation.layers) {
        expect(newAnimation.layers[key].nm).toEqual(originalAnimation.layers[key].nm);
    }
}