export class AnimationFactory {
    public generateWithLayer(layerName: string): any {
        let animation = this.generateEmpty();
        animation.layers.push(this.generateLayer(layerName));
        return animation;
    }

    public generateLayer(layerName: string): any {
        return {
            'ddd': 0,
            'ind': 3,
            'ty': 4,
            'nm': layerName,
            'parent': 1,
            'sr': 1,
            'ks': {
                'o': {
                    'a': 0,
                    'k': 100,
                    'ix': 11,
                },
                'r': {
                    'a': 0,
                    'k': 0,
                    'ix': 10,
                },
                'p': {
                    'a': 0,
                    'k': [
                        47.382,
                        42.697,
                        0,
                    ],
                    'ix': 2,
                },
                'a': {
                    'a': 0,
                    'k': [
                        0,
                        0,
                        0,
                    ],
                    'ix': 1,
                },
                's': {
                    'a': 0,
                    'k': [
                        100,
                        100,
                        100,
                    ],
                    'ix': 6,
                },
            },
            'ao': 0,
            'shapes': [
                {
                    'ty': 'gr',
                    'it': [
                        {
                            'ind': 0,
                            'ty': 'sh',
                            'ix': 1,
                            'ks': {
                                'a': 0,
                                'k': {
                                    'i': [
                                        [
                                            0,
                                            -1.98,
                                        ],
                                        [
                                            0,
                                            0,
                                        ],
                                        [
                                            0,
                                            0,
                                        ],
                                        [
                                            3.19,
                                            0.78,
                                        ],
                                    ],
                                    'o': [
                                        [
                                            -0.01,
                                            1.9,
                                        ],
                                        [
                                            0,
                                            0,
                                        ],
                                        [
                                            0,
                                            0,
                                        ],
                                        [
                                            -4.84,
                                            -1.19,
                                        ],
                                    ],
                                    'v': [
                                        [
                                            -11.378,
                                            -2.616,
                                        ],
                                        [
                                            -11.378,
                                            2.714,
                                        ],
                                        [
                                            11.382,
                                            3.694,
                                        ],
                                        [
                                            7.462,
                                            -2.276,
                                        ],
                                    ],
                                    'c': true,
                                },
                                'ix': 2,
                            },
                            'nm': 'Path 1',
                            'mn': 'ADBE Vector Shape - Group',
                            'hd': false,
                        },
                        {
                            'ty': 'fl',
                            'c': {
                                'a': 0,
                                'k': [
                                    0.270588248968,
                                    0.274509817362,
                                    0.290196090937,
                                    1,
                                ],
                                'ix': 4,
                            },
                            'o': {
                                'a': 0,
                                'k': 100,
                                'ix': 5,
                            },
                            'r': 1,
                            'bm': 0,
                            'nm': 'Fill 1',
                            'mn': 'ADBE Vector Graphic - Fill',
                            'hd': false,
                        },
                        {
                            'ty': 'tr',
                            'p': {
                                'a': 0,
                                'k': [
                                    0,
                                    0,
                                ],
                                'ix': 2,
                            },
                            'a': {
                                'a': 0,
                                'k': [
                                    0,
                                    0,
                                ],
                                'ix': 1,
                            },
                            's': {
                                'a': 0,
                                'k': [
                                    100,
                                    100,
                                ],
                                'ix': 3,
                            },
                            'r': {
                                'a': 0,
                                'k': 0,
                                'ix': 6,
                            },
                            'o': {
                                'a': 0,
                                'k': 100,
                                'ix': 7,
                            },
                            'sk': {
                                'a': 0,
                                'k': 0,
                                'ix': 4,
                            },
                            'sa': {
                                'a': 0,
                                'k': 0,
                                'ix': 5,
                            },
                            'nm': 'Transform',
                        },
                    ],
                    'nm': 'additional',
                    'np': 2,
                    'cix': 2,
                    'bm': 0,
                    'ix': 1,
                    'mn': 'ADBE Vector Group',
                    'hd': false,
                },
            ],
            'ip': 0,
            'op': 600,
            'st': 0,
            'bm': 0,
        };
    }

    public generateEmpty(): any {
        return {
            'v': '5.6.3',
            'fr': 30,
            'ip': 0,
            'op': 290,
            'w': 750,
            'h': 500,
            'nm': '750 × 503',
            'ddd': 0,
            'assets': [],
            'layers': [],
            'markers': [],
        };
    }
}