import {AnimationCustomizationPattern} from '@/utils/jsonAnimations/AnimationCustomizationPattern';
import _ from 'lodash';
import {faker} from '@faker-js/faker';

export class AnimationCustomizationPatternFactory {
    public generate(overrides: Partial<AnimationCustomizationPattern> = {}): AnimationCustomizationPattern {
        let result: AnimationCustomizationPattern = {
            brand: faker.color.rgb(),
            middle: faker.color.rgb(),
            additional: faker.color.rgb(),
            background: faker.color.rgb(),
        };
        _.assign(result, overrides);
        return result;
    }
}