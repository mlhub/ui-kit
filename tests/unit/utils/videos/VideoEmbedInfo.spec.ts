import {EMBED_VIDEO_TYPE, VideoEmbedInfo} from '@/utils/videos/VideoEmbedInfo';

it('should show error in constructor (wrong embed type)', () => {
    let url = 'https://www.dailymotion.com/video/x8bfx7a?playlist=x6lgtp';

    expect(() => {
        new VideoEmbedInfo(url);
    }).toThrowError('url is not embed');
});

describe('should show correct info', () => {

    it('youtube', () => {
        let url = 'https://www.youtube.com/watch?v=bdPZ2Cu1vNU&list=PL4sw7qqmVm0-L7wlQ-FwdTMFx9NLq5vGA';
        let videoEmbedInfo = new VideoEmbedInfo(url);

        expect(videoEmbedInfo.id).toEqual('bdPZ2Cu1vNU');
        expect(videoEmbedInfo.type).toEqual(EMBED_VIDEO_TYPE.YOUTUBE);
    });

    it('vimeo', () => {
        let url = 'https://vimeo.com/28164488';
        let videoEmbedInfo = new VideoEmbedInfo(url);

        expect(videoEmbedInfo.id).toEqual('28164488');
        expect(videoEmbedInfo.type).toEqual(EMBED_VIDEO_TYPE.VIMEO);
    });
});