import GradientParser from '../../../../src/utils/colors/GradientParser';
import {Gradient} from '../../../../src/components/utils/colors/types/Gradient';
import {GRADIENT_TYPE} from '../../../../src/components/pickers/color/gradientPicker/constants';

let gradientParser = new GradientParser();

it('should parse all values correctly with linear type', () => {
    let gradient: Gradient = {
        type: GRADIENT_TYPE.LINEAR,
        degree: 0,
        points: [
            {
                left: 0,
                red: 173,
                green: 143,
                blue: 143,
                alpha: 0.99,
            },
            {
                left: 100,
                red: 255,
                green: 0,
                blue: 0,
                alpha: 0.99,
            },
        ],
    };

    let expectedResult = `linear-gradient(${gradient.degree}deg, rgba(${gradient.points[0].red}, ${gradient.points[0].green}, ${gradient.points[0].blue}, ${gradient.points[0].alpha}) ${gradient.points[0].left}%, rgba(${gradient.points[1].red}, ${gradient.points[1].green}, ${gradient.points[1].blue}, ${gradient.points[1].alpha}) ${gradient.points[1].left}%)`;
    let result = gradientParser.parse(gradient);

    expect(result).toEqual(expectedResult);
});

it('should parse all values correctly with radial type', () => {
    let gradient: Gradient = {
        type: GRADIENT_TYPE.RADIAL,
        degree: 0,
        points: [
            {
                left: 0,
                red: 173,
                green: 143,
                blue: 143,
                alpha: 0.99,
            },
            {
                left: 100,
                red: 255,
                green: 0,
                blue: 0,
                alpha: 0.99,
            },
        ],
    };

    let expectedResult = `radial-gradient( rgba(${gradient.points[0].red}, ${gradient.points[0].green}, ${gradient.points[0].blue}, ${gradient.points[0].alpha}) ${gradient.points[0].left}%, rgba(${gradient.points[1].red}, ${gradient.points[1].green}, ${gradient.points[1].blue}, ${gradient.points[1].alpha}) ${gradient.points[1].left}%)`;
    let result = gradientParser.parse(gradient);

    expect(result).toEqual(expectedResult);
});
