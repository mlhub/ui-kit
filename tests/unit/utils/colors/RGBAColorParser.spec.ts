import RGBAColorParser from '../../../../src/utils/colors/RGBAColorParser';
import {RGBAColorFactory} from '../../../../src/utils/factories/RGBAColorFactory';

let rgbaColorParser = new RGBAColorParser();
let rgbaColorFactory = new RGBAColorFactory();

it('should parse all values correctly rgba', () => {
    let color = rgbaColorFactory.generate({a: 0.5});
    let expectedResult = `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a})`;
    let result = rgbaColorParser.parse(color);

    expect(result).toEqual(expectedResult);
});

it('should parse all values correctly rgb', () => {
    let color = rgbaColorFactory.generate();
    let expectedResult = `rgb(${color.r}, ${color.g}, ${color.b})`;
    let result = rgbaColorParser.parse(color);

    expect(result).toEqual(expectedResult);
});
