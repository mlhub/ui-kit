/* eslint-disable arrow-body-style */
// https://docs.cypress.io/guides/guides/plugins-guide.html

// if you need a custom webpack configuration you can uncomment the following import
// and then use the `file:preprocessor` event
// as explained in the cypress docs
// https://docs.cypress.io/api/plugins/preprocessors-api.html#Examples

// /* eslint-disable import/no-extraneous-dependencies, global-require */
// const webpack = require('@cypress/webpack-preprocessor')

import {startDevServer} from '@cypress/webpack-dev-server';

export default (on, config) => {
    if (config.testingType === 'component') {
        // Vue's Webpack configuration
        const webpackConfig = require('@vue/cli-service/webpack.config.js');
        on('dev-server:start', (options) => {
            return startDevServer({
                options,
                webpackConfig,
            });
        });
    }

    return Object.assign({}, config, {
        fixturesFolder: 'tests/e2e/fixtures',
        integrationFolder: 'tests/e2e/specs',
        screenshotsFolder: 'tests/e2e/screenshots',
        videosFolder: 'tests/e2e/videos',
        supportFile: 'tests/e2e/support/index.ts',
    });
};
