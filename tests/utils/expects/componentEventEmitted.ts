import {Wrapper} from '@vue/test-utils';

export function expectEmitted(wrapper: Wrapper<any>, eventName: string, emittedData: any = null): void {
    expect(wrapper.emitted(eventName)).not.toBeUndefined();
    expect(wrapper.emitted(eventName)![0]).not.toBeUndefined();

    if (emittedData !== null && emittedData !== undefined) {
        expect(wrapper.emitted(eventName)![0][0]).toEqual(emittedData);
    }
}

export function expectNotEmitted(wrapper: Wrapper<any>, eventName: string): void {
    expect(wrapper.emitted(eventName)).toBeUndefined();
}
