import {SinonStub} from 'cypress/types/sinon';
// @ts-ignore
import * as Cypress from 'cypress';

export function waitForStubCall(stub: SinonStub, callsNumber = 1, timeout = 4000): Promise<void> {
    if (callsNumber < 1) {
        throw new Error('Argument callsNumber should be equal or greater than 1');
    }
    return new Promise(((resolve) => {
        let timeStart = Date.now();
        let interval = setInterval(() => {
            if (stub.callCount === callsNumber) {
                clearInterval(interval);
                resolve();
            }
            if ((Date.now() - timeStart) >= timeout) {
                clearInterval(interval);
                throw new Error(`Waiting for stub failed, timeout ${timeout}ms exceeded, expected calls: ${callsNumber}, received: ${stub.callCount}`);
            }
        }, 10);
    }));
}

export function waitForStubCallCypress(stub: SinonStub, callsNumber = 1, timeout = 4000): Cypress.Chainable {
    return cy.then(() => waitForStubCall(stub, callsNumber, timeout));
}
