import * as _ from 'lodash';
import type {PaginationResponseData} from '@/types/PaginatedListApiResponse';
import {PartialDeep} from 'type-fest';

export default class PaginationResponseFactory {
    public generate(overrides: PartialDeep<PaginationResponseData> = {}): PaginationResponseData {
        let result = {
            data: [],
            links: {
                first: 'https://cb.web-developers.in.ua/api/medialibrary/get_folders?page=1',
                last: 'https://cb.web-developers.in.ua/api/medialibrary/get_folders?page=1',
                next: 'https://cb.web-developers.in.ua/api/medialibrary/get_folders?page=1',
                prev: 'https://cb.web-developers.in.ua/api/medialibrary/get_folders?page=1',
            },
            meta: {
                current_page: 1,
                from: 1,
                last_page: 1,
                path: 'https://cb.web-developers.in.ua/api/medialibrary/get_folders',
                per_page: 15,
                to: 2,
                total: 2,
            },
        };

        result = _.merge(result, overrides);

        return result;
    }
}
