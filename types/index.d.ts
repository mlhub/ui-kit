import {PartialDeep} from "type-fest";

export * from "../src/components/index";
export * from "../src/types/index";

export type UiKitOptions = {
    mqOptions: {
        breakpoints: {
            extraSmall: number,
            verySmall: number,
            small: number,
            vuetifyBreakpoint: number,
            medium: number,
            large: number,
            notebook: number,
            extraLarge: number,
        },
    },
};
declare const _default: {
    install(Vue: any, options?: PartialDeep<UiKitOptions>): void;
    setUpMq(Vue: any, options?: PartialDeep<UiKitOptions>): void;
    setUpLineClamp(Vue: any): void;
    setUpToasts(Vue: any): void;
};
export default _default;
