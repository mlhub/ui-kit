module.exports = {
    'env': {
        'browser': true,
        'es2021': true,
        'commonjs': true,
    },
    'extends': [
        'eslint:recommended',
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:vue/essential',
    ],
    'parserOptions': {
        'ecmaVersion': 12,
        'parser': '@typescript-eslint/parser',
        'sourceType': 'module',
    },
    'ignorePatterns': ['dev/*'],
    'plugins': [
        'vue',
        '@typescript-eslint',
        'jsdoc',
    ],
    'rules': {
        'no-useless-escape': 'off',
        'no-undef': 'off',
        '@typescript-eslint/no-var-requires': 'off',
        'semi': 'warn',
        'no-debugger': 'warn',
        'comma-dangle': ['warn', 'always-multiline'],
        'vue/no-unused-vars': 'off',
        'prefer-const': 'off',
        'quotes': ['warn', 'single'],
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/naming-convention': 'off',
        '@typescript-eslint/explicit-module-boundary-types': 'off',
        '@typescript-eslint/ban-ts-comment': 'off',
        '@typescript-eslint/no-empty-function': 'off',
        '@typescript-eslint/no-non-null-assertion': 'off',
        'no-dupe-else-if': 'off',
        'no-prototype-builtins': 'off',
        'vue/multi-word-component-names': 'off',
        'vue/order-in-components': ['warn', {
            'order': [
                'el',
                'name',
                'key',
                'extends',
                'mixins',
                'parent',
                'functional',
                ['delimiters', 'comments'],
                ['components', 'directives', 'filters'],
                ['provide', 'inject'],
                'ROUTER_GUARDS',
                'layout',
                'middleware',
                'validate',
                'scrollToTop',
                'transition',
                'loading',
                'inheritAttrs',
                'model',
                ['props', 'propsData'],
                'emits',
                'setup',
                'asyncData',
                'data',
                'fetch',
                'head',
                'computed',
                'watch',
                'watchQuery',
                'methods',
                ['template', 'render'],
                'renderError',
                'LIFECYCLE_HOOKS',
            ],
        }],
    },
};
