export const AVATAR_CLASS = 'avatar';

export enum AVATAR_SIZE {
    EXTRA_SMALL = 'extraSmall',
    SMALL = 'small',
    MEDIUM = 'medium',
    LARGE = 'large',
    EXTRA_LARGE = 'extraLarge',
}
