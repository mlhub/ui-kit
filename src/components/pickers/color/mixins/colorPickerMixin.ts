import Vue from 'vue';
import vClickOutside from 'v-click-outside';
import {ColorPicker} from 'vue-color-gradient-picker';


export default Vue.extend({
    directives: {
        clickOutside: vClickOutside.directive,
    },
    components: {
        ColorPicker,
    },
    props: {
        label: {
            type: String,
            default: '',
        },
        labelWithoutColon: {
            type: Boolean,
            default: false,
        },
    },
    data() {
        return {
            pickerIsVisible: false,
        };
    },
    methods: {
        closePicker(): void {
            this.pickerIsVisible = false;
        },
        showPicker(): void {
            this.pickerIsVisible = true;
        },
    },
});
