export enum GRADIENT_TYPE {
    LINEAR = 'linear',
    RADIAL = 'radial',
}

export const DEFAULT_GRADIENT = {
    type: GRADIENT_TYPE.LINEAR,
    degree: 0,
    points: [
        {
            left: 0,
            red: 0,
            green: 0,
            blue: 0,
            alpha: 0.99,
        },
        {
            left: 100,
            red: 255,
            green: 0,
            blue: 0,
            alpha: 0.99,
        },
    ],
};
