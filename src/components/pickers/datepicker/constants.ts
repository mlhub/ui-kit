export const DATE_FORMAT = 'YYYY-MM-DD';
export const TIME_FORMAT_24 = 'HH:mm:ss';
export const TIME_FORMAT_12 = 'hh:mm a';
export const DATE_TIME_FORMAT = `${DATE_FORMAT} ${TIME_FORMAT_24}`;

export const DISPLAYED_DATE_FORMAT = 'DD/MM/YYYY';
export const DISPLAYED_TIME_FORMAT_24 = 'HH:mm';
export const DISPLAYED_DATE_TIME_FORMAT_24 = `${DISPLAYED_DATE_FORMAT} ${DISPLAYED_TIME_FORMAT_24}`;
export const DISPLAYED_DATE_TIME_FORMAT_12 = `${DISPLAYED_DATE_FORMAT} ${TIME_FORMAT_12}`;
export const DISPLAYED_WEEKDAYS_FORMAT = 'WWW';

export const MOBILE_HEADER_DATE_FORMAT = 'MMM D, YYYY';
export const MOBILE_HEADER_DATE_TIME_FORMAT_24 = `${MOBILE_HEADER_DATE_FORMAT} ${DISPLAYED_TIME_FORMAT_24}`;
export const MOBILE_HEADER_DATE_TIME_FORMAT_12 = `${MOBILE_HEADER_DATE_FORMAT} ${TIME_FORMAT_12}`;

export const DEFAULT_DATE_PLACEHOLDER = 'dd/mm/yyyy';
export const DEFAULT_DATE_TIME_PLACEHOLDER = `${DEFAULT_DATE_PLACEHOLDER} ${DISPLAYED_TIME_FORMAT_24}`;

export const DEFAULT_DATEPICKER_CLASS = 'datepicker__wrapper';
export const POPOVER_IS_ACTIVE_CLASS = 'popoverIsActive';

export type RangeDateObject = {
    start: string,
    end: string,
};

export const DESKTOP_TRANSITION = 'fade';
export const MOBILE_TRANSITION = 'none';
