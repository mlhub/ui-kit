import {
    DATE_TIME_FORMAT,
    DEFAULT_DATE_PLACEHOLDER,
    DEFAULT_DATEPICKER_CLASS,
    DESKTOP_TRANSITION,
    DISPLAYED_DATE_FORMAT,
    DISPLAYED_DATE_TIME_FORMAT_12,
    DISPLAYED_DATE_TIME_FORMAT_24,
    DISPLAYED_WEEKDAYS_FORMAT,
    MOBILE_TRANSITION,
    POPOVER_IS_ACTIVE_CLASS,
} from '../constants';
import mediaQueriesMixin from '../../../../mixins/mediaQueriesMixin';
import mixins from 'vue-typed-mixins';
import moment from 'moment';

export default mixins(mediaQueriesMixin).extend({
    mixins: [mediaQueriesMixin],
    props: {
        disablePreviousDates: {
            type: Boolean,
            default: false,
        },
        placeholder: {
            type: String,
            default: DEFAULT_DATE_PLACEHOLDER,
        },
        disabled: {
            type: Boolean,
            default: false,
        },
        isVertical: {
            type: Boolean,
            default: false,
        },
        locale: {
            type: String,
            default: 'en-US',
        },
        label: String,
        labelWithoutColon: {
            type: Boolean,
            default: false,
        },
    },
    data() {
        return {
            masks: {
                L: DISPLAYED_DATE_FORMAT,
                weekdays: DISPLAYED_WEEKDAYS_FORMAT,
                data: DISPLAYED_DATE_FORMAT,
                input: DISPLAYED_DATE_FORMAT,
                inputDateTime: DISPLAYED_DATE_TIME_FORMAT_12,
                inputDateTime24hr: DISPLAYED_DATE_TIME_FORMAT_24,
            },
            modelConfig: {
                type: 'string',
                mask: DATE_TIME_FORMAT,
            },
            popover: {
                visibility: 'click',
                positionFixed: true,
            },
            debounce: 100,
            popoverIsActive: false,
        };
    },
    computed: {
        minDate(): Date | null {
            return this.disablePreviousDates ? new Date() : null;
        },
        pickerClass(): any {
            return {
                [DEFAULT_DATEPICKER_CLASS]: true,
                [POPOVER_IS_ACTIVE_CLASS]: this.popoverIsActive,
            };
        },
        columns(): number {
            return this.isVertical || this.isMobile ? 1 : 2;
        },
        rows(): number {
            return this.isVertical || this.isMobile ? 2 : 1;
        },
        picker(): any {
            return this.$refs.picker;
        },
    },
    methods: {
        openPopover(showPopover: (transition: any) => void): void {
            if (!this.disabled) {
                showPopover({transition: this.isMobile ? MOBILE_TRANSITION : DESKTOP_TRANSITION});
            }
        },
        setLocale(): void {
            moment.locale(this.locale);
        },
        displayedValue(value: string): string {
            return value || '';
        },
    },
    mounted() {
        this.setLocale();
    },
});
