export const DEFAULT_LABEL_CLASS = 'cardLabel';

export const CARD_LABEL_COLORS = {
    RED: 'red',
    GREEN: 'green',
    BLUE: 'blue',
    DARK: 'dark',
};

export const CARD_LABEL_POSITIONS = {
    [CARD_LABEL_COLORS.RED]: 0,
    [CARD_LABEL_COLORS.GREEN]: 1,
    [CARD_LABEL_COLORS.BLUE]: 2,
    [CARD_LABEL_COLORS.DARK]: 3,
};

export type Label = {
    color: string,
    text: string,
};
