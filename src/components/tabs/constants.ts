export const TAB_ITEM_CLASS = 'tab__nav__item';
export const TAB_ITEM_ACTIVE_CLASS = 'tabActive';
export const TAB_ITEM_DISABLED_CLASS = 'tabDisabled';
