export type SearchList = {
    correctWord: string,
    differentItem: string,
    moreTerms: string,
}