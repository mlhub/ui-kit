export type Animation = {
    addEventListener: (eventName: string, callback: (e: any) => void) => void;
    play: () => void;
    stop: () => void;
    pause: () => void;
};
