import {FormattedRGBAColor} from '../../../../components/utils/colors/types/RGBAColor';

export type GradientPoint = FormattedRGBAColor & {
    left: number,
};

export type Gradient = {
    type: string,
    degree: number,
    points: GradientPoint[],
}
