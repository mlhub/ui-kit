export type RGBAColor = {
    r: number,
    g: number,
    b: number,
    a: number,
}

export type FormattedRGBAColor = {
    red: number,
    green: number,
    blue: number,
    alpha: number,
}
