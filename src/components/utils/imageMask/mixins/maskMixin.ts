import Vue from 'vue';

export default Vue.extend({
    data() {
        return {};
    },
    computed: {
        svg(): any {
            return this.$el;
        },
        gradient(): any {
            return this.svg.querySelector('linearGradient') || this.svg.querySelector('radialGradient');
        },
    },
    watch: {},
    methods: {
        setGradientAttribute(): void {
            this.svg.setAttribute('withGradient', '');
        },
        changeGradientId(): void {
            // @ts-ignore
            let uniqueId = `gradientId_${this._uid}`;
            this.gradient.setAttribute('id', uniqueId);
            this.svg.querySelector('path').setAttribute('fill', `url(#${uniqueId})`);
        },
    },
    mounted() {
        if (this.svg && this.gradient) {
            this.setGradientAttribute();
            this.changeGradientId();
        }
    },
});