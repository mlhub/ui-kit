<template>
    <component
        v-if="Object.keys(filters).length"
        :is="filterType"
        :filters="filtersVisible"
        :hasTempChanges="hasTempChanges"
        :selectedOptions="selectedOptions"
        :totalItemsText="totalItemsText"
        :localization="localization"
        :filtersWithValue="filtersWithValue"
    >
        <template v-for="(_, index) in filters" v-slot:[index]="data">
            <slot :name="`item.${index}`" v-bind="data"></slot>
        </template>
    </component>
</template>

<script lang="ts">
    import mixins from 'vue-typed-mixins';
    import mediaQueriesMixin from '../../mixins/mediaQueriesMixin';
    import FiltersDesktop from '../../components/filters/FiltersDesktop.vue';
    import FiltersMobile from '../../components/filters/FiltersMobile.vue';
    import {FILTER_TYPE, FilterPayload, FiltersPayload} from '../../components/filters/types/Filters';
    import {
        FilterComponent,
        FilterSelectedOption,
        FilterUrlValue,
    } from '../../components/filters/types/FilterComponent';
    import _ from 'lodash';
    import {Url} from '../../utils/Url';

    export type FiltersLocalizations = {
        filter: string,
        clearFilters: string,
        back: string,
        apply: string,
    };

    export default mixins(mediaQueriesMixin).extend({
        provide() {
            return {
                // @ts-ignore
                registerFilterComponent: this.registerFilterComponent,
                // @ts-ignore
                clearAllFilters: this.clearAllFilters,
                // @ts-ignore
                applyFilterChanges: this.applyFilterChanges,
                // @ts-ignore
                clearFilterSelectedOption: this.clearFilterSelectedOption,
                // @ts-ignore
                applyTempChanges: this.applyTempChanges,
                // @ts-ignore
                discardTempChanges: this.discardTempChanges,
                // @ts-ignore
                markFilterAsUnsaved: this.markFilterAsUnsaved,
                // @ts-ignore
                markFilterAsSaved: this.markFilterAsSaved,
                // @ts-ignore
                storeTempChanges: this.storeTempChanges,
                // @ts-ignore
                restoreFiltersToTempChangesState: this.restoreFiltersToTempChangesState,
            };
        },
        components: {
            FiltersMobile,
            FiltersDesktop,
        },
        props: {
            filters: {
                type: Object as () => FiltersPayload,
                required: true,
            },
            localization: {
                type: Object as () => FiltersLocalizations,
                default: () => {
                    return {
                        filter: 'Filter',
                        clearFilters: 'Clear filters',
                        back: 'Back',
                        apply: 'Apply',
                    } as FiltersLocalizations;
                },
            },
            totalItemsText: {
                type: String,
                default: '',
            },
        },
        data() {
            return {
                filterComponents: {} as { [index: string]: FilterComponent },
                unsavedFilters: [] as string[],
                tempChanges: null as FilterSelectedOption[] | null,
                realChanges: [] as FilterSelectedOption[],
                filtersWithValue: [] as string[],
            };
        },
        computed: {
            selectedOptions(): FilterSelectedOption[] {
                return this.hasTempChanges && this.tempChanges !== null
                    ? this.tempChanges!
                    : this.realChanges;
            },
            hasTempChanges(): boolean {
                return this.hasUnsavedFilters || this.tempChanges !== null;
            },
            hasUnsavedFilters(): boolean {
                return this.unsavedFilters.length > 0;
            },
            filtersVisible(): FiltersPayload {
                return Object.fromEntries(
                    Object.entries(this.filters)
                        .filter(([, value]: [any, FilterPayload]) =>
                            value.options.length > 0 ||
                            value.type === FILTER_TYPE.CUSTOM ||
                            value.type === FILTER_TYPE.DATE_RANGE,
                        ),
                );
            },
            filterType(): string {
                return this.showMobileVersion ? 'FiltersMobile' : 'FiltersDesktop';
            },
            showMobileVersion(): boolean {
                return this.isMedium || this.isMobile;
            },
        },
        watch: {
            realChanges(newValue: FilterSelectedOption[], oldValue: FilterSelectedOption[]): void {
                if (_.isEqual(newValue, oldValue)) {
                    return;
                }
                this.markAllFiltersAsSaved();
                this.setFilterValuesToUrl();
                this.$emit('updated', this.getCurrentValue());
            },
            filters() {
                this.$nextTick(() => {
                    this.$emit('updated', this.getCurrentValue());
                    this.refreshFiltersWithValue();
                });
            },
        },
        methods: {
            markFilterAsUnsaved(index: string): void {
                if (this.unsavedFilters.includes(index)) {
                    return;
                }
                this.unsavedFilters.push(index);
            },
            markAllFiltersAsSaved(): void {
                this.unsavedFilters = [];
            },
            markFilterAsSaved(index: string): void {
                this.unsavedFilters.splice(_.findIndex(this.unsavedFilters, index), 1);
            },
            storeTempChanges(): void {
                let selectedOptions = [] as FilterSelectedOption[];
                for (let index in this.filterComponents) {
                    selectedOptions.push(...this.filterComponents[index].getSelectedOptions());
                    this.markFilterAsSaved(index);
                }
                if (_.isEqual(this.selectedOptions, selectedOptions)) {
                    return;
                }
                this.tempChanges = selectedOptions;
            },
            registerFilterComponent(index: string, filter: FilterComponent): void {
                this.filterComponents[index] = filter;
                filter.parseUrlValue();
                this.markFilterAsSaved(index);
            },
            clearAllFilters(): void {
                for (let index in this.filterComponents) {
                    this.filterComponents[index].clear();
                }
                this.markAllFiltersAsSaved();
            },
            restoreFiltersToTempChangesState(): void {
                for (let index in this.filterComponents) {
                    this.filterComponents[index].setSelectedOptionsSilently(this.selectedOptions);
                    this.markFilterAsSaved(index);
                }
            },
            applyTempChanges(): void {
                this.realChanges = _.cloneDeep(this.selectedOptions);
                this.discardTempChanges();
            },
            discardTempChanges(): void {
                this.tempChanges = null;
                this.markAllFiltersAsSaved();
            },
            setFilterValuesToUrl(): void {
                Url.replaceGetParams(this.getCurrentValue());
            },
            getCurrentValue(): FilterUrlValue {
                let result = Url.getAllGetParams() as FilterUrlValue;
                for (let index in this.filterComponents) {
                    let value = this.filterComponents[index].getUrlValue();
                    result = _.assign(result, value);
                }
                for (const key in result) {
                    if (result[key] === '' || _.isEqual(result[key], []) || result[key] === null) {
                        delete result[key];
                    }
                }
                return result;
            },
            clearFilterSelectedOption(option: FilterSelectedOption): void {
                this.filterComponents[option.filterIndex].clearSelectedOption(option);
            },
            refreshFiltersWithValue(): void {
                let filtersWithValue = [] as string[];
                for (const index in this.filterComponents) {
                    if (this.filterComponents[index].hasValue) {
                        filtersWithValue.push(index);
                    }
                }
                this.filtersWithValue = filtersWithValue;
            },
        },
    });
</script>

<style scoped lang="scss">
    @import "filters";
</style>
