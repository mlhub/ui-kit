import Vue, {VueConstructor} from 'vue';
import {FilterPayload, FilterSettings} from '../types/Filters';
import {MainFilterComponent} from '../types/MainFilterComponent';
import {FilterComponent, FilterSelectedOption, FilterUrlValue} from '../types/FilterComponent';
import _ from 'lodash';
import {Url, UrlGetParams} from '../../../utils/Url';

export default (Vue as VueConstructor<MainFilterComponent & FilterComponent>).extend({
    inject: [
        'registerFilterComponent',
        'applyTempChanges',
        'markFilterAsUnsaved',
        'markFilterAsSaved',
        'storeTempChanges',
        'restoreFiltersToTempChangesState',
    ],
    props: {
        index: {
            type: String,
            required: true,
        },
        filter: {
            type: Object as () => FilterPayload,
        },
    },
    data() {
        return {
            internalValue: null as any,
        };
    },
    computed: {
        hasValue(): boolean {
            return this.internalValue !== null;
        },
        settings(): FilterSettings {
            return this.filter?.settings || {};
        },
        allItemsText(): string {
            return this.settings?.allItemsText || '';
        },
    },
    watch: {
        internalValue: {
            handler(newValue: any, oldValue: any): void {
                if (!_.isEqual(newValue, oldValue)) {
                    this.markFilterAsUnsaved(this.index);
                }
            },
            deep: true,
        },
    },
    methods: {
        getUrlValue(): FilterUrlValue {
            throw new Error('getUrlValue method should be implemented in all filter components');
        },
        parseUrlValue(): void {
            this.internalValue = this.getInternalValueFromUrl(Url.getAllGetParams());
            this.storeTempChanges();
            this.applyTempChanges();
        },
        // @ts-ignore
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        getInternalValueFromUrl(searchParams: UrlGetParams): any {
            throw new Error('getInternalValueFromUrl method should be implemented in all filter components');
        },
        // @ts-ignore
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        setSelectedOptionsSilently(options: FilterSelectedOption[]): void {
            throw new Error('setSelectedOptionsSilently method should be implemented in all filter components');
        },
        // @ts-ignore
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        clearSelectedOption(option: FilterSelectedOption): void {
            this.clear();
        },
        clear(): void {
            this.clearInternalValue();
            this.storeTempChanges();
        },
        clearInternalValue(): void {
            this.internalValue = null;
        },
        getSelectedOptions(): FilterSelectedOption[] {
            throw new Error('getSelectedOptions method should be implemented in all filter components');
        },
    },
    mounted() {
        this.registerFilterComponent(this.index, this);
    },
});
