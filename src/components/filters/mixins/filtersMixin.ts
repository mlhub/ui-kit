import Vue from 'vue';
import {FiltersPayload} from '../types/Filters';
import {FilterUrlValue} from '../types/FilterComponent';
import Filters from '../Filters.vue';

export default Vue.extend({
    components: {
        Filters,
    },
    props: {
        totalItemsCount: {
            type: Number,
            default: 0,
        },
        filteredItem: {
            type: String,
            default: '',
        },
    },
    data() {
        return {
            filters: {} as FiltersPayload,
        };
    },
    computed: {
        totalItemsText(): string {
            return `${this.totalItemsCount} ${this.filteredItem}`;
        },
    },
    methods: {
        getFilters(): Promise<FiltersPayload> {
            throw new Error('getFilters is required method.');
        },
        loadFilters() {
            this.getFilters()
                .then((filters: FiltersPayload) => {
                    this.filters = filters;
                });
        },
        updatedListener(payload: FilterUrlValue): void {
            this.$emit('updated', payload);
        },
    },
    mounted() {
        this.loadFilters();
    },
});
