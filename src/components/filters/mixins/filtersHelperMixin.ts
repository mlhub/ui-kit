import Vue, {VueConstructor} from 'vue';
import {FILTER_TYPE, FiltersPayload} from '../types/Filters';
import {FilterSelectedOption} from '../types/FilterComponent';
import {MainFilterComponent} from '../types/MainFilterComponent';
import CheckboxFilter from '../checkboxFilter/CheckboxFilter.vue';
import LabelFilter from '../labelFilter/LabelFilter.vue';
import DateRangeFilter from '../dateRangeFilter/DateRangeFilter.vue';

export default (Vue as VueConstructor<MainFilterComponent>).extend({
    components: {
        CheckboxFilter,
        LabelFilter,
        DateRangeFilter,
    },
    inject: [
        'storeTempChanges',
        'discardTempChanges',
        'applyTempChanges',
        'restoreFiltersToTempChangesState',
    ],
    props: {
        filters: {
            type: Object as () => FiltersPayload,
            required: true,
        },
        hasTempChanges: {
            type: Boolean,
            required: true,
        },
        selectedOptions: {
            type: Array as () => FilterSelectedOption[],
            required: true,
        },
        localization: {
            type: Object,
            required: true,
        },
        totalItemsText: {
            type: String,
        },
    },
    data() {
        return {
            FILTER_TYPE,
        };
    },
});
