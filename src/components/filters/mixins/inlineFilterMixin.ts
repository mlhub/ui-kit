import Vue from 'vue';
import {FilterOption} from '../types/Filters';
import {FilterUrlValue} from '../types/FilterComponent';
import InlineFilter from '../inlineFilter/InlineFilter.vue';

export default Vue.extend({
    components: {
        InlineFilter,
    },
    data() {
        return {
            options: [] as FilterOption[],
        };
    },
    methods: {
        getOptions(): Promise<FilterOption[]> {
            throw new Error('getFilters is required method.');
        },
        getUrlIndex(): string {
            throw new Error('getUrlIndex is required method.');
        },
        loadOptions() {
            this.getOptions()
                .then((options: FilterOption[]) => {
                    this.options = options;
                });
        },
        updatedListener(payload: FilterUrlValue): void {
            this.$emit('updated', payload);
        },
    },
    mounted() {
        this.loadOptions();
    },
});
