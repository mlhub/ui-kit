import _ from 'lodash';
import singleFilterMixin from './singleFilterMixin';
import {FilterOption} from '../types/Filters';
import {FilterSelectedOption, FilterUrlValue} from '../types/FilterComponent';

export default singleFilterMixin.extend({
    data() {
        return {
            internalValue: [] as FilterOption[],
        };
    },
    computed: {
        canHaveMultipleValues(): boolean {
            return this.settings.isMultiple ?? true;
        },
        hasValue(): boolean {
            return this.internalValue.length > 0;
        },
    },
    methods: {
        isActive(option: FilterOption): boolean {
            return this.internalValue.find((currentOption) => {
               return currentOption.value === option.value;
            }) !== undefined;
        },
        optionClick(option: FilterOption): void {
            let value: FilterOption[] = _.cloneDeep(this.internalValue);
            if (this.isActive(option)) {
                _.remove(value, (selectedOption: FilterOption) => {
                    return selectedOption.value === option.value;
                });
            } else {
                if (this.canHaveMultipleValues) {
                    value.push(option);
                } else {
                    value = [option];
                }
            }
            this.internalValue = value;
        },
        getUrlValue(): FilterUrlValue {
            return {
                [this.index]: _.map(this.internalValue, (option: FilterOption) => {
                    return option.value;
                }) as string[],
            };
        },
        clearSelectedOption(option: FilterSelectedOption): void {
            this.optionClick(option.option);
            this.storeTempChanges();
        },
        clearInternalValue(): void {
            this.internalValue = [];
        },
        getInternalValueFromUrl(searchParams: { [key: string]: string | string[] }): any {
            let values: string[] = searchParams[this.index] as string[] ?? [];
            return _.filter(this.filter.options, (option: FilterOption) => {
                return values.includes(option.value.toString());
            });
        },
        getSelectedOptions(): FilterSelectedOption[] {
            if (!this.hasValue) {
                return [];
            }

            let result: FilterSelectedOption[] = [];
            for (let key in this.internalValue) {
                result.push({
                    filterIndex: this.index,
                    text: this.internalValue[key].title,
                    option: this.internalValue[key],
                });
            }
            return result;
        },
        setSelectedOptionsSilently(options: FilterSelectedOption[]): void {
            let newValue: FilterOption[] = [];
            for (const option of options) {
                if (option.filterIndex !== this.index) {
                    continue;
                }
                newValue.push(option.option);
            }
            this.internalValue = newValue;
        },
    },
});
