export type FiltersPayload = {
    [index: string]: FilterPayload;
};

export type FilterPayload = {
    type: FILTER_TYPE,
    title: string,
    options: FilterOption[],
    settings?: FilterSettings,
};

export type FilterOption = {
    value: number | string,
    title: string,
    counter?: number,
};

export type FilterSettings = {
    placeholder?: string,
    allItemsText?: string,
    isSearchable?: boolean,
    isMultiple?: boolean,
    isOpened?: boolean,
    minValue?: number,
    maxValue?: number,
};

export enum FILTER_TYPE {
    LABEL = 'LabelFilter',
    CHECKBOX = 'CheckboxFilter',
    DATE_RANGE = 'DateRangeFilter',
    CUSTOM = 'CUSTOM',
}
