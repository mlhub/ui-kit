import Vue from 'vue';
import {FilterComponent, FilterSelectedOption} from './FilterComponent';

export interface MainFilterComponent extends Vue {
    registerFilterComponent(index: string, filter: FilterComponent): void

    markFilterAsUnsaved(index: string): void
    markFilterAsSaved(index: string): void

    storeTempChanges(): void
    discardTempChanges(): void
    restoreFiltersToTempChangesState(): void
    applyTempChanges(): void

    clearFilterSelectedOption(option: FilterSelectedOption): void
    clearAllFilters(): void
}
