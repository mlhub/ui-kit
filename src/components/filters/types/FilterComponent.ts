import Vue from 'vue';
import {UrlGetParams} from '../../../utils/Url';

export interface FilterComponent extends Vue {
    hasValue: boolean

    getSelectedOptions(): FilterSelectedOption[]

    clearSelectedOption(option: FilterSelectedOption): void

    setSelectedOptionsSilently(options: FilterSelectedOption[]): void

    clear(): void

    parseUrlValue(): void

    getUrlValue(): FilterUrlValue
}

export type FilterUrlValue = UrlGetParams;

export type FilterSelectedOption = {
    filterIndex: string,
    text: string,
    option: any,
};
