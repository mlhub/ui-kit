export const ALERT_TYPES = {
    ERROR: 'error',
    INFO: 'info',
    WARNING: 'warning',
    SUCCESS: 'success',
    TIP: 'tip',
};

export const ALERT_WRAPPER_CLASS = 'alert__wrapper';
export const ALERT_FIXED_CLASS = 'alert__fixed';
