export const POPOVER_CLASS = 'fixedPopup';
export const POPOVER_ACTIVATOR_ACTIVE_CLASS = 'fixedPopup__activator_active';

export enum POPOVER_SIZE {
    SMALL = 'small',
    MEDIUM = 'medium',
    LARGE = 'large',
    CARD_SIZE = 'cardSize',
}
