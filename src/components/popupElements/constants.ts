export const POSITION_TOP = 'top';
export const POSITION_TOP_LEFT = 'topLeft';
export const POSITION_TOP_RIGHT = 'topRight';

export const POSITION_BOTTOM = 'bottom';
export const POSITION_BOTTOM_LEFT = 'bottomLeft';
export const POSITION_BOTTOM_RIGHT = 'bottomRight';

export const POSITION_LEFT = 'left';
export const POSITION_LEFT_TOP = 'leftTop';
export const POSITION_LEFT_BOTTOM = 'leftBottom';

export const POSITION_RIGHT = 'right';
export const POSITION_RIGHT_TOP = 'rightTop';
export const POSITION_RIGHT_BOTTOM = 'rightBottom';

export const POSITION_ABSOLUTE = 'absolutePosition';

export type BoundingClientRect = {
    x: number,
    y: number,
    width: number,
    height: number,
    top: number,
    left: number,
};

export const POSITIONS = [
    POSITION_TOP,
    POSITION_TOP_LEFT,
    POSITION_TOP_RIGHT,
    POSITION_BOTTOM,
    POSITION_BOTTOM_LEFT,
    POSITION_BOTTOM_RIGHT,
    POSITION_LEFT,
    POSITION_LEFT_TOP,
    POSITION_LEFT_BOTTOM,
    POSITION_RIGHT,
    POSITION_RIGHT_TOP,
    POSITION_RIGHT_BOTTOM,
];


export const POSITION = {
    TOP: 'top',
    TOP_LEFT: 'topLeft',
    TOP_RIGHT: 'topRight',
    BOTTOM: 'bottom',
    BOTTOM_LEFT: 'bottomLeft',
    BOTTOM_RIGHT: 'bottomRight',
    LEFT: 'left',
    LEFT_TOP: 'leftTop',
    LEFT_BOTTOM: 'leftBottom',
    RIGHT: 'right',
    RIGHT_TOP: 'rightTop',
    RIGHT_BOTTOM: 'rightBottom',
};
