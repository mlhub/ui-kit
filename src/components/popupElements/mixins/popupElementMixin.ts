import Vue from 'vue';
import {BoundingClientRect, POSITION} from '../constants';
import _ from 'lodash';

export default Vue.extend({
    props: {
        position: {
            type: String,
            validator(value: string): boolean {
                return _.includes(POSITION, value);
            },
        },
        absolute: {
            type: Boolean,
            default: false,
        },
        auto: {
            type: Boolean,
            default: false,
        },
    },
    data() {
        return {
            blockPosition: {} as BoundingClientRect,
            popoverPosition: {} as BoundingClientRect,
            popupElementPosition: this.position,
        };
    },
    computed: {
        activator(): any {
            return this.$refs.activator;
        },
        popover(): any {
            return this.$refs.popover;
        },
        popupStyle(): any {
            if (!this.absolute) {
                return {
                    top: this.positionTop,
                    left: this.positionLeft,
                };
            }
            return {};
        },
        positionTop(): any {
            switch (this.popupElementPosition) {
                case POSITION.TOP:
                case POSITION.TOP_LEFT:
                case POSITION.TOP_RIGHT:
                case POSITION.LEFT_TOP:
                case POSITION.RIGHT_TOP:
                    return `${this.blockPosition.top}px`;
                case POSITION.BOTTOM:
                case POSITION.BOTTOM_LEFT:
                case POSITION.BOTTOM_RIGHT:
                case POSITION.LEFT_BOTTOM:
                case POSITION.RIGHT_BOTTOM:
                    return `${this.blockPosition.top + this.blockPosition.height}px`;
                case POSITION.LEFT:
                case POSITION.RIGHT:
                    return `${this.blockPosition.top + this.blockPosition.height / 2}px`;
                default: {
                    return `${this.blockPosition.top + this.blockPosition.height / 2}px`;
                }
            }
        },
        positionLeft(): any {
            switch (this.popupElementPosition) {
                case POSITION.TOP:
                case POSITION.BOTTOM:
                    return `${this.blockPosition.left + this.blockPosition.width / 2}px`;
                case POSITION.TOP_LEFT:
                case POSITION.BOTTOM_LEFT:
                case POSITION.LEFT:
                case POSITION.LEFT_TOP:
                case POSITION.LEFT_BOTTOM:
                    return `${this.blockPosition.left}px`;
                case POSITION.TOP_RIGHT:
                case POSITION.BOTTOM_RIGHT:
                case POSITION.RIGHT:
                case POSITION.RIGHT_TOP:
                case POSITION.RIGHT_BOTTOM:
                    return `${this.blockPosition.left + this.blockPosition.width}px`;
                default: {
                    return `${this.blockPosition.left + this.blockPosition.width}px`;
                }
            }
        },
        predefinePositions(): any {
            return [...new Set([
                this.position || POSITION.RIGHT,
                POSITION.TOP,
                POSITION.BOTTOM,
                POSITION.RIGHT,
                POSITION.LEFT,
                // @ts-ignore
                ...Object.keys(POSITION).map(e => e.toLowerCase()),
            ])];
        },
    },
    methods: {
        calculateBlockPosition(): void {
            // @ts-ignore
            this.blockPosition = this.activator ? this.activator.getBoundingClientRect() : {};
            // @ts-ignore
            this.popoverPosition = this.popover ? this.popover.getBoundingClientRect() : {};
        },
        setPopupElementPosition() {
            this.calculateBlockPosition();
            if (this.auto) {
                this.popupElementPosition = this.calculatePosition();
            } else {
                this.popupElementPosition = this.position ? this.position : POSITION.RIGHT;
            }
        },
        calculatePosition(): string {
            for (const position of this.predefinePositions) {
                let positionSide = position.split('_')[0];

                const isVertical = [POSITION.TOP, POSITION.BOTTOM].includes(positionSide);
                let searchPredicate = this.blockPosition[positionSide as keyof DOMRect];

                if (positionSide === POSITION.BOTTOM) {
                    searchPredicate = window.innerHeight - this.blockPosition[positionSide as keyof DOMRect];
                }

                if (positionSide === POSITION.RIGHT) {
                    searchPredicate = window.innerWidth - this.blockPosition[positionSide as keyof DOMRect];
                }

                if (searchPredicate >= this.popoverPosition[isVertical ? 'height' : 'width'] + 24) {
                    return position;
                }
            }
            return this.position || POSITION.RIGHT;
        },
    },
    mounted() {
        window.addEventListener('resize', this.calculateBlockPosition);
        window.addEventListener('scroll', this.calculateBlockPosition);
        window.addEventListener('load', this.calculateBlockPosition);
        this.$nextTick(() => {
            this.calculateBlockPosition();
        });
    },
    beforeDestroy() {
        window.removeEventListener('resize', this.calculateBlockPosition);
        window.removeEventListener('scroll', this.calculateBlockPosition);
        window.removeEventListener('load', this.calculateBlockPosition);
    },
});
