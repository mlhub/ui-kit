export const DIALOG_SIZE_MOBILE = 'mobile';
export const DIALOG_SIZE_SMALL = 'small';
export const DIALOG_SIZE_MEDIUM = 'medium';
export const DIALOG_SIZE_LARGE = 'large';
export const DIALOG_SIZE_EXTRA_LARGE = 'extraLarge';

export const DIALOG_WIDTH_MOBILE = '300px';
export const DIALOG_WIDTH_SMALL = '400px';
export const DIALOG_WIDTH_MEDIUM = '600px';
export const DIALOG_WIDTH_LARGE = '760px';
export const DIALOG_WIDTH_EXTRA_LARGE = '1000px';

export const DIALOG_CLASS = 'dialog__container';
export const DIALOG_CLASS_SIMPLE = 'simple';
export const DIALOG_CLASS_ANIMATION = 'animation';
export const DIALOG_CLASS_WITH_UNDERLINE = 'withUnderline';
export const DIALOG_CLASS_CUSTOM_PADDINGS = 'customPaddings';
export const DIALOG_CLASS_WITH_ACTIONS = 'withActions';
export const DIALOG_CLASS_STRETCH_ON_MOBILE = 'fullViewport';

export enum DIALOG_ACTIONS_POSITION {
    LEFT = 'left',
    CENTER = 'center',
    RIGHT = 'right',
}
