export type RouterLink = {
    name: string,
    params: any,
}

export type Route = {
    link?: RouterLink,
    url?: string,
    title: string,
};
