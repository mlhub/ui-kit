export const DEFAULT_PLYR_OPTIONS = {
    centeredSlides: true,
    storage: {
        enabled: false,
    },
    fullscreen: {
        enabled: true,
        fallback: true,
        iosNative: true,
    },
};

export const DEFAULT_RATIO = '16:9';