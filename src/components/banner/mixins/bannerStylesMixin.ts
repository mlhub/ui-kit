import Vue from 'vue';
import RGBAColorParser from '../../../utils/colors/RGBAColorParser';
import {Slide} from '../../../components/banner/types/Slide';

export default Vue.extend({
    props: {
        slide: Object as () => Slide,
    },
    data() {
        return {
            colorParser: new RGBAColorParser(),
        };
    },
    computed: {
        buttonBackgroundColor(): string {
            return this.colorParser.parse(this.slide.button_background_color!);
        },
        buttonTextColor(): string {
            return this.colorParser.parse(this.slide.button_text_color!);
        },
        titleColor(): string {
            return this.colorParser.parse(this.slide.title_color);
        },
        descriptionColor(): string {
            return this.colorParser.parse(this.slide.description_color);
        },
    },
});
