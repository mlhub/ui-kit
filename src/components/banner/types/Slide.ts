import {RGBAColor} from '../../../components/utils/colors/types/RGBAColor';
import {MASK} from '../../../components/utils/imageMask/constants';
import {Gradient} from '../../../components/utils/colors/types/Gradient';
import {BACKGROUND_SCALE_MODE} from '../../../components/utils/image/constants';

export type Slide = {
    id: number,
    order: number,
    title: string,
    description: string,
    title_color: RGBAColor,
    description_color: RGBAColor,
    background_color: Gradient | RGBAColor,
    button_text: string | null,
    button_target: string | null,
    button_text_color: RGBAColor | null,
    button_background_color: RGBAColor | null,
    button_url: string | null,
    mask: MASK,
    background_media: string | null,
    embed_video_url: string | null,
    background_scale_mode: BACKGROUND_SCALE_MODE,
};

