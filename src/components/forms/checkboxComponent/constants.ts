export const CHECKBOX__WRAPPER_CLASS = 'checkbox__wrapper';
export const CHECKBOX__SMALL_CLASS = 'checkbox__small';
export const CHECKBOX__DISABLED_CLASS = 'checkbox__disabled';
export const CHECKBOX__WITH_ERROR_CLASS = 'checkbox__withError';
export const CHECKBOX__CHECKED_CLASS = 'checkbox__checked';