import _ from 'lodash';
import {SelectOption} from '../types/SelectOption';
import {PaginatedListApiResponse} from '../../../../types/PaginatedListApiResponse';
import InputTemplate from '../../inputTemplate/InputTemplate.vue';
import {createPopper} from '@popperjs/core';

export const FIXED_DROPDOWN_STYLE = 'fixed__dropdown';
export const DEFAULT_PAGE = 0;

export default InputTemplate.extend({
    props: {
        appendToBody: {
            type: Boolean,
            default: true,
        },
        itemText: {
            type: String,
            default: 'text',
        },
        itemValue: {
            type: String,
            default: 'value',
        },
        noResult: {
            type: String,
            // todo: default message?
            default: 'No result',
        },
        options: {
            type: Array,
            default: () => [],
        },
        loadOptionsFunction: {
            type: Function,
        },
        searchable: {
            type: Boolean,
            default: false,
        },
        clearable: {
            type: Boolean,
            default: false,
        },
        hideIcons: {
            type: Boolean,
            default: false,
        },
    },
    data() {
        return {
            currentPage: DEFAULT_PAGE,
            formattedOptions: [] as SelectOption[],
            hasMorePages: true,
            searchText: '',
            placement: 'bottom',
        };
    },
    computed: {
        select(): any {
            return this.$refs.input;
        },
        loadOptionsFunctionIsPresent(): boolean {
            return !!this.loadOptionsFunction;
        },
        loadMoreIsDisabled(): boolean {
            return this.formattedOptions.length < 1 && this.searchable;
        },
        inputValue(): SelectOption | any {
            return {};
        },
        valueIsEmpty(): boolean {
            return _.isEmpty(this.inputValue);
        },
        selectPlaceholder(): string {
            //@ts-ignore
            return this.valueIsEmpty && this.placeholder ? this.placeholder : '';
        },
    },
    watch: {
        options() {
            this.formattedOptions = this.formatOptions(this.options);
        },
    },
    methods: {
        optionIsSelected(option: SelectOption): boolean {
            let result = false;
            _.forEach(this.select.selectedValue, (selectedValue: SelectOption) => {
                if (selectedValue.value === option.value) {
                    result = true;
                }
            });
            return result;
        },
        focusElement(): void {
        },
        getOptionText(option: SelectOption) {
            return option.text;
        },
        loadMore(): void {
            if (this.hasMorePages) {
                this.currentPage++;
                return this.loadOptionsFunction(this.currentPage, this.searchText)
                    .then((response: PaginatedListApiResponse) => {
                        this.formattedOptions = this.formattedOptions.concat(this.formatOptions(response.data.data));
                        this.hasMorePages = response.data.links.next !== null;
                    })
                    .catch((e) => {
                        console.error(e);
                    });
            }
        },
        formatOptions(options: any[]): SelectOption[] {
            return _.map(options, (option: any): SelectOption => {
                return {
                    text: option[this.itemText],
                    value: option[this.itemValue],
                    realOption: option,
                };
            });
        },
        search(value: string): void {
            if (this.loadOptionsFunctionIsPresent) {
                this.searchText = value;
                this.clearData();
                this.loadMore();
            }
        },
        clearData(): void {
            this.formattedOptions = [];
            this.currentPage = DEFAULT_PAGE;
            this.hasMorePages = true;
        },
        withPopper(dropdownList, component, {width}) {
            dropdownList.style.width = width;
            const popper = createPopper(component.$refs.toggle, dropdownList, {
                // @ts-ignore
                placement: this.placement,
                modifiers: [
                    {
                        name: 'offset', options: {
                            offset: [0, 8],
                        },
                    },
                    {
                        name: 'toggleClass',
                        enabled: true,
                        phase: 'write',
                        fn({state}) {
                            component.$el.classList.toggle('drop-up', state.placement === 'top');
                        },
                    }],
            });
            return () => popper.destroy();
        },
    },
    mounted() {
        if (this.loadOptionsFunctionIsPresent) {
            this.loadMore();
        } else {
            this.formattedOptions = this.formatOptions(this.options);
        }
    },
});
