import Vue from 'vue';

export default Vue.extend({
    props: {
        disabled: {
            type: Boolean,
            default: false,
        },
        maxWidthOfUpperElements: {
            type: String,
            default: '100%',
        },
    },
    data() {
        return {};
    },
    computed: {
        maxWidthStyle(): any {
            return {
                maxWidth: this.maxWidthOfUpperElements,
            };
        },
    },
    methods: {},
});
