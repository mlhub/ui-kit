import Vue from 'vue';

export default Vue.extend({
    computed: {
        showPrependContent(): boolean {
            return !!this.$slots.prepend;
        },
        input(): any {
            return this.$refs.input;
        },
        inputTemplate(): any {
            return this.$refs.inputTemplate;
        },
    },
    methods: {
        focusElement() {
            this.input.focus();
        },
        onMouseEnter(): void {
            this.inputTemplate.isHovered = true;
        },
        onMouseLeave(): void {
            this.inputTemplate.isHovered = false;
        },
        onBlur(): void {
            if (!this.inputTemplate.isHovered) {
                this.inputTemplate.isFocused = false;
            }
            this.$emit('change', this.inputTemplate.inputValue);
        },
        onFocus(): void {
            this.inputTemplate.isFocused = true;
            this.$emit('focus');
        },
    },
});
