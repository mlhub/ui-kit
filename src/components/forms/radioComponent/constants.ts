export const RADIO__WRAPPER_CLASS = 'radio__wrapper';
export const RADIO__SMALL_CLASS = 'radio__small';
export const RADIO__DISABLED_CLASS = 'radio__disabled';
export const RADIO__WITH_ERROR_CLASS = 'radio__withError';
export const RADIO__CHECKED_CLASS = 'radio__checked';