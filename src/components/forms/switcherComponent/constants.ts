export const SWITCHER_WRAPPER_CLASS = 'switcher__wrapper';
export const SWITCHER_SMALL = 'switcher__small';
export const SWITCHER_DISABLED = 'switcher__disabled';
export const SWITCHER_CHECKED = 'switcher__checked';
