export const DEFAULT_UPLOAD_FILE_SIZE = 10000000;


export type File = {
    name: string,
    size: number,
    type: string,
}

export type DefaultTexts = {
    title: string,
    divider: string,
    button: string,
}

export const DEFAULT_TEXTS = {
    title: 'Upload files by drag and drop',
    divider: 'or',
    button: 'click to upload',
};

export const MIME_REGEX = new RegExp('\\w+\\/[-+.\\w]+');


