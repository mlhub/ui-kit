import {build, parse} from 'search-params';

export type UrlGetParams = { [key: string]: string | string[] };

export class Url {
    public static getSingleGetParam(paramName: string): string | string[] | undefined {
        return Url.getAllGetParams()[paramName];
    }

    public static getAllGetParams(): UrlGetParams {
        return parse(window.location.search);
    }

    public static addGetParams(params: UrlGetParams): void {
        let currentParams = Url.getAllGetParams();
        let newParams = {...currentParams, ...params};
        Url.replaceGetParams(newParams);
    }

    public static removeGetParam(paramName: string): void {
        let params = Url.getAllGetParams();
        if (params[paramName]) {
            delete params[paramName];
        }
        Url.replaceGetParams(params);
    }

    public static replaceGetParams(params: UrlGetParams): void {
        let queryString = build(params, {arrayFormat: 'brackets'});
        let hash = window.location.hash ?? '';
        let url = queryString.length > 0 ? `${window.location.pathname}?${queryString}${hash}` : `${window.location.pathname}${hash}`;
        window.history.pushState({}, document.title, url);
    }
}
