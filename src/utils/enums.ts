import _ from 'lodash';

export function randomEnumValue<T>(enumeration: T): T[keyof T] {
    return enumeration[_.sample(getAllEnumKeys(enumeration))!];
}

export function getAllEnumKeys<T>(enumeration: T): string[] {
    return Object.keys(enumeration)
        .filter((k) => {
            return Number.isNaN(Number.parseInt(k, 10));
        });
}

export function getAllEnumValues<T>(enumeration: T): T[keyof T][] {
    let keys: string[] = getAllEnumKeys(enumeration);
    return Object.values(enumeration).filter((value: any) => !keys.includes(value));
}

export function enumIncludes<T>(enumeration: T, value: any): boolean {
    let values = getAllEnumValues(enumeration);
    return _.includes(values, value);
}
