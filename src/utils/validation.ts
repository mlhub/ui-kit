import _ from 'lodash';
import {VideoEmbedInfo} from '../utils/videos/VideoEmbedInfo';

export const EMAIL_REGEX = /^([a-zA-Z0-9_-]+\.)*[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)*\.[a-zA-Z]{2,6}$/;
export const DEFAULT_MAX_STRING_LENGTH = 255;

export function emailRule(errorText: string): any {
    let regex = EMAIL_REGEX;
    return (value: string) => regex.test(value) || _valueIsNotSet(value) || errorText;
}

export function requiredRule(errorText: string): any {
    return (value: any) => !_valueIsNotSet(value) || errorText;
}

export function minStringRule(errorText: string, min: number): any {
    return (value: any) => !_valueIsNotSet(value) && value.length >= min || _valueIsNotSet(value) || errorText;
}

export function minNumberRule(errorText: string, min: number): any {
    return (value: any) => {
        if (_valueIsNotSet(value)) {
            return true;
        }
        value = stringToFloat(value);

        return value >= min || errorText;
    };
}

export function numberIsInteger(errorText: string): any {
    return (value: any) => {
        if (_valueIsNotSet(value)) {
            return true;
        }

        let regex = /^-?\d+$/;
        return regex.test(value) || errorText;
    };
}

export function maxStringRule(errorText: string, max: number = DEFAULT_MAX_STRING_LENGTH): any {
    return (value: any) => !_valueIsNotSet(value) && value.length <= max || _valueIsNotSet(value) || errorText;
}

export function maxNumberRule(errorText: string, max: number = DEFAULT_MAX_STRING_LENGTH): any {
    return (value: any) => {
        if (_valueIsNotSet(value)) {
            return true;
        }
        value = stringToFloat(value);
        return value <= max || errorText;
    };
}

export function urlRule(errorText: string): any {
    let regex = /^((https?:\/\/)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,15}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*))$/;
    return (value: string) => regex.test(value) || _valueIsNotSet(value) || errorText;
}

export function embedVideoUrlRule(errorText: string): any {
    return (value: string) => VideoEmbedInfo.isSupportedEmbedVideoUrl(value) || _valueIsNotSet(value) || errorText;
}

export function numberIsNotInRange(errorText: string, from: number, to: number): any {
    return (value: any) => {
        if (_valueIsNotSet(value)) {
            return true;
        }
        value = stringToFloat(value);
        return (value <= from || value >= to) || errorText;
    };
}

function _valueIsNotSet(value: any): boolean {
    return value === undefined || value === null || value === '' || (Array.isArray(value) && value.length === 0);
}

export function isValid(value: any, rules: any): boolean {
    let validationMessage = validate(value, rules);
    return !validationMessage;
}

export function validate(value: string, rules: any): string {
    let failedRule = _.find(rules, (rule: any) => {
            return rule(value) !== true;
        },
    );
    if (failedRule) {
        return failedRule(value);
    }
    return '';
}

export function stringToFloat(value: any): number {
    return typeof value === 'string' ? parseFloat(value.replace(/[\s,%]/g, '')) : value;
}
