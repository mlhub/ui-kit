import _ from 'lodash';
import faker from 'faker';
import {Slide} from '../../components/banner/types/Slide';
import {RGBAColorFactory} from '../factories/RGBAColorFactory';
import {MASK} from '../../components/utils/imageMask/constants';
import {BACKGROUND_SCALE_MODE} from '../../components/utils/image/constants';

const rgbaColorFactory = new RGBAColorFactory();

export class SlideFactory {
    public generate(overrides: Partial<Slide> = {}): Slide {
        let result: Slide = {
            id: faker.random.number(),
            order: faker.random.number(),
            title: faker.lorem.word(),
            description: faker.lorem.sentence(),
            title_color: rgbaColorFactory.generate(),
            description_color: rgbaColorFactory.generate(),
            background_color: rgbaColorFactory.generate(),
            button_text: faker.lorem.words(),
            button_target: '_blank',
            button_text_color: rgbaColorFactory.generate(),
            button_background_color: rgbaColorFactory.generate(),
            button_url: faker.internet.url(),
            background_media: faker.internet.url(),
            embed_video_url: null,
            mask: MASK.DEPTH,
            background_scale_mode: BACKGROUND_SCALE_MODE.FILL,
        };

        _.assign(result, overrides);

        return result;
    }
}
