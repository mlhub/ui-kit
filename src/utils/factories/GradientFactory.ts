import _ from 'lodash';
import faker from 'faker';
import {Gradient} from '@/components/utils/colors/types/Gradient';
import {GRADIENT_TYPE} from '@/components/pickers/color/gradientPicker/constants';

export class GradientFactory {
    public generate(overrides: Partial<Gradient> = {}): Gradient {
        let result: Gradient = {
            type: faker.random.objectElement(GRADIENT_TYPE),
                degree: faker.random.number({min: 0, max: 100}),
                points: [
                {
                    left: faker.random.number({min: 0, max: 100}),
                    red: faker.random.number({min: 0, max: 255}),
                    green: faker.random.number({min: 0, max: 255}),
                    blue: faker.random.number({min: 0, max: 255}),
                    alpha: 1,
                },
                {
                    left: faker.random.number({min: 0, max: 100}),
                    red: faker.random.number({min: 0, max: 255}),
                    green: faker.random.number({min: 0, max: 255}),
                    blue: faker.random.number({min: 0, max: 255}),
                    alpha: 1,
                },
            ],
        };

        _.assign(result, overrides);

        return result;
    }
}
