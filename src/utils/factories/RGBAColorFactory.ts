import _ from 'lodash';
import faker from 'faker';
import {RGBAColor} from '../../components/utils/colors/types/RGBAColor';

export class RGBAColorFactory {
    public generate(overrides: Partial<RGBAColor> = {}): RGBAColor {
        let result: RGBAColor = {
            r: faker.random.number({min: 0, max: 255}),
            g: faker.random.number({min: 0, max: 255}),
            b: faker.random.number({min: 0, max: 255}),
            a: 1,
        };

        _.assign(result, overrides);

        return result;
    }
}
