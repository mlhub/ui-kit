import axios from 'axios';

export enum EMBED_VIDEO_TYPE {
    YOUTUBE = 'youtube',
    VIMEO = 'vimeo',
}

export enum EMBED_THUMBNAIL_SRC {
    YOUTUBE = 'https://img.youtube.com/vi/',
    VIMEO = 'https://vimeo.com/api/v2/video/',
    VIMEO_V3 = 'https://vimeo.com/api/oembed.json',
}

const regexVimeo = /^.*(vimeo.com\/|video\/)(\d+)(\?.*&*h=|\/)+([\d,a-f]+)/;

export class VideoEmbedInfo {
    public readonly fullUrl: string;
    public readonly id: string;
    public readonly hash: string | null;
    public readonly type: string;
    public thumbnail: string;

    constructor(url: string) {
        if (!VideoEmbedInfo.isSupportedEmbedVideoUrl(url)) {
            throw new Error('url is not embed');
        }
        this.fullUrl = url;
        this.id = this.getId();
        this.type = this.getType();
        this.hash = this.getHash();
        this.thumbnail = '';
        this.setThumbnail();
    }

    static isSupportedEmbedVideoUrl(url: string): boolean {
        return !!url && !!url.match(/(http:\/\/|https:\/\/|)(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);
    }

    private getType(): EMBED_VIDEO_TYPE {

        let type;

        if (RegExp.$3.indexOf('youtu') > -1) {
            type = EMBED_VIDEO_TYPE.YOUTUBE;
        } else if (RegExp.$3.indexOf('vimeo') > -1) {
            type = EMBED_VIDEO_TYPE.VIMEO;
        }
        return type;
    }

    private getId(): string {
        return RegExp.$6;
    }

    private getHash(): string | null {
        if (this.type !== EMBED_VIDEO_TYPE.VIMEO) {
            return null;
        }
        const parseArray = this.fullUrl.match(regexVimeo);
        if (parseArray && parseArray.length === 5) {
            return parseArray[4];
        }
        return null;
    }

    private setThumbnail(): void {
        if (this.type === EMBED_VIDEO_TYPE.YOUTUBE) {
            this.setYoutubeThumbnail();
        } else if (this.type === EMBED_VIDEO_TYPE.VIMEO) {
            this.setVimeoThumbnail();
        } else {
            this.thumbnail = '';
        }
    }

    private setYoutubeThumbnail(): void {
        this.thumbnail = `${EMBED_THUMBNAIL_SRC.YOUTUBE}${this.id}/0.jpg`;
    }

    private setVimeoThumbnail(): void {
        if (this.hash) {
            axios.get(`${EMBED_THUMBNAIL_SRC.VIMEO_V3}?url=${this.fullUrl}`)
                .then((response) => {
                    this.thumbnail = `${response.data.thumbnail_url}.jpg`;
                });
        } else {
            axios.get(`${EMBED_THUMBNAIL_SRC.VIMEO}${this.id}.json`)
                .then((response) => {
                    this.thumbnail = `${response.data[0].thumbnail_large}.jpg`;
                });
        }
    }
}
