import {AnimationCustomizationPattern} from './AnimationCustomizationPattern';
import defaultCustomizationColors from './defaultCustomizationColors';
import {flatten} from 'lottie-colorify';
import _ from 'lodash';

export class AnimationCustomizationApplier {
    public apply(animation: any, pattern: AnimationCustomizationPattern): any {
        let result = _.cloneDeep(animation);
        result.layers.forEach((layer, key) => {
            const layerName = layer.nm;
            let color;
            if (pattern[layerName]) {
                color = pattern[layerName];
            } else if (defaultCustomizationColors[layerName]) {
                color = defaultCustomizationColors[layerName];
            } else {
                return;
            }
            result.layers[key] = this.setNewColorToLayer(layer, color);
        });

        return result;
    }

    private setNewColorToLayer(layer: any, color: string): any {
        let newLayer;
        try {
            newLayer = flatten(color, layer);
        } catch (e) {
            newLayer = layer;
            try {
                // set color directly (without library)
                newLayer.shapes[0].it[1].c.k = this.convertColorToLottieColor(color);
            } catch (e) {
                //
            }
        }
        try {
            // set alpha (opacity)
            newLayer.shapes[0].it[1].o.k = 100;
        } catch (e) {
            //
        }

        return newLayer;
    }

    private convertColorToLottieColor(color: any): number[] | undefined {
        if (
            typeof color === 'string' &&
            color.match(/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i)
        ) {
            const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);
            if (!result) {
                throw new Error('Color can be only hex or rgb array (ex. [10,20,30])');
            }
            return [
                Math.round((parseInt(result[1], 16) / 255) * 1000) / 1000,
                Math.round((parseInt(result[2], 16) / 255) * 1000) / 1000,
                Math.round((parseInt(result[3], 16) / 255) * 1000) / 1000,
            ];
        } else if (
            typeof color === 'object' &&
            color.length === 3 &&
            color.every((item) => item >= 0 && item <= 255)
        ) {
            return [
                Math.round((color[0] / 255) * 1000) / 1000,
                Math.round((color[1] / 255) * 1000) / 1000,
                Math.round((color[2] / 255) * 1000) / 1000,
            ];
        } else {
            throw new Error('Color can be only hex or rgb array (ex. [10,20,30])');
        }
    }
}