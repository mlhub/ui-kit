/**
 * Contains colors in HEX e.g #FF00FF.
 * {
 *     brand: '#FF00FF',
 *     middle: '#FF00FF',
 *     additional: '#FF00FF',
 *     background: '#FF00FF'
 * }
 */
export type AnimationCustomizationPattern = {
    brand: string,
    middle: string,
    additional: string,
    background: string,
};