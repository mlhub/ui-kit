export const IMAGES_ACCEPT_FORMATS_STRING = '.jpg, .jpeg, .png, .gif';
export const IMAGES_ACCEPTED_FORMATS = IMAGES_ACCEPT_FORMATS_STRING.split(', ');

export const IMAGES_ACCEPTED_MIME_TYPES = ['image/jpeg', 'image/png', 'image/gif'];

export function hasImageExtension(fileName: string): boolean {
    return IMAGES_ACCEPTED_FORMATS.includes(`.${getFileExtension(fileName)}`);
}

export function getFileExtension(fileName: string): string {
    let splittedName = fileName.split('.');
    return splittedName[splittedName.length - 1].toLowerCase();
}
