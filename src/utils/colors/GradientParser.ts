import {Gradient, GradientPoint} from '../../components/utils/colors/types/Gradient';
import {GRADIENT_TYPE} from '../../components/pickers/color/gradientPicker/constants';

export default class GradientParser {
    public parse(gradient: Gradient): string {
        if (gradient.type === GRADIENT_TYPE.LINEAR) {
            return this.getLinearGradientStyle(gradient);
        } else {
            return this.getRadialGradientStyle(gradient);
        }
    }

    private getLinearGradientStyle(gradient: Gradient): string {
        return `linear-gradient(${gradient.degree}deg,${this.getGradientPointsStyles(gradient)})`;
    }

    private getRadialGradientStyle(gradient: Gradient): string {
        return `radial-gradient(${this.getGradientPointsStyles(gradient)})`;
    }

    private getGradientPointsStyles(gradient: Gradient): string {
        let result = '';
        let sortedPoints = this.getSortedPoints(gradient);
        sortedPoints.forEach((point: GradientPoint, index: number) => {
            result += this.getSinglePointStyles(point);
            if (index !== sortedPoints.length - 1) {
                result += ',';
            }
        });
        return result;
    }

    private getSinglePointStyles(point: GradientPoint): string {
        return ` rgba(${point.red}, ${point.green}, ${point.blue}, ${point.alpha}) ${point.left}%`;
    }

    private getSortedPoints(gradient: Gradient): GradientPoint[] {
        return gradient.points
            .slice()
            .sort((a: GradientPoint, b: GradientPoint) => a.left - b.left);
    }
}
