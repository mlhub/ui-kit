import {RGBAColor} from '../../components/utils/colors/types/RGBAColor';

export default class RGBAColorParser {
    public parse(rgba: RGBAColor): string {
        if (rgba.a === 1) {
            return `rgb(${rgba.r}, ${rgba.g}, ${rgba.b})`;
        }
        return `rgba(${rgba.r}, ${rgba.g}, ${rgba.b}, ${rgba.a})`;
    }
}
