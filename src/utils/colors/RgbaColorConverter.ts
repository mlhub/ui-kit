export default class RgbaColorConverter {

    public colorToRgbaArray(color: string): null | number[] {
        if (!color) {
            return null;
        }
        if (this.colorIsEmpty(color)) {
            return null;
        } else if (this.colorIsTransparent(color)) {
            return [0, 0, 0, 0];
        } else if (this.colorIsHex(color)) {
            return this.hexToRgba(color);
        } else if (this.colorIsRgb(color)) {
            return this.rgbToRgba(color);
        } else if (this.colorIsRgba(color)) {
            return this.rgbaStringToArray(color);
        }
        return null;
    }

    public rgbaArrayToString(rgbaArray: number[]): string | null {
        if (!rgbaArray) {
            return null;
        }
        return `rgba(${rgbaArray.join()})`;
    }

    private colorIsEmpty(color: string): boolean {
        return color === '';
    }

    private colorIsTransparent(color: string | null): boolean {
        if (!!color && typeof color === 'string') {
            return color.toLowerCase() === 'transparent';
        }
        return false;
    }

    private colorIsHex(color: string | null): null | boolean {
        if (!color) {
            return null;
        }
        return color[0] === '#';
    }

    private hexToRgba(color: string | null): null | number[] {
        if (!color) {
            return null;
        }
        if (color.length < 7) {
            // convert #RGB and #RGBA to #RRGGBB and #RRGGBBAA
            color = '#' + color[1] + color[1] + color[2] + color[2] + color[3] + color[3] + (color.length > 4 ? color[4] + color[4] : '');
        }
        return [parseInt(color.substr(1, 2), 16),
            parseInt(color.substr(3, 2), 16),
            parseInt(color.substr(5, 2), 16),
            color.length > 7 ? parseInt(color.substr(7, 2), 16) / 255 : 1];
    }

    private colorIsRgb(color: string | null): boolean {
        return !!color && color.indexOf('rgb') === 0 && color.indexOf('rgba') === -1;
    }

    private rgbToRgba(color: string | null): null | number[] {
        if (!color) {
            return null;
        }
        color += ',1';
        return this.rgbaStringToArray(color);
    }

    private colorIsRgba(color: string | null): boolean {
        return !!color && color.indexOf('rgb') === 0 && color.indexOf('rgba') === 0;
    }

    private rgbaStringToArray(color: string | null): null | number[] {
        if (!color) {
            return null;
        }
        // @ts-ignore
        return color.match(/[\.\d]+/g).map((a) => {
            return +a;
        });
    }
}
