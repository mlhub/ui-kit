const smartgrid = require('smart-grid');

const settings = {
    oldSizeStyle: false,
    properties: [],
    outputStyle: 'scss', /* less || scss || sass || styl */
    columns: 12, /* number of grid columns */
    offset: '30px', /* gutter width px || % */
    mobileFirst: false, /* mobileFirst ? 'min-width' : 'max-width' */
    container: {
        maxWidth: '100%', /* max-width оn very large screen */
        fields: '24px', /* side fields */
    },
    breakPoints: {
        extraLarge: {
            width: '1679px',
        },
        large: {
            width: '1279px',
            offset: '20px',
        },
        medium: {
            width: '1023px',
            offset: '20px',
        },
        small: {
            width: '767px',
            offset: '20px',
        },
    },
};

smartgrid('../scss', settings);
