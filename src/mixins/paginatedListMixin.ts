import axios from 'axios';
import _ from 'lodash';
import Vue from 'vue';

export const DEFAULT_PAGE = 0;

export default Vue.extend({
    props: {},
    data() {
        return {
            currentPage: DEFAULT_PAGE,
            hasMorePages: true,
            items: [] as Array<any>,
            searchText: '',
            showEmptySearchField: false,
            filters: {} as any,
            params: {} as any,
            filterAreLoaded: false,
            isLoading: false,
            showEmptyBlock: false,
            emptySearchText: '',
        };
    },
    computed: {},
    watch: {
        filters: {
            handler(): void {
                this.reloadItems();
            },
            deep: true,
        },
    },
    methods: {
        getUrl(): string {
            throw new Error('getUrl is required method.');
        },
        getParams(): any {
            let params: any = _.merge(_.clone(this.params), _.clone(this.filters));

            params.search = this.searchText;

            params.page = ++this.currentPage;

            return params;
        },
        loadMore(): void {
            if (!this.hasMorePages || this.isLoading) {
                return;
            }

            this.isLoading = true;
            this.showEmptyBlock = false;
            this.showEmptySearchField = false;
            this.emptySearchText = this.searchText;

            axios.get(this.getUrl(), {
                params: this.getParams(),
            })
                .then((response: any) => {
                    if (response.data.data.length === 0) {
                        if (this.searchText) {
                            this.showEmptySearchField = true;
                        } else {
                            this.showEmptyBlock = true;
                        }
                    } else {
                        this.items = _.concat(this.items, this.formatItems(response.data.data));
                        this.afterNextPageLoad(response);
                        this.filterAreLoaded = true;
                    }
                    this.hasMorePages = response.data.links.next !== null;
                })
                .catch((error: any) => {
                    console.log(error);
                })
                .finally(() => {
                    this.isLoading = false;
                });
        },
        formatItems(responseData: any): any {
            return responseData;
        },
        afterNextPageLoad(response: any): any {
            return response;
            //    optional method
        },
        reloadItems(): void {
            this.currentPage = DEFAULT_PAGE;
            this.items = [];
            this.showEmptySearchField = false;
            this.hasMorePages = true;
            this.loadMore();
        },
        setFilter(key: string, value: any) {
            this.$set(this.filters, key, value);
        },
        clearFilterKey(key: string) {
            this.$delete(this.filters, key);
        },
        clearAllFilters() {
            this.filters = {};
        },
    },
    beforeMount() {
        this.filterAreLoaded = false;
    },
});
