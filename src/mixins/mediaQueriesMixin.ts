import Vue from 'vue';

export default Vue.extend({
    computed: {
        isMobile(): boolean {
            return this.$root.$mq === 'small' || this.$mq === 'extraSmall';
        },
        isMedium(): boolean {
            return this.$root.$mq === 'medium';
        },
        isDesktop(): boolean {
            return !this.isMobile;
        },
        isLarge(): boolean {
            return this.$root.$mq === 'large';
        },
        isExtraLarge(): boolean {
            return this.$root.$mq === 'extraLarge';
        },
        isInfinity(): boolean {
            return this.$root.$mq === 'Infinity';
        },
    },
});
