import VuePlyr from 'vue-plyr';

export function setUpPlyr(Vue: any) {
    Vue.use(VuePlyr);
}
