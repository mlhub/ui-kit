const currentValueProp = 'vLineClampValue';

const styles = `
      display: block;
      display: -webkit-box;
      -webkit-box-orient: vertical;
      overflow: hidden;
      word-break: break-word;
      text-overflow: ellipsis;
    `;

function defaultFallbackFunc(el, bindings, lines) {
    if (lines) {
        let lineHeight = parseInt(bindings.arg);
        if (isNaN(lineHeight)) {
            console.warn(
                'line-height argument for vue-line-clamp must be a number (of pixels), falling back to 16px',
            );
            lineHeight = 16;
        }

        let maxHeight = lineHeight * lines;

        el.style.maxHeight = maxHeight ? maxHeight + 'px' : '';
        el.style.overflowX = 'hidden';
        el.style.lineHeight = lineHeight + 'px'; // to ensure consistency
    } else {
        el.style.maxHeight = el.style.overflowX = '';
    }
}

const truncateText = function (el, bindings, useFallbackFunc) {
    let lines = parseInt(bindings.value);
    if (isNaN(lines)) {
        console.error('Parameter for vue-line-clamp must be a number');
        return;
    } else if (lines !== el[currentValueProp]) {
        el[currentValueProp] = lines;

        if (useFallbackFunc) {
            useFallbackFunc(el, bindings, lines);
        } else {
            el.style.webkitLineClamp = lines ? lines : '';
        }
    }
};

const useFallbackFunc =
    'webkitLineClamp' in document.body.style
        ? undefined
        : defaultFallbackFunc;

export const directive = {
    // currentValue: 0,
    bind(el) {
        el.style.cssText += styles;
    },
    inserted: (el, bindings) => truncateText(el, bindings, useFallbackFunc),
    update: (el, bindings) => truncateText(el, bindings, useFallbackFunc),
    componentUpdated: (el, bindings) => truncateText(el, bindings, useFallbackFunc),
};

export const componentDefinition = {
    directives: {
        'line-clamp': directive,
    },
};

