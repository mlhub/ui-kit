import Toasted from 'vue-toasted';
import {
    LargeToastPayload,
    TOAST_ERROR_ICON,
    TOAST_INFO_ICON,
    TOAST_SUCCESS_ICON,
    TOAST_WARNING_ICON,
} from '../types/Toast';

export function setUpToasts(Vue: any) {
    Vue.use(Toasted);

    let options = {
        position: 'bottom-right',
        iconPack: 'custom-pack',
        keepOnHover: true,
        duration: 10000,
        action: {
            text: '',
            class: 'toast__cross',
            // @ts-ignore
            onClick: (e, toastObject) => {
                toastObject.goAway(0);
            },
        },
    };

    registerToast(options, Vue);

    Vue.mixin({
        methods: {
            $showToast(message: string): void {
                // @ts-ignore
                this.$toasted.global.toast({
                    message: message,
                });
            },

            $showLargeToast(payload: LargeToastPayload): void {
                // @ts-ignore
                this.$toasted.global.largeToast(payload);
            },
        },
    });
}

function registerToast(options: any, Vue: any) {
    Vue.toasted.register('toast',
        (payload) => {
            if (!payload.message) {
                throw new Error('Toast message is required field.');
            }

            return `<div class="toast">
                        <div class="toast__text">${payload.message}</div>
                    </div>
                    <div class="toast__cross__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path d="M18 6L6 18" stroke="#ffffff" stroke-width="2" stroke-linecap="round"/>
                                        <path d="M18 18L6 6" stroke="#ffffff" stroke-width="2" stroke-linecap="round"/>
                            </svg>
                    </div>`;
        },
        options,
    );

    Vue.toasted.register('largeToast',
        (payload: LargeToastPayload) => {
            return `<div class="toast__icon">
                        ${getIcon(payload.type)}
                    </div>
                    <div class="toast">
                        <div class="toast__title">${payload.title}</div>
                        <div class="toast__text">${payload.message}</div>
                    </div>
                    <div class="toast__cross__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path d="M18 6L6 18" stroke="#ffffff" stroke-width="2" stroke-linecap="round"/>
                                        <path d="M18 18L6 6" stroke="#ffffff" stroke-width="2" stroke-linecap="round"/>
                            </svg>
                    </div>`;
        },
        options,
    );
}

function getIcon(type: string) {
    if (type === TOAST_SUCCESS_ICON) {
        return '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M20 10C20 15.5228 15.5228 20 10 20C4.47715 20 0 15.5228 0 10C0 4.47715 4.47715 0 10 0C15.5228 0 20 4.47715 20 10ZM7.3824 9.06892C7.50441 9.12133 7.61475 9.19751 7.707 9.29302L9 10.586L12.293 7.29302C12.3852 7.19751 12.4956 7.12133 12.6176 7.06892C12.7396 7.01651 12.8708 6.98892 13.0036 6.98777C13.1364 6.98662 13.2681 7.01192 13.391 7.0622C13.5138 7.11248 13.6255 7.18673 13.7194 7.28063C13.8133 7.37452 13.8875 7.48617 13.9378 7.60907C13.9881 7.73196 14.0134 7.86364 14.0122 7.99642C14.0111 8.1292 13.9835 8.26042 13.9311 8.38243C13.8787 8.50443 13.8025 8.61477 13.707 8.70702L9.707 12.707C9.51947 12.8945 9.26516 12.9998 9 12.9998C8.73483 12.9998 8.48053 12.8945 8.293 12.707L6.293 10.707C6.19749 10.6148 6.1213 10.5044 6.0689 10.3824C6.01649 10.2604 5.9889 10.1292 5.98775 9.99642C5.98659 9.86364 6.0119 9.73196 6.06218 9.60907C6.11246 9.48617 6.18671 9.37452 6.2806 9.28063C6.3745 9.18673 6.48615 9.11248 6.60904 9.0622C6.73194 9.01192 6.86362 8.98662 6.9964 8.98777C7.12918 8.98892 7.2604 9.01651 7.3824 9.06892Z" fill="white"/></svg>';
    }
    if (type === TOAST_WARNING_ICON) {
        return '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M12.6488 1.51817C11.4716 -0.506055 8.52842 -0.506056 7.35115 1.51817L0.414341 13.4455C-0.762924 15.4697 0.708658 18 3.06319 18H16.9368C19.2913 18 20.7629 15.4697 19.5857 13.4455L12.6488 1.51817ZM10.0005 5.00073C9.4482 5.00073 9.00049 5.44845 9.00049 6.00073V11.0007C9.00049 11.553 9.4482 12.0007 10.0005 12.0007C10.5528 12.0007 11.0005 11.553 11.0005 11.0007V6.00073C11.0005 5.44845 10.5528 5.00073 10.0005 5.00073ZM10.0005 13.0007C9.4482 13.0007 9.00049 13.4484 9.00049 14.0007C9.00049 14.553 9.4482 15.0007 10.0005 15.0007C10.5528 15.0007 11.0005 14.553 11.0005 14.0007C11.0005 13.4484 10.5528 13.0007 10.0005 13.0007Z" fill="white"/></svg>';
    }
    if (type === TOAST_ERROR_ICON) {
        return '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M12.1199 0.878112C10.9491 -0.292703 9.05087 -0.292705 7.88005 0.87811L0.878112 7.88005C-0.292703 9.05087 -0.292705 10.9491 0.87811 12.1199L7.88005 19.1219C9.05087 20.2927 10.9491 20.2927 12.1199 19.1219L19.1219 12.1199C20.2927 10.9491 20.2927 9.05087 19.1219 7.88005L12.1199 0.878112ZM10 5C9.44772 5 9 5.44772 9 6V11C9 11.5523 9.44772 12 10 12C10.5523 12 11 11.5523 11 11V6C11 5.44772 10.5523 5 10 5ZM10 13C9.44772 13 9 13.4477 9 14C9 14.5523 9.44772 15 10 15C10.5523 15 11 14.5523 11 14C11 13.4477 10.5523 13 10 13Z" fill="white"/></svg>';
    }
    if (type === TOAST_INFO_ICON) {
        return '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M10 20C4.477 20 0 15.523 0 10C0 4.477 4.477 0 10 0C15.523 0 20 4.477 20 10C20 15.523 15.523 20 10 20ZM11 6C11 6.55228 10.5523 7 10 7C9.44771 7 9 6.55228 9 6C9 5.44772 9.44771 5 10 5C10.5523 5 11 5.44772 11 6ZM10 8C10.5523 8 11 8.44771 11 9V14C11 14.5523 10.5523 15 10 15C9.44771 15 9 14.5523 9 14V9C9 8.44771 9.44771 8 10 8Z" fill="white"/></svg>';
    }
}
