import {Toasted} from 'vue-toasted';

declare module 'vue/types/vue' {
  interface Vue {
    $mq: string;
    $themeVariables: object;
    $toasted: Toasted;
    $showToast: (message: string) => void;
  }
}
