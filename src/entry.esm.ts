import _Vue, {PluginFunction} from 'vue';
import {directive} from './plugins/vueLineClamp';
import VueMq from 'vue-mq';
import {PartialDeep} from 'type-fest';
import {UiKitOptions} from '../types';
import {setUpToasts} from './plugins/Toast';
import {setUpPlyr} from './plugins/plyr';

// @ts-ignore
import themeVariables from './assets/scss/colors.module.scss';
// Import vue components
// import * as components from '@/components/index';

// install function executed by Vue.use()
const install: PluginFunction<any> = function installMlhUiKit(Vue: typeof _Vue, options: PartialDeep<UiKitOptions> = {}) {
    Vue.prototype.$themeVariables = themeVariables;
    setUpMq(Vue, options);
    setUpLineClamp(Vue);
    setUpToasts(Vue);
    setUpPlyr(Vue);
};

function setUpMq(Vue: any, options: PartialDeep<UiKitOptions> = {}) {
    let mqOptions = {
        breakpoints: {
            extraSmall: 320,
            small: 768,
            medium: 1024,
            large: 1280,
            extraLarge: 1680,
            Infinity: Infinity,
        },
    };
    if (options.mqOptions) {
        // @ts-ignore
        mqOptions = options.mqOptions;
    }
    Vue.use(VueMq, mqOptions);
}

function setUpLineClamp(Vue: any) {
    Vue.directive('line-clamp', directive);
}

// Create module definition for Vue.use()
export default install;

// To allow individual component use, export components
// each can be registered via Vue.component()
export * from '@/components/index';
export * from '@/сonstants';
