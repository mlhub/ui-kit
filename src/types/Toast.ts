export const TOAST_SUCCESS_ICON = 'success';
export const TOAST_WARNING_ICON = 'warning';
export const TOAST_ERROR_ICON = 'error';
export const TOAST_INFO_ICON = 'info';

export type LargeToastPayload = {
    title: string,
    message: string,
    type: string,
};
